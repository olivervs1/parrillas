<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class StudioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('studios')->insert([
	        	[
	            'name' => 'Estudio 1',
	            'shortname' => 'ES1',
	        	],[
	        	'name' => 'Estudio 2',
	            'shortname' => 'ES2',
	        	],[
	        	'name' => 'Milenio',
	            'shortname' => 'ES3',
	        	]
        	]
    	);
    }
}
