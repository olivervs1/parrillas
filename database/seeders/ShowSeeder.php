<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ShowSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('shows')->insert([
	        	[
	            'name' => 'Viva la Vi',
	            'shortname' => 'VLV',
	            'employee_id' => '1',
	            'studio_id' => '1',
	            'active' => '1',
	            'notes' => 'Entretenimiento general nocturno',
	        	],[
	        	'name' => 'Futbol al día',
	            'shortname' => 'FAD',
	            'employee_id' => '2',
	            'studio_id' => '2',
	            'active' => '1',
	            'notes' => 'Debate futbolístico general',
	        	],[
	        	'name' => 'Telediario Matutino',
	            'shortname' => 'TDM',
	            'employee_id' => '3',
	            'studio_id' => '2',
	            'active' => '1',
	            'notes' => 'Noticias al momento por la mañana',
	        	]
        	]
    	);
    }
}
