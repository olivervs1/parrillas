<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('positions')->insert([
	        	[
	            'name' => 'Camarógrafo',
	            'shortname' => 'CAM',
	            'active' => '1',
	            'rate' => '175.25',
	            'notes' => 'Camarógrafo con especialidad y conocimiento en grabación de estudio y exterior',
	        	],[
	        	'name' => 'Operador de audio',
	            'shortname' => 'OPA',
	            'active' => '1',
	            'rate' => '210.5',
	            'notes' => 'Operador de audio de estudio',
	        	],[
	        	'name' => 'Floor manager',
	            'shortname' => 'FMG',
	            'active' => '0',
	            'rate' => '145.55',
	            'notes' => 'Operador de studio encargado de eventualidades',
	        	]
        	]
    	);
    }
}
