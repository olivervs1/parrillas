<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Alejandro Gonzalez',
            'email' => 'adgm1988@gmail.com',
            'password' => bcrypt('12341234'),
        ]);
    }
}
