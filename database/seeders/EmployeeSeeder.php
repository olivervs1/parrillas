<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('employees')->insert([
	        	[
	            'name' => 'Juan Carlos',
	            'number' => 'JC1234',
	            'active' => '1',
	            'notes' => 'Camarógrafo con mucha experiencia en el area de noticias de gobierno',
	            'department_id' => '1'
	        	],[
	        	'name' => 'Mario Gutierrez',
	            'number' => 'MG91264',
	        	'active' => '0',
	        	'notes' => 'Camarógrafo con mas de 15 años de experiencia en temas de seguridad',
	        	'department_id' => '2'
	        	],[
	        	'name' => 'Luis Azcunaga',
	            'number' => 'LA91648723',
	            'active' => '1',
	            'notes' => 'Operados de audio con especialidad en entretenimiento',
	        	'department_id' => '1'
	        	]
        	]
    	);
    }
}
