<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->id('id_program');
            $table->string('name');
            $table->string('chapter_title')->nullable();
            $table->string('duration')->nullable();
            $table->text('description')->nullable();
            $table->integer('id_rating')->nullable();
            $table->integer('id_genre')->nullable();
            $table->integer('id_language')->nullable();
            $table->integer('id_audio')->nullable();
            $table->integer('id_video')->nullable();
            $table->integer('id_subtitle')->nullable();
            $table->string('date')->nullable();
            $table->integer('sexual_content');
            $table->integer('violence');
            $table->integer('premiere');
            $table->integer('hd');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
