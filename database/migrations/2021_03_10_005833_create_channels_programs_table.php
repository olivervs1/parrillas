<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChannelsProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channels_programs', function (Blueprint $table) {
            $table->id('id_channel_program');
            $table->integer('id_channel');
            $table->integer('id_program');
            $table->string('program_name')->nullable();
            $table->string('chapter_title')->nullable();
            $table->string('duration');
            $table->integer('week');
            $table->string('date');
            $table->string('start');
            $table->text('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('channels_programs');
    }
}
