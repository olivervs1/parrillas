<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->id();
            $table->string('station_name')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('model_number')->nullable();
            $table->string('serial_number')->nullable();
            $table->string('time_zone')->nullable();
            $table->string('dst_adjustment')->nullable();
            $table->string('dst_start_date')->nullable();
            $table->string('dst_start_time')->nullable();
            $table->string('dst_end_date')->nullable();
            $table->string('dst_end_time')->nullable();
            $table->text('channels')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
