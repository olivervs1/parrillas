<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateChannelsProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('channels_programs', function (Blueprint $table) {
            $table->string('cast1')->default('Cast1');
            $table->string('cast2')->default('Cast2');
            $table->string('cast3')->default('Cast3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('channels_programs', function (Blueprint $table) {
            $table->dropColumn('cast1');
            $table->dropColumn('cast2');
            $table->dropColumn('cast3');
        });
    }
}
