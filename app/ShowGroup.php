<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShowGroup extends Model {
    protected $table = 'show_group';

    public function Shows() {
    	return $this->HasMany('App\Program', 'id_show_group', 'id');
    }
}
