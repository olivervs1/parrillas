<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Devices extends Model
{
    public function Channels() {
        return $this->hasMany('App\Channel', 'id_device', 'id');
    }
}
