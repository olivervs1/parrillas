<?php

namespace App\Console\Commands;

use App\ChannelProgram;
use Illuminate\Console\Command;

class castProgramsDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'programs:castDate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $programs = ChannelProgram::where('created_at','>','2021-01-01 00:00:00')->get();

        foreach ($programs as $p) {
            $p->date = date('Y-m-d', strtotime($p->date));
            $p->save();
        }
    }
}
