<?php

namespace App\Http\Controllers;

use App\User;
use App\Plaza;
use App\Channel;
use App\Devices;
use Illuminate\Http\Request;

class ChannelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $channels = Channel::get();
        
        return view('pages.channels.channels-list', compact('channels'));
    }

    public function record(Request $request) {
        $channel = Channel::find($request->id_channel);

        return view('pages.channels.channels-record', compact('channel'));
    }

    public function add() {
        $plazas = Plaza::orderBy('name')->get();
        $devices = Devices::get();

        return view('pages.channels.channels-form', compact('plazas','devices'));
    }

    public function addDB(Request $request) {
        $name = $request->name;
        $id_plaza = $request->id_plaza;
        $principal_channel = $request->principal_channel;
        $secondary_channel = $request->secondary_channel;
        $id_device = $request->id_device;
        $thumbnail = $request->thumbnail;
        
        $exist = Channel::where('name', '=', $name)->where('id_plaza','=',$id_plaza)->first();

        if($exist != NULL) {
            echo json_encode(array('status' => false, 'message' => 'Este Canal ya esta registrado.'));
            exit;
        }

        $channel = new Channel;
        $channel->name = $name;
        $channel->id_plaza = $id_plaza;
        $channel->principal_channel = $principal_channel;
        $channel->secondary_channel = $secondary_channel;
        $channel->id_device = $id_device;

        if($thumbnail != NULL) {
            //get filename with extension
            $filenamewithextension = $thumbnail->getClientOriginalName();
            
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            
            //get file extension
            $extension = $thumbnail->getClientOriginalExtension();
            
            $filepath = public_path('img/channels/');

            if(!is_dir(public_path('/img/channels/'))) {
                mkdir(public_path('/img/channels/'));
            }

            //Upload File
            $thumbnail->move($filepath, $filenamewithextension);

            $channel->thumbnail = $filenamewithextension;
        }
        
        if($channel->save()) {
            echo json_encode(array('status' => true, 'redirectTo' => url('channels')));
            exit;
        } else {
            echo json_encode(array('status' => false, 'message' => 'No se pudo agregar el Canal, por favor intentalo nuevamente.'));
            exit;
        }
    }

    public function edit(Request $request) {
        $channel = Channel::find($request->id_channel);
        $plazas = Plaza::orderBy('name')->get();
        $devices = Devices::get();

        return view('pages.channels.channels-form', compact('channel', 'plazas', 'devices'));   
    }

    public function editDB(Request $request) {
        $channel = Channel::find($request->id_channel);
        $name = $request->name;
        $id_plaza = $request->id_plaza;
        $principal_channel = $request->principal_channel;
        $secondary_channel = $request->secondary_channel;
        $id_device = $request->id_device;
        $thumbnail = $request->thumbnail;
        
        if($channel instanceof Channel) {
            $channel->name = $request->name;
            $channel->id_plaza = $id_plaza;
            $channel->principal_channel = $principal_channel;
            $channel->secondary_channel = $secondary_channel;
            $channel->id_device = $id_device;

            if($thumbnail != NULL) {
                //get filename with extension
                $filenamewithextension = $thumbnail->getClientOriginalName();
                
                //get filename without extension
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                
                //get file extension
                $extension = $thumbnail->getClientOriginalExtension();
                
                $filepath = public_path('img/channels/');

                if(!is_dir(public_path('/img/channels/'))) {
                    mkdir(public_path('/img/channels/'));
                }

                //Upload File
                $thumbnail->move($filepath, $filenamewithextension);

                $channel->thumbnail = $filenamewithextension;
            }
            
            if($channel->save()) {
                echo json_encode(array('status' => true, 'redirectTo' => url('channels')));
                exit;  
            }
        } else {
            echo json_encode(array('status' => false, 'message' => 'No se pudo editar el Canal, por favor intentalo nuevamente.'));
            exit;
        }
    }
}
