<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Channel;
use App\ChannelProgram;
use Carbon\Carbon;

class ExternalController extends Controller
{
    public function schedule(Channel $channel){
        $now = Carbon::now();
        /**
        $weekStartDate = $now->startOfWeek()->format('Y-m-d');
        $weekEndDate = $now->endOfWeek()->format('Y-m-d');

        $programs = ChannelProgram::where('id_channel',$channel->id_channel)
            ->where('date','>=',$weekStartDate)
            ->where('date','<=',$weekEndDate)
            ->orderBy('date','asc')
            ->orderBy('start','asc')
            ->get();
        **/

        $monday=$now->startOfWeek()->format('Y-m-d');
        $tuesday=$now->startOfWeek()->addDays(1)->format('Y-m-d');
        $wednesday=$now->startOfWeek()->addDays(2)->format('Y-m-d');
        $thursday=$now->startOfWeek()->addDays(3)->format('Y-m-d');
        $friday=$now->startOfWeek()->addDays(4)->format('Y-m-d');
        $saturday=$now->startOfWeek()->addDays(5)->format('Y-m-d');
        $sunday=$now->startOfWeek()->addDays(6)->format('Y-m-d');

        $programs_monday = ChannelProgram::where('id_channel',$channel->id_channel)
            ->where('date','=',$monday)
            ->orderBy('start')
            ->get();
        $programs_tuesday = ChannelProgram::where('id_channel',$channel->id_channel)
            ->where('date','=',$tuesday)
            ->orderBy('start')
            ->get();
        $programs_wednesday = ChannelProgram::where('id_channel',$channel->id_channel)
            ->where('date','=',$wednesday)
            ->orderBy('start')
            ->get();
        $programs_thursday = ChannelProgram::where('id_channel',$channel->id_channel)
            ->where('date','=',$thursday)
            ->orderBy('start')
            ->get();
        $programs_friday = ChannelProgram::where('id_channel',$channel->id_channel)
            ->where('date','=',$friday)
            ->orderBy('start')
            ->get();
        $programs_saturday = ChannelProgram::where('id_channel',$channel->id_channel)
            ->where('date','=',$saturday)
            ->orderBy('start')
            ->get();
        $programs_sunday = ChannelProgram::where('id_channel',$channel->id_channel)
            ->where('date','=',$sunday)
            ->orderBy('start')
            ->get();




        return view('external.schedule',compact('channel','programs_monday','programs_tuesday','programs_wednesday','programs_thursday','programs_friday','programs_saturday','programs_sunday'));
    }
}
