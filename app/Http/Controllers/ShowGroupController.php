<?php

namespace App\Http\Controllers;

use App\ShowGroup;
use Illuminate\Http\Request;

class ShowGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $show_groups = ShowGroup::get();

        return view('pages.show-group.show-group-list', compact('show_groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('pages.show-group.show-group-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $name = $request->name;

        $exist = ShowGroup::where('name','=', $name)->first();

        if($exist instanceof ShowGroup) {
            echo json_encode(array('status' => false, 'message' => 'Este nombre de Grupo de programas ya existe.'));
            exit;
        }

        $show_group = new ShowGroup;
        $show_group->name = $name;

        if($show_group->save()) {
            echo json_encode(array('status' => true, 'message' => 'El Grupo de programa ha sido agregado exitosamnete.', 'redirectTo' => url('/show-group'))); 
            exit;
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $show_group = ShowGroup::find($id);

        if($show_group instanceof ShowGroup) {
            return view('pages.show-group.show-group-record', compact('show_group'));
        }

        return redirect(url('/show-group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $show_group = ShowGroup::find($id);

        if($show_group instanceof ShowGroup) {
            return view('pages.show-group.show-group-form', compact('show_group'));
        }

        return redirect(url('/show-group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $show_group = ShowGroup::find($id);

        if($show_group instanceof ShowGroup) {
            $show_group->name = $request->name;
            if($show_group->save()) {
                echo json_encode(array('status' => true, 'message' => 'El Grupo de programa ha sido agregado exitosamnete.', 'redirectTo' => url('/show-group'))); 
                exit;
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
