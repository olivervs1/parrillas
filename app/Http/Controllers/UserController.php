<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $users = User::get();
        
        return view('pages.users.users-list', compact('users'));
    }

    public function edit(Request $request) {
        $user = User::find($request->id_user);
        
        return view('pages.users.users-form', compact('user'));
    }
}
