<?php

namespace App\Http\Controllers;

use App\User;
use App\Genre;
use App\Audio;
use App\Rating;
use App\Program;
use App\Plaza;
use App\Channel;
use App\Subtitle;
use App\Language;
use App\ShowGroup;
use App\ChannelProgram;
use App\Exports\ProgramsExport;
use App\Exports\NielsenExport;
use Illuminate\Http\Request;

use \PDF;
use Maatwebsite\Excel\Facades\Excel;


class ProgramController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $programs = Program::where('status','=',1)->get();
        
        return view('pages.programs.programs-list', compact('programs'));
    }

    public function show($id) {
        $program = Program::find($id);

        if($program instanceof Program) {
            return view('pages.programs.programs-record', compact('program'));    
        }
        
        return redirect(url('/programs'));
    }

    public function add() {
        $ratings = Rating::orderBy('name')->get();
        $genres = Genre::orderBy('name')->get();
        $languages = Language::orderBy('name')->get();
        $audios = Audio::orderBy('name')->get();
        $subtitles = Subtitle::orderBy('name')->get();
        $plazas = Plaza::get();
        $show_groups = ShowGroup::get();

        return view('pages.programs.programs-form', compact('ratings', 'genres', 'audios', 'languages', 'subtitles', 'plazas', 'show_groups'));
    }

    public function addDB(Request $request) {
        $name = $request->name;
        $guide_name = $request->guide_name;
        $duration = $request->duration;
        $color = $request->color;
        $description = $request->description;
        $id_rating = $request->id_rating;
        $id_genre = $request->id_genre;
        $id_language = $request->id_language;
        $id_audio = $request->id_audio;
        $id_subtitle = $request->id_subtitle;
        $sexual_content = $request->sexual_content;
        $violence = $request->violence;
        $premiere = $request->premiere;
        $hd = $request->hd;
        $production_type = $request->production_type;
        $id_plaza = $request->id_plaza;
        $id_show_group = $request->id_show_group;
        $cast1 = $request->cast1;
        $cast2 = $request->cast2;
        $cast3 = $request->cast3;
        $release_year = $request->release_year;
        $link = $request->link;
        $exist = Program::where('name', '=', $name)->first();

        if($exist != NULL) {
            echo json_encode(array('status' => false, 'message' => 'Este Programa ya esta registrado.'));
            exit;
        }

        $program = new Program;
        $program->name = $name;
        $program->guide_name = $guide_name;
        $program->duration = $duration;
        $program->description = $description;
        $program->color = $color;
        $program->id_rating = $id_rating;
        $program->id_genre = $id_genre;
        $program->id_language = $id_language;
        $program->id_audio = $id_audio;
        $program->id_subtitle = $id_subtitle;
        $program->sexual_content = $sexual_content;
        $program->violence = $violence;
        $program->premiere = $premiere;
        $program->hd = $hd;
        $program->production_type = $production_type;
        $program->id_plaza = $id_plaza;
        $program->id_show_group = $id_show_group;
        $program->cast1 = $cast1;
        $program->cast2 = $cast2;
        $program->cast3 = $cast3;
        $program->link = $link;
        $program->release_year = $release_year;
                
        if($program->save()) {
            echo json_encode(array('status' => true, 'redirectTo' => url('programs')));
            exit;
        } else {
            echo json_encode(array('status' => false, 'message' => 'No se pudo agregar el Programa, por favor intentalo nuevamente.'));
            exit;
        }
    }

    public function edit(Request $request) {
        $program = Program::find($request->id_program);
        $ratings = Rating::orderBy('name')->get();
        $genres = Genre::orderBy('name')->get();
        $languages = Language::orderBy('name')->get();
        $audios = Audio::orderBy('name')->get();
        $subtitles = Subtitle::orderBy('name')->get();
        $plazas = Plaza::get();
        $show_groups = ShowGroup::get();

        return view('pages.programs.programs-form', compact('program','ratings', 'genres', 'audios', 'languages', 'subtitles', 'plazas', 'show_groups'));   
    }

    public function editDB(Request $request) {
        $program = Program::find($request->id_program);
        
        $name = $request->name;
        $guide_name = $request->guide_name;
        $duration = $request->duration;
        $color = $request->color;
        $description = $request->description;
        $id_rating = $request->id_rating;
        $id_genre = $request->id_genre;
        $id_language = $request->id_language;
        $id_audio = $request->id_audio;
        $id_subtitle = $request->id_subtitle;
        $sexual_content = $request->sexual_content;
        $violence = $request->violence;
        $premiere = $request->premiere;
        $hd = $request->hd;
        $production_type = $request->production_type;
        $id_plaza = $request->id_plaza;
        $id_show_group = $request->id_show_group;
        $cast1 = $request->cast1;
        $cast2 = $request->cast2;
        $cast3 = $request->cast3;
        $link = $request->link;
        $release_year = $request->release_year;

        if($program instanceof Program) {
            $program->name = $name;
            $program->guide_name = $guide_name;
            $program->duration = $duration;
            $program->color = $color;
            $program->description = $description;
            $program->id_rating = $id_rating;
            $program->id_genre = $id_genre;
            $program->id_language = $id_language;
            $program->id_audio = $id_audio;
            $program->id_subtitle = $id_subtitle;
            $program->sexual_content = $sexual_content;
            $program->violence = $violence;
            $program->premiere = $premiere;
            $program->hd = $hd;
            $program->production_type = $production_type;
            $program->id_plaza = $id_plaza;
            $program->id_show_group = $id_show_group;
            $program->cast1 = $cast1;
            $program->cast2 = $cast2;
            $program->cast3 = $cast3;
            $program->link = $link;
            $program->release_year = $release_year;
            
            if($program->save()) {
                echo json_encode(array('status' => true, 'redirectTo' => url('programs')));
                exit;  
            }
        } else {
            echo json_encode(array('status' => false, 'message' => 'No se pudo editar el Programa, por favor intentalo nuevamente.'));
            exit;
        }
    }

    public function addToCalendar(Request $request) {
        $id_channel_program = $request->id_channel_program;
        $id_program = $request->id_program;
        $id_channel = $request->id_channel;
        $date = $request->date;
        $program_name = $request->program_name;
        $chapter_title = $request->chapter_title;
        $duration = $request->duration;
        $description = $request->description;
        $start = $request->begins;
        $cast1 = $request->cast1 != "" ? $request->cast1 : "Cast1";
        $cast2 = $request->cast2 != "" ? $request->cast2 : "Cast2";
        $cast3 = $request->cast3 != "" ? $request->cast3 : "Cast3";
        $week = date('W', strtotime($date));
        $year = date('Y', strtotime($date));
        $days = explode(',', $request->days);
        //$end = date('H:i', strtotime('+ 90 minutes', strtotime($start))); exit;

        $updateIn = $request->updateIn;

        $day = date('w', strtotime($date)) == 0 ? 7 : date('w', strtotime($date));
        $show_date = "";

        $updateChannels = explode(',', $updateIn);

        if(isset($request->is_massive)) {            
            if($program_name == "" || $program_name == NULL || $program_name == "NULL") {
                $program = Program::find($id_program);
                $program_name = $program->name;
            }

            $cP = new ChannelProgram;
            $cP->id_program = $id_program;
            $cP->id_channel = $id_channel;
            $cP->program_name = $program_name;
            $cP->chapter_title = $chapter_title;
            $cP->description = $description;
            $cP->date = $show_date;
            $cP->duration = $duration;
            $cP->start = $start;
            $cP->week = $week;
            $cP->year = $year;
            $cP->cast1 = $cast1;
            $cP->cast2 = $cast2;
            $cP->cast3 = $cast3;
            $cP->save();

            foreach ($days as $d) {
                if($d < $day) {
                    $diff = $day - $d;
                    $show_date = date('Y-m-d', strtotime('-' . $diff . ' days', strtotime($date)));
                } else if($d == $day) {
                    $show_date = $date;
                } else {
                    $diff = $d - $day;
                    $show_date = date('Y-m-d', strtotime('+' . $diff . ' days', strtotime($date)));
                }

                if($updateIn != NULL) {
                    $updateChannels = explode(',', $updateIn);

                    foreach ($updateChannels as $ch) {
                        $chObj = new ChannelProgram;                        
                        $chObj->id_program = $cP->id_program;
                        $chObj->program_name = $program_name;
                        $chObj->chapter_title = $chapter_title;
                        $chObj->id_channel = $ch;
                        $chObj->description = $description;
                        $chObj->date = $show_date;
                        $chObj->duration = $duration;
                        $chObj->start = $start;
                        $chObj->week = $week;
                        $chObj->year = $year;
                        $chObj->cast1 = $cast1;
                        $chObj->cast2 = $cast2;
                        $chObj->cast3 = $cast3;
                        $chObj->save();
                    }
                }
            }

            echo json_encode(array('status' => true, 'message' => 'El programa ha sido agregado correctamente.'));
            exit;
        } else {
            if($id_channel_program == 0) {
                foreach ($days as $d) {
                    if($d < $day) {
                        $diff = $day - $d;
                        $show_date = date('Y-m-d', strtotime('-' . $diff . ' days', strtotime($date)));
                    } else if($d == $day) {
                        $show_date = $date;
                    } else {
                        $diff = $d - $day;
                        $show_date = date('Y-m-d', strtotime('+' . $diff . ' days', strtotime($date)));
                    }

                    $cP = new ChannelProgram;
                    $cP->id_program = $id_program;
                    $cP->id_channel = $id_channel;
                    $cP->program_name = $program_name;
                    $cP->chapter_title = $chapter_title;
                    $cP->description = $description;
                    $cP->date = $show_date;
                    $cP->duration = $duration;
                    $cP->start = $start;
                    $cP->week = $week;
                    $cP->year = $year;
                    $cP->cast1 = $cast1;
                    $cP->cast2 = $cast2;
                    $cP->cast3 = $cast3;
                    $cP->save();
                }

                echo json_encode(array('status' => true, 'message' => 'El programa ha sido agregado correctamente.'));
                exit;
            } else {
                $cP = ChannelProgram::find($id_channel_program);
                
                if(!$cP instanceof ChannelProgram) {
                    $cP = new ChannelProgram;    
                }
                
                $cP->id_program = $id_program;
                $cP->id_channel = $id_channel;
                //$cP->program_name = $program_name;
                $cP->chapter_title = $chapter_title;
                $cP->description = $description;
                $cP->date = $date;
                $cP->duration = $duration;
                $cP->start = $start;
                $cP->week = $week;
                $cP->year = $year;
                $cP->cast1 = $cast1;
                $cP->cast2 = $cast2;
                $cP->cast3 = $cast3;
                
                if($cP->save()) {

                    if($updateIn != NULL) {
                        $updateChannels = explode(',', $updateIn);

                        foreach ($updateChannels as $ch) {
                            $cP = ChannelProgram::find($ch);                        
                            $cP->chapter_title = $chapter_title;
                            $cP->description = $description;
                            $cP->date = $date;
                            $cP->duration = $duration;
                            $cP->start = $start;
                            $cP->week = $week;
                            $cP->year = $year;
                            $cP->cast1 = $cast1;
                            $cP->cast2 = $cast2;
                            $cP->cast3 = $cast3;
                            $cP->save();
                        }
                    }
                    echo json_encode(array('status' => true, 'message' => 'El programa ha sido agregado correctamente.'));
                    exit;
                }
            }
        }
    }

    public function updateCalendarEvent(Request $request) {
        $id_channel_program = $request->id_channel_program;
        $channelProgram = ChannelProgram::find($id_channel_program);

        if($channelProgram instanceof ChannelProgram) {
            if(isset($request->duration)) {
                $channelProgram->duration = $request->duration;
            }    

            if(isset($request->start)) {
                $channelProgram->start = $request->start;
            }    

            if(isset($request->program_name)) {
                $channelProgram->program_name = $request->program_name;
            }   

            if(isset($request->chapter_title)) {
                $channelProgram->chapter_title = $request->chapter_title;
            }  

            if(isset($request->date)) {
                $channelProgram->date = $request->date;
                
                $week = date('W', strtotime($request->date));
                $year = date('Y', strtotime($request->date));
                $channelProgram->week = $week;
                $channelProgram->year = $year;
            }

            if(isset($request->description)) {
                $channelProgram->description = $request->description;
            }   

            if($channelProgram->save()) {
                echo json_encode(array('status' => true, 'message' => 'El programa ha sido actualizado correctamente.'));
                exit;
            }
        }

        echo json_encode(array('status' => true, 'message' => 'Hubo un problema, el programa no ha sido actualizado.'));
        exit;
    }

    public function removeCalendarEvent(Request $request) {
        $id_channel_program = $request->id_channel_program;
        $id_program = $request->id_program;
        $id_channel = $request->id_channel;
        $date = $request->date;

        if($request->removeIn != NULL) {
            $removeIn = explode(',', $request->removeIn);

            foreach ($removeIn as $ch) {
                $cP = ChannelProgram::find($ch);
                
                if($cP instanceof ChannelProgram) {
                    $cP->delete();
                }                
            }
        }

        $cP = ChannelProgram::find($id_channel_program);
        
        if($cP instanceof ChannelProgram) {
            if($cP->delete()) {
                echo json_encode(array('status' => true)); exit;
                exit;
            }
        }

        echo json_encode(array('status' => false, 'message' => 'Hubo un error al intentar elimianr este programa.')); exit;
        exit;
    }

    public function cloneWeek(Request $request) {
        $from_channel = $request->from_channel;
        $from_week = explode('-', $request->from_week);
        $to_channel = $request->to_channel;
        $to_week_data = $request->to_week;
        $to_week = explode('-', $to_week_data)[0];
        $to_year = explode('-', $to_week_data)[1];

        $weekData = ChannelProgram::where('week', $from_week[0])->where('id_channel', $from_channel)->where('year', $from_week['1'])->first();
        ChannelProgram::where('id_channel','=', $to_channel)->where('week','=', $to_week)->where('year','=', $to_year)->delete();

        $programs = ChannelProgram::where('id_channel','=', $from_channel)->where('week','=', $from_week)->where('year','=', $weekData->year)->get();
        
        $channels = collect([]);

        foreach ($programs as $p) {
            $cp = new \stdClass();
            $cp->id_channel = $to_channel;
            $cp->id_program = $p->id_program;
            $cp->program_name = $p->program_name;
            $cp->chapter_title = $p->chapter_title;
            $cp->duration = $p->duration;
            $cp->start = $p->start;
            $cp->description = $p->description;
            $cp->cast1 = $p->cast1;
            $cp->cast2 = $p->cast2;
            $cp->cast3 = $p->cast3;

            $timestamp = mktime( 0, 0, 0, 1, 1,  date('Y') ) + ( $to_week * 7 * 24 * 60 * 60 );
            $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
            $from = date( 'Y-m-d', $timestamp_for_monday);
            $date = $from;
            switch (date('w', strtotime($p->date))) {
                case 0:
                    $date = date('Y-m-d', strtotime('+6 days', strtotime($from)));   
                    break;
                case 1:
                    $date = $from;
                    break;
                case 2:
                    $date = date('Y-m-d', strtotime('+1 days', strtotime($from)));   
                    break;
                case 3:
                    $date = date('Y-m-d', strtotime('+2 days', strtotime($from)));   
                    break;
                case 4:
                    $date = date('Y-m-d', strtotime('+3 days', strtotime($from)));   
                    break;
                case 5:
                    $date = date('Y-m-d', strtotime('+4 days', strtotime($from)));   
                    break;
                case 6:
                    $date = date('Y-m-d', strtotime('+5 days', strtotime($from)));   
                    break;
                default:
                    $date = $from;
                    break;
            }

            $cp->date = $date;
            $cp->week = date('W', strtotime($date));
            $cp->year = date('Y', strtotime($date));
            // $cp->save();
            $channels->push((array)$cp);
        }

        ChannelProgram::insert($channels->toArray());

        echo json_encode(array('status' => true, 'message' => 'La semana ha sido clonada correctamente.'));
        exit;
    }

    public function exportWeek(Request $request) {
        $id_plaza = $request->id_plaza;
        $id_channel = $request->id_channel;
        $week_data = $request->week;
        $week = explode('-', $week_data)[0];
        $year = explode('-', $week_data)[1];
        $programs = array();

        $programs = ChannelProgram::where('id_channel','=', $id_channel)->where('week','=', $week)->where('year','=', $year)->orderBy('date','asc')->orderBy('start','asc')->get();

        foreach($programs as $p) {
            $p->date = date('Y-m-d', strtotime($p->date));
            $p->save();
        }

        $programs = ChannelProgram::where('id_channel','=', $id_channel)->where('week','=', $week)->where('year','=', $year)->orderBy('id_channel', 'asc')->orderBy('date','asc')->orderBy('start','asc')->get();

        $totalMinutes = 0;
        foreach($programs as $p) {
            $totalMinutes += $p->duration;
        }

        /*if($totalMinutes < (60 * 24) * 7) {
            echo json_encode(array('status' => false, 'message' => 'La parrilla aún cuenta con horarios libres, por favor validar antes de exportar el archivo.')); 
            exit;
        }*/

        $channel = Channel::find($id_channel);

        $plaza = Plaza::find($id_plaza);

        $exportFile = $request->epg_name;

        $file = "";

        switch ($exportFile) {
            case "EPG 1":
                
                if($channel->Device == NULL) {
                    echo json_encode(array('status' => false, 'message' => 'Aún no se ha seleccionado el equipo al que pertenece el canal.'));
                    exit;
                }

                $channels  = $channel->Device->Channels;

                $programs = array();
                $channelIds = array();
                $channelsList = array();
                foreach($channels as $c) { 
                    $channelIds[] = $c->id_channel;
                    $channelsList[] = $c->principal_channel . '.' . $c->secondary_channel;
                }

                $to = date('Y-m-d', strtotime('+' . $request->days . ' days', strtotime($request->from)));
                $from = date('Y-m-d', strtotime('-1 day', strtotime($request->from)));

                $programs = ChannelProgram::whereIn('id_channel', $channelIds)->whereBetween('date', [$from, $to])->orderBy('id_channel', 'asc')->orderBy('date','asc')->orderBy('start','asc')->get();

                foreach($programs as $k => $p) {
                    $p->date = date('Y-m-d', strtotime($p->date));
                    $p->save();

                    if($p->date >= $request->from) {
                        continue;
                    } else {
                        $mins = explode(':', $p->start);
                        $hourMins = $mins[0] * 60;
                        $total = $hourMins + $mins[1] + $p->duration;
                        $hours = floor($total / 60);
                        $mins = $total - ($hours * 60);
                        
                        if($hours <= 24) {
                            unset($programs[$k]);
                        } else {
                            $p->date = $request->from;
                            $p->start = '00:00';
                            $p->duration = (($hours - 24) * 60) + $mins;
                        }
                    }
                }

                $epg = $this->exportDishEPG($programs, $channel);
                $file = asset('exports/' . date('d_m_Y') .' - '. $channel->name . '.dat');

                echo json_encode(array('status' => true, 'message' => 'El archivo ha sido generado correctamente, con la información de los siguientes canáles: ' . implode(', ', $channelsList), 'file' => $file));
                exit;    

                break;
            case "AVG":
                $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
                $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
                $fromDate = date( 'd-m-Y', $timestamp_for_monday);
                $fromDay = date( 'd', $timestamp_for_monday);
                $fromMonth = date( 'm', $timestamp_for_monday);
                $toDate = date('d-m-Y', strtotime('+6 days', strtotime($fromDate)));
                $toDay = date('d', strtotime($fromDate));
                $toMonth = date('m', strtotime($fromDate));
                $toYear = date('y', strtotime($fromDate));

                $filename = $channel->name . ' ' . $plaza->shortname . 'DEL ' . $fromDay . ' AL ' . $toDay .' ' . $toMonth . ' ' . $toYear . '.txt';
                
                $epg = $this->exportTXT($programs, $channel, $filename);

                $file = asset('exports/' . $filename);
                break;
            case "XLS":
                $filename = time() . '_MMTV.xls';
                
                $epg = $this->exportXLS($programs, $channel, $filename);

                $file = asset('exports/' . $filename);
                break;
            case "XLS2":
                $filename = time() . '_MMTV.xlsx';
                
                //$epg = $this->exportXLS2($programs, $channel, $filename);

                $file = asset('exports/' . $filename);
                $file = url('/programs/render-nielsen/' . $channel->id_channel . '/' . $week_data);
                break;
            default:
                $epg = $this->exportDishEPG($programs, $channel);
                $file = asset('exports/' . date('d_m_Y') .' - '. $channel->name . '.dat');
                break;
        }

        if($file != "") {
            echo json_encode(array('status' => true, 'message' => 'El archivo ha sido generado correctamente.', 'file' => $file));
            exit;    
        }            

        echo json_encode(array('status' => false, 'message' => 'No se ha asignado EPG en la plaza.'));
        exit;
    }

    private function exportDishEPG($programs, $channel) {
        if(!is_dir(env('FILESYSTEM') . 'public/exports/')) {
            //mkdir(env('FILESYSTEM') . 'public/exports/');
        }

        $file = env('FILESYSTEM') . 'public/exports/' . date('d_m_Y') .' - '. $channel->name . '.dat';
        $epg = fopen($file, 'w');
        fwrite($epg, '# MAPPER' . PHP_EOL);
        fwrite($epg, '# Multimedios Guia de programacion' . PHP_EOL);
        fwrite($epg, '# FECHA ' . date('d m Y') . PHP_EOL);
        fwrite($epg, '#' . PHP_EOL);
        fwrite($epg, '[STATION_NAME]' . PHP_EOL);
        fwrite($epg, $channel->Device->station_name . PHP_EOL);
        fwrite($epg, '[IP_ADDRESS]' . PHP_EOL);
        fwrite($epg, $channel->Device->ip_address . PHP_EOL);
        fwrite($epg, '[MODEL_NUMBER]' . PHP_EOL);
        fwrite($epg, $channel->Device->model_number . PHP_EOL);
        fwrite($epg, '[SERIAL_NUMBER]' . PHP_EOL);
        fwrite($epg, $channel->Device->serial_number . PHP_EOL);
        fwrite($epg, '[TIME_ZONE]' . PHP_EOL);
        fwrite($epg, $channel->Device->time_zone . PHP_EOL);
        fwrite($epg, '[DST_ADJUSTMENT]' . PHP_EOL);
        fwrite($epg, $channel->Device->dst_adjustment . PHP_EOL);
        fwrite($epg, '[DST_START_DATE]' . PHP_EOL);
        fwrite($epg, $channel->Device->dst_start_date . PHP_EOL);
        fwrite($epg, '[DST_START_TIME]' . PHP_EOL);
        fwrite($epg, $channel->Device->dst_start_time . PHP_EOL);
        fwrite($epg, '[DST_END_DATE]' . PHP_EOL);
        fwrite($epg, $channel->Device->dst_end_date . PHP_EOL);
        fwrite($epg, '[DST_END_TIME]' . PHP_EOL);
        fwrite($epg, $channel->Device->dst_end_time . PHP_EOL);
        fwrite($epg, '[ATSC_SERVICES]' . PHP_EOL);
        
        $channelKey = array();
        foreach (json_decode($channel->Device->channels) as $k => $d) {
            $channelKey[$d->principal_channel . '_'.$d->secondary_channel] = $k;

            fwrite($epg, $d->channel_name . "\t" . $d->principal_channel . "\t" . $d->secondary_channel . PHP_EOL);    
        }
        
        fwrite($epg, '[PSIP_EVENTS]' . PHP_EOL);
        fwrite($epg, PHP_EOL);
        foreach ($programs as $p) {

            if(!isset($channelKey[$p->Channel->principal_channel . '_'.$p->Channel->secondary_channel])) {
                echo json_encode(array('status' => false, 'message' => 'El canal ' . $p->Channel->principal_channel .'.'.$p->Channel->secondary_channel . ' no se encuentrá agregado en los Canales del equipo ' . $channel->Device->station_name)); 
                exit;
            }

            $date = date('Y/m/d', strtotime($p->date));

            if($p->Program->Language == NULL) {
                echo json_encode(array('status' => false, 'message' => "El programa " . $p->program_name . ' no tiene asignada una opción de Lenguaje válida.'));
                exit;
            }

            if($p->Program->Rating == NULL) {
                echo json_encode(array('status' => false, 'message' => "El programa " . $p->program_name . ' no tiene asignada una opción de Clasificación válida.'));
                exit;
            }

            $end = date('H:i:s', strtotime('+' . $p->duration . ' minutes', strtotime($p->start)));

            $programName = $p->program_name;
            if($p->chapter_title != "") {
                $programName .= ' - ' . $p->chapter_title;
            }

            $fulldate = date('d-m-Y H:i:s', strtotime($date . ' ' . $p->start));

            $jet_lag = $channel->Plaza->jet_lag != "" || $channel->Plaza->jet_lag != 0 ? $channel->Plaza->jet_lag : "";

            if($p->duration > 60) {
                $hours = floor($p->duration / 60); 
                $minutes = $p->duration - ($hours * 60);
                $minutes = str_pad($minutes, 2 ,'0', STR_PAD_LEFT);
                $days = $hours / 24;

                if($days > 1) {
                    for ($i=0; $i < $days; $i++) { 
                        $nDate = date('Y/m/d', strtotime('+' . $i . ' days', strtotime($fulldate)));
                        $start = date('H:i', strotime($p->start));

                        if($jet_lag != "") {
                            $nDate = date('Y/m/d', strtotime('+' . $i . ' days ' . $jet_lag . ' hours', strtotime($fulldate)));
                            $start = date('H:i', strtotime($jet_lag . ' hours', strotime($p->start)));
                        }

                        $remain = $p->duration - ($i * (24 * 60));

                        $end = "";

                        if($remain > (24 * 60)) {
                            $end = '24:00:00';
                        } else {
                            $hoursRemain = floor($remain / 60);
                            $minutes = $remain - ($hoursRemain * 60);
                            $minutes = str_pad($minutes, 2,'0', STR_PAD_LEFT);
                            $end = $hoursRemain . ':' . $minutes . ':00';
                        }
                        
                        fwrite($epg,$channelKey[$p->Channel->principal_channel . '_'.$p->Channel->secondary_channel] . "\t" . $nDate . "\t" . $start . "\t" . $end . "\t" . $p->Program->Language->name . "\t" . 'NR' . "\t" . '[' . $p->Program->Rating->name .']' . " " . utf8_decode($programName) . PHP_EOL);        
                        
                    }
                } else {
                    $nDate = date('Y/m/d', strtotime($fulldate));
                    $start = date('H:i', strtotime($p->start));
                    if($jet_lag != "") {
                        $nDate = date('Y/m/d', strtotime($jet_lag . ' hours', strtotime($fulldate)));
                        $start = date('H:i', strtotime($jet_lag . ' hours', strtotime($p->start)));
                    }
                    
                    fwrite($epg,$channelKey[$p->Channel->principal_channel . '_'.$p->Channel->secondary_channel] . "\t" . $nDate . "\t" . $start . "\t" . $p->duration . "\t" . $p->Program->Language->name . "\t" . 'NR' . "\t" . '[' . $p->Program->Rating->name .']' . " " . utf8_decode($programName) . PHP_EOL);        
                }
            } else {
                $hours = floor($p->duration / 60); 
                $minutes = $p->duration - ($hours * 60);
                $minutes = str_pad($minutes, 2 ,'0', STR_PAD_LEFT);
                $end = $hours . ':' . $minutes . ':00';

                $start = date('H:i', strtotime($p->start));

                $nDate = date('Y/m/d', strtotime($fulldate));

                if($jet_lag != "") {
                    $nDate = date('Y/m/d', strtotime($jet_lag . ' hours', strtotime($fulldate)));
                    $start = date('H:i', strtotime($jet_lag . ' hours', strtotime($p->start)));
                }

                fwrite($epg,$channelKey[$p->Channel->principal_channel . '_'.$p->Channel->secondary_channel] . "\t" . $nDate . "\t" . $start . "\t" . $p->duration . "\t" . $p->Program->Language->name . "\t" . 'NR' . "\t" . '[' . $p->Program->Rating->name .']' . " " . utf8_decode($programName) . PHP_EOL);        
            } 
        }
        fclose($epg);

        return true;
    }

    public function getChannels(Request $request) {
        $id_plaza = $request->id_plaza;

        $plaza = Plaza::find($id_plaza);

        if($plaza instanceof Plaza) {
            echo json_encode(array('status' => true, 'channels' => $plaza->channels ));
            exit;    
        }

        echo json_encode(array('status' => false, 'message' => 'No se pudieron obtener los canales de esta plaza.'));
        exit;
    }   

    public function getWeeks(Request $request) {
        $id_channel = $request->id_channel;
        $channel = Channel::find($id_channel);

        if($channel instanceof Channel) {
            $availableWeeks = ChannelProgram::groupBy('week')->where('id_channel','=', $id_channel)->where('year','=', date('Y'))->orderBy('week','asc')->get();
            $dataWeeks = array();

            foreach ($availableWeeks as $w) {
                $timestamp = mktime( 0, 0, 0, 1, 1,  $w->year ) + ( $w->week * 7 * 24 * 60 * 60 );
                $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
                $from = date( 'd-m-Y', $timestamp_for_monday);
                $dataWeeks[$w->week] = array('week' => $w->week, 'from' => $from, 'to' => date('d-m-Y', strtotime('+6 days', strtotime($from))), 'value' => $w->week . '-' . date('Y', strtotime($from)));
            }
            
            echo json_encode(array('status' => true, 'weeks' => $dataWeeks));
            exit;
        }
        
        echo json_encode(array('status' => false, 'message' => 'No se pudieron obtener semanas para este canal.'));
        exit;
    }

    public function createExcel(Request $request) {
        $id_channel = $request->id_channel;
        $week_data = $request->week;
        $week = explode('-', $week_data)[0];
        $year = explode('-', $week_data)[1];
        $channel = Channel::find($id_channel); 

        if($channel instanceof Channel) {
            $programs = ChannelProgram::where('id_channel','=', $id_channel)->where('week','=', $week)->where('year','=', $year)->orderBy('date','asc')->get();
            
            foreach ($availableWeeks as $w) {
                $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
                $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
                $from = date( 'd-m-Y', $timestamp_for_monday);
                $dataWeeks[$w->week] = array('week' => $w->week, 'from' => $from, 'to' => date('d-m-Y', strtotime('+6 days', strtotime($from))));
            }
            
            echo json_encode(array('status' => true, 'weeks' => $dataWeeks));
            exit;
        }
        
        echo json_encode(array('status' => false, 'message' => 'No se seleccionó un canal válido.'));
        exit;
    }

    public function createPDF(Request $request) {
        $id_channel = $request->id_channel;
        $week_data = $request->week;
        $week = explode('-', $request->week)[0];
        $year = explode('-', $request->week)[1];
        
        $channel = Channel::find($id_channel);

        $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
        $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
        
        $monday = date( 'd-m-Y', $timestamp_for_monday );
        $mondayDay = date('d',$timestamp_for_monday );

        $meses = array('', 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
        $firstMonth = date( 'n', $timestamp_for_monday );

        $sunday = date('d-m-Y', strtotime('+6 days', strtotime($monday)));
        $sundayDay = date('d',strtotime($sunday) );
        $lastMonth = date( 'n', strtotime($sunday));

        $programsArr = array();
        if($channel instanceof Channel) {
            $programs = ChannelProgram::where('id_channel','=', $id_channel)->where('week','=', $week)->where('year','=', $year)->orderBy('start','asc')->get();
            
            $programsConsult = array();
            $intervals_of_15 = 0;
            foreach($programs as $p) {
                $day = date('w', strtotime($p->date)) == 0 ? 7 : date('w', strtotime($p->date));
                $programsConsult[$day][$p->id_program][$p->start] = true;

                if($p->duration / 15 == 1) {
                    $intervals_of_15++;
                }
            }

            $interval = $intervals_of_15 > 0 ? 15 : 30;
            foreach ($programs as $p) {
                $rows = 1;
                $day = date('w', strtotime($p->date)) == 0 ? 7 : date('w', strtotime($p->date));
                $title = $p->chapter_title != "" ? $p->program_name .' - ' . $p->chapter_title : $p->program_name;
                $rows = ceil($p->duration / $interval);
                $start = $p->start;

                $programsArr[$day][$start] = array('title' => $title, 'rowspan' => $rows, 'start' => $start);

                $hours = $p->duration / 60;
                $startArr = explode(':', $start);

                $hours = $hours + $startArr[0];

                if($hours >= 24) {
                    if($day != 7) {
                        $newDay = $day+1;
                        $diff = $hours - 24;
                        $mins = $startArr[1];

                        if($diff > 0 || $mins != "00") {
                            if($diff > 0) {
                                $divider = $intervals_of_15 > 0 ? 4 : 2;
                                $rows = $diff * $divider;
                            } else {
                                $rows = $mins / $interval;
                                $mins = "00";
                            }

                            if($mins != "00") {
                                $rows = $rows + ($mins / $interval);   
                            }

                            $programsArr[$newDay]['00:00'] = array('title' => $title, 'rowspan' => $rows, 'start' => '00:00');
                        }
                    }
                }
            }

            $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
            $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
            $monday = date( 'd-m-Y', $timestamp_for_monday );
            $sunday = date('d-m-Y', strtotime('+6 days', strtotime($monday)));
            $plaza = $channel->Plaza->name;

            $lastSunday = date('Y-m-d', strtotime('-1 days', strtotime($monday)));
            $lastSundayPrograms = ChannelProgram::where('id_channel','=', $id_channel)->where('date','=', $lastSunday)->orderBy('start','asc')->get();

            foreach ($lastSundayPrograms as $p) {
                $title = $p->chapter_title != "" ? $p->program_name .' - ' . $p->chapter_title : $p->program_name;
                $rows = $p->duration / $interval;
                $start = $p->start;
                //$programsArr[$day][$start] = array('title' => $title, 'rowspan' => $rows, 'start' => $start);

                $hours = $p->duration / 60;
                $startArr = explode(':', $start);

                $hours = $hours + $startArr[0];
                
                if($hours > 24) {
                    $newDay = 1;
                    $diff = $hours - 24;
                    $mins = $startArr[1];
                    $divider = $intervals_of_15 > 0 ? 4 : 2;
                    $rows = 1 / $diff == 1 ? $divider : 1 / $diff;
                    if($mins != "00") {
                        $rows = $diff + ($mins / $interval);   
                    }

                    $programsArr[$newDay]['00:00'] = array('title' => $title, 'rowspan' => $rows, 'start' => '00:00');
                }
            }

            $pdf = PDF::loadView('pages.programs.programs-pdf', compact('programs', 'programsArr', 'monday', 'sunday', 'plaza', 'week', 'intervals_of_15', 'channel'));
            return $pdf->stream('export.pdf');
        }
    }

    public function renderExcelView(Request $request) {
        $id_channel = $request->id_channel;
        $week_data = $request->week;
        $week = explode('-', $week_data)[0];
        $year = explode('-', $week_data)[1];
        
        $channel = Channel::find($id_channel);

        $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
        $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
        
        $monday = date( 'd-m-Y', $timestamp_for_monday );
        $mondayDay = date('d',$timestamp_for_monday );

        $meses = array('', 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
        $firstMonth = date( 'n', $timestamp_for_monday );

        $sunday = date('d-m-Y', strtotime('+6 days', strtotime($monday)));
        $sundayDay = date('d',strtotime($sunday) );
        $lastMonth = date( 'n', strtotime($sunday));

        return Excel::download(new ProgramsExport($id_channel, $week, $year), $channel->Plaza->shortname . ' ' . $channel->principal_channel .'.' . $channel->secondary_channel . ' Sem_' . $week . ' del ' . $mondayDay .' de ' . $meses[$firstMonth] . ' al ' . $sundayDay . ' de ' . $meses[$lastMonth] . '.xlsx');
    }

    public function destroy($id) {
        $show = Program::find($id);

        if($show instanceof Program) {
            $show->status = 0;
            if($show->save()) {
                echo json_encode(array('status' => true, 'message' => 'El programa ha sido eliminado correctamente.'));
                exit;
            }
        }
    }

    private function exportTXT($programs, $channel, $filename) {
        if(!is_dir(env('FILESYSTEM') . 'public/exports/')) {
            //mkdir(env('FILESYSTEM') . 'public/exports/');
        }

        $file = env('FILESYSTEM') . 'public/exports/' . $filename;

        if(is_file($file)) {
            unlink($file);
        }

        $epg = fopen($file, 'w');
        fwrite($epg, 'CANAL PRINCIPAL' . "\t" . 'CANAL MENOR' . "\t" . 'FECHA DE INICIO' . "\t" . 'HORA INICIO' . "\t" . 'TERMINA' . "\t" . 'TITULO PROGRAMA' . "\t" . utf8_decode('DESCRIPCIÓN DEL PROGRAMA') . "\t" . utf8_decode('GÉNERO') . "\t" . utf8_decode('CLASIFICACIÓN') . "\t" . 'IDIOMA' . "\t" . 'SUBTITULOS' . "\t" . 'AUDIO' . PHP_EOL);
        
        foreach ($programs as $p) {
            $date = date('n/j/Y', strtotime($p->date));
            $description = trim(preg_replace('/\s+/', ' ', utf8_decode($p->description)));

            if($p->Program->Language == NULL) {
                echo json_encode(array('status' => false, 'message' => "El programa " . $p->program_name . ' no tiene asignada una opción de Lenguaje válida.'));
                exit;
            }

            if($p->Program->Rating == NULL) {
                echo json_encode(array('status' => false, 'message' => "El programa " . $p->program_name . ' no tiene asignada una opción de Clasificación válida.'));
                exit;
            }

            if($p->Program->Genre == NULL) {
                echo json_encode(array('status' => false, 'message' => "El programa " . $p->program_name . ' no tiene asignada una opción de Género válida.'));
                exit;
            }

            if($p->Program->Subtitle == NULL) {
                echo json_encode(array('status' => false, 'message' => "El programa " . $p->program_name . ' no tiene asignada una opción de Subtítulos válida.'));
                exit;
            }

            if($p->Program->Audio == NULL) {
                echo json_encode(array('status' => false, 'message' => "El programa " . $p->program_name . ' no tiene asignada una opción de Audio válida.'));
                exit;
            }

            $end = date('H:i:s', strtotime('+' . $p->duration . ' minutes', strtotime($p->start)));

            $programName = $p->program_name;
            if($p->chapter_title != "") {
                $programName .= ' - ' . $p->chapter_title;
            }

            $jet_lag = $channel->Plaza->jet_lag != "" || $channel->Plaza->jet_lag != 0 ? $channel->Plaza->jet_lag : "";

            if($p->duration > 60) {
                $hours = floor($p->duration / 60); 
                $minutes = $p->duration - ($hours * 60);
                $minutes = str_pad($minutes, 2 ,'0', STR_PAD_LEFT);
                $days = $hours / 24;

                if($days > 1) {
                    for ($i=0; $i < $days; $i++) { 
                        $nDate = date('n/j/Y', strtotime('+' . $i . ' days', strtotime($p->date)));
                        $start = date('H:i:00', strtotime($p->start));

                        if($jet_lag != "") {
                            $nDate = date('n/j/Y', strtotime('+' . $i . ' days ' . $jet_lag . ' hours', strtotime($p->date)));
                            $start = date('H:i:00', strtotime($jet_lag . ' hours', strtotime($p->start)));
                        }
                        $remain = $p->duration - ($i * (24 * 60));

                        $end = "";

                        if($remain > (24 * 60)) {
                            $end = '24:00:00';
                        } else {
                            $hoursRemain = floor($remain / 60);
                            $minutes = $remain - ($hoursRemain * 60);
                            $minutes = str_pad($minutes, 2,'0', STR_PAD_LEFT);
                            $end = $hoursRemain . ':' . $minutes . ':00';
                        }
                        
                        fwrite($epg,$channel->principal_channel . "\t" . $channel->secondary_channel . "\t" . $nDate . "\t" . $p->start . ':00' . "\t" . $end . "\t" . '[' . $p->Program->Rating->name .']' . utf8_decode($programName) . "\t" . $description . "\t" . utf8_decode($p->Program->Genre->name) . "\t" . $p->Program->Rating->name . "\t" .  utf8_decode($p->Program->Language->name) . "\t" . $p->Program->Subtitle->name . "\t" . $p->Program->Audio->name . PHP_EOL);
                        
                    }
                } else {
                    $end = $hours . ':' . $minutes . ':00';
                    fwrite($epg,$channel->principal_channel . "\t" . $channel->secondary_channel . "\t" . $date . "\t" . $p->start . ':00' . "\t" . $end . "\t" . '[' . $p->Program->Rating->name .']' . utf8_decode($programName) . "\t" . $description . "\t" . utf8_decode($p->Program->Genre->name) . "\t" . $p->Program->Rating->name . "\t" .  utf8_decode($p->Program->Language->name) . "\t" . $p->Program->Subtitle->name . "\t" . $p->Program->Audio->name . PHP_EOL); 
                }
            } else {
                $hours = floor($p->duration / 60); 
                $minutes = $p->duration - ($hours * 60);
                $minutes = str_pad($minutes, 2 ,'0', STR_PAD_LEFT);
                $end = $hours . ':' . $minutes . ':00';

                fwrite($epg,$channel->principal_channel . "\t" . $channel->secondary_channel . "\t" . $date . "\t" . $p->start . ':00' . "\t" . $end . "\t" . '[' . $p->Program->Rating->name .']' . utf8_decode($programName) . "\t" . $description . "\t" . utf8_decode($p->Program->Genre->name) . "\t" . $p->Program->Rating->name . "\t" .  utf8_decode($p->Program->Language->name) . "\t" . $p->Program->Subtitle->name . "\t" . $p->Program->Audio->name . PHP_EOL);
            }    
        }
        fclose($epg);

        return true;
    }

    private function exportXLS($programs, $channel, $filename) {
        if(!is_dir(env('FILESYSTEM') . 'public/exports/')) {
            //mkdir(env('FILESYSTEM') . 'public/exports/');
        }
        
        $html = '<table>';
        $html .= '<tr>';
        $html .= '<td>Date</td>';
        $html .= '<td>StartTime</td>';
        $html .= '<td>EndTime</td>';
        $html .= '<td>ShowName</td>';
        $html .= '<td>EpisodeTitle</td>';
        $html .= '<td>Description</td>';
        $html .= '<td>Rating</td>';
        $html .= '<td>Language</td>';
        $html .= '<td>SexualContent</td>';
        $html .= '<td>Violence</td>';
        $html .= '<td>ShowType</td>';
        $html .= '<td>Review</td>';
        $html .= '<td>Cast1</td>';
        $html .= '<td>Cast2</td>';
        $html .= '<td>Cast3</td>';
        $html .= '<td>ReleaseYear</td>';
        $html .= '<td>ClosedCap</td>';
        $html .= '<td>Stereo</td>';
        $html .= '<td>HD</td>';
        $html .= '<td>Premiere</td>';
        $html .= '</tr>';
        
        foreach ($programs as $p) {
            $date = date('n/j/Y', strtotime($p->date));
            $description = trim(preg_replace('/\s+/', ' ', utf8_decode($p->description)));

            if($p->Program->Language == NULL) {
                echo json_encode(array('status' => false, 'message' => "El programa " . $p->program_name . ' no tiene asignada una opción de Lenguaje válida.'));
                exit;
            }

            if($p->Program->Rating == NULL) {
                echo json_encode(array('status' => false, 'message' => "El programa " . $p->program_name . ' no tiene asignada una opción de Clasificación válida.'));
                exit;
            }

            if($p->Program->Genre == NULL) {
                echo json_encode(array('status' => false, 'message' => "El programa " . $p->program_name . ' no tiene asignada una opción de Género válida.'));
                exit;
            }

            if($p->Program->Subtitle == NULL) {
                echo json_encode(array('status' => false, 'message' => "El programa " . $p->program_name . ' no tiene asignada una opción de Subtítulos válida.'));
                exit;
            }

            if($p->Program->Audio == NULL) {
                echo json_encode(array('status' => false, 'message' => "El programa " . $p->program_name . ' no tiene asignada una opción de Audio válida.'));
                exit;
            }

            if($p->Program->release_year == "") {
                echo json_encode(array('status' => false, 'message' => "El programa " . $p->program_name . ' no tiene fecha de lanzamiento.'));
                exit;
            }

            $hours = floor($p->duration / 60); 
            $minutes = $p->duration - ($hours * 60);
            $minutes = str_pad($minutes, 2 ,'0', STR_PAD_LEFT);
            $days = $hours / 24;

            $end = date('H:i:s', strtotime('+' . $p->duration . ' minutes', strtotime($p->start)));
            $sexual_content = $p->Program->sexual_content == 1 ? "Y" : "N";
            $violence = $p->Program->violence == 1 ? "Y" : "N";
            $closedCap = "Y";
            $stereo = "Y";
            $hd = "Y";
            $premiere = "Y";

            $cast1 = $p->cast1 == "" ? $p->Program->cast1 : $p->cast1;
            $cast2 = $p->cast2 == "" ? $p->Program->cast2 : $p->cast2;
            $cast3 = $p->cast3 == "" ? $p->Program->cast3 : $p->cast3;

            $programName = $p->program_name;
            $chapterTitle = "EpisodeTitle";
            if($p->chapter_title != "") {
                $chapterTitle = $p->chapter_title;
            }

            if($days > 1) {
                for ($i=0; $i < $days; $i++) { 
                    $nDate = date('m/d/Y', strtotime('+' . $i . ' days', strtotime($p->date)));
                    $remain = $p->duration - ($i * (24 * 60));

                    $end = "";

                    if($remain > (24 * 60)) {
                        $end = '24:00:00';
                    } else {
                        $hoursRemain = floor($remain / 60);
                        $minutes = $remain - ($hoursRemain * 60);
                        $minutes = str_pad($minutes, 2,'0', STR_PAD_LEFT);
                        $end = $hoursRemain . ':' . $minutes . ':00';
                    }

                    $html .= '<tr>';
                    $html .= '<td>' . $nDate . '</td>';
                    $html .= '<td>' . $p->start . ':00</td>';
                    $html .= '<td>' . $end . '</td>';
                    $html .= '<td>' . utf8_decode($programName) . '</td>';
                    $html .= '<td>' . utf8_encode($chapterTitle) . '</td>';
                    $html .= '<td>' . $description . '</td>';
                    $html .= '<td>' . $p->Program->Rating->name . '</td>';
                    $html .= '<td>N</td>';
                    $html .= '<td>' . $sexual_content . '</td>';
                    $html .= '<td>' . $violence . '</td>';
                    $html .= '<td>' . utf8_decode($p->Program->Genre->name) . '</td>';
                    $html .= '<td>***</td>';
                    $html .= '<td>' . utf8_encode($cast1) . '</td>';
                    $html .= '<td>' . utf8_encode($cast2) . '</td>';
                    $html .= '<td>' . utf8_encode($cast3) . '</td>';
                    $html .= '<td>' . $p->Program->release_year . '</td>';
                    $html .= '<td>' . $closedCap . '</td>';
                    $html .= '<td>' . $stereo . '</td>';
                    $html .= '<td>' . $hd . '</td>';
                    $html .= '<td>' . $premiere . '</td>';
                    $html .= '</tr>';
                }
            } else {
                $html .= '<tr>';
                $html .= '<td>' . date('m/d/Y', strtotime($p->date)) . '</td>';
                $html .= '<td>' . $p->start . ':00</td>';
                $html .= '<td>' . $end . '</td>';
                $html .= '<td>' . utf8_decode($programName) . '</td>';
                $html .= '<td>' . utf8_encode($chapterTitle) . '</td>';
                $html .= '<td>' . $description . '</td>';
                $html .= '<td>' . $p->Program->Rating->name . '</td>';
                $html .= '<td>N</td>';
                $html .= '<td>' . $sexual_content . '</td>';
                $html .= '<td>' . $violence . '</td>';
                $html .= '<td>' . utf8_decode($p->Program->Genre->name) . '</td>';
                $html .= '<td>***</td>';
                $html .= '<td>' . utf8_encode($cast1) . '</td>';
                $html .= '<td>' . utf8_encode($cast2) . '</td>';
                $html .= '<td>' . utf8_encode($cast3) . '</td>';
                $html .= '<td>' . $p->Program->release_year . '</td>';
                $html .= '<td>' . $closedCap . '</td>';
                $html .= '<td>' . $stereo . '</td>';
                $html .= '<td>' . $hd . '</td>';
                $html .= '<td>' . $premiere . '</td>';
                $html .= '</tr>';
            }            
        }

        $html .= '</table>';

        file_put_contents(env('FILESYSTEM') . '/public/exports/' . $filename, $html);

        return true;
    }

    /*private function exportXLS2($programs, $channel, $filename) {
        if(!is_dir(env('FILESYSTEM') . 'public/exports/')) {
            mkdir(env('FILESYSTEM') . 'public/exports/');
        }
        
        $html = '<table>';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td>HORARIO</td>';
        $html .= '<td>PLAZA</td>';
        $html .= '<td></td>';
        $html .= '<td>PROGRAMA</td>';
        $html .= '<td></td>';
        $html .= '<td></td>';
        $html .= '<td>GENERO</td>';
        $html .= '<td>FECHA</td>';
        $html .= '</tr>';
        
        $months = array('', 'ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC');

        foreach ($programs as $p) {
            $date = date('n/j/Y', strtotime($p->date));

            if($p->Program->Genre == NULL) {
                echo json_encode(array('status' => false, 'message' => "El programa " . $p->program_name . ' no tiene asignada una opción de Género válida.'));
                exit;
            }

            $hours = floor($p->duration / 60); 
            $minutes = $p->duration - ($hours * 60);
            $minutes = str_pad($minutes, 2 ,'0', STR_PAD_LEFT);
            $days = $hours / 24;

            $end = date('H:i:s', strtotime('+' . $p->duration . ' minutes', strtotime($p->start)));

            $programName = $p->program_name;

            if($p->chapter_title != "") {
                $chapterTitle = $p->chapter_title;
            }

            if($days > 1) {
                for ($i=0; $i < $days; $i++) { 
                    $nDate = date('m/d/Y', strtotime('+' . $i . ' days', strtotime($p->date)));
                    $remain = $p->duration - ($i * (24 * 60));

                    $end = "";

                    if($remain > (24 * 60)) {
                        $end = '24:00:00';
                    } else {
                        $hoursRemain = floor($remain / 60);
                        $minutes = $remain - ($hoursRemain * 60);
                        $minutes = str_pad($minutes, 2,'0', STR_PAD_LEFT);
                        $end = $hoursRemain . ':' . $minutes . ':00';
                    }

                    $html .= '<tr>';
                    $html .= '<td></td>';
                    $html .= '<td>\'' . str_pad(str_replace(':', '', $p->start), 2, '0', STR_PAD_LEFT) . '</td>';
                    $html .= '<td>MULTIMEDIOS ' . $p->Channel->Plaza->name . '</td>';
                    $html .= '<td></td>';
                    $html .= '<td>' . utf8_decode($programName) . '</td>';
                    $html .= '<td></td>';
                    $html .= '<td></td>';
                    $html .= '<td>' . $p->Program->Genre->shortname . '</td>';
                    $html .= '<td>\'' . $months[date('n', strtotime($nDate))] . str_pad(date('d', strtotime($nDate)), 2, 0, STR_PAD_LEFT) . '</td>';
                    $html .= '</tr>';
                }
            } else {
                $html .= '<tr>';
                $html .= '<td></td>';
                $html .= '<td>\'' . str_pad(str_replace(':', '', $p->start), 2, '0', STR_PAD_LEFT) . '</td>';
                $html .= '<td>MULTIMEDIOS ' . $p->Channel->Plaza->name . '</td>';
                $html .= '<td></td>';
                $html .= '<td>' . utf8_decode($programName) . '</td>';
                $html .= '<td></td>';
                $html .= '<td></td>';
                $html .= '<td>' . $p->Program->Genre->shortname . '</td>';
                $html .= '<td>\'' . $months[date('n', strtotime($date))] . str_pad(date('d', strtotime($date)), 2, 0, STR_PAD_LEFT) . '</td>';
                $html .= '</tr>';
            }            
        }

        $html .= '</table>';

        file_put_contents(env('FILESYSTEM') . '/public/exports/' . $filename, $html);

        return true;
    }*/

    public function exportNielsen(Request $request) {
        $id_plaza = $request->id_plaza;
        $id_channel = $request->id_channel;
        $week_data = $request->week;
        $week = explode('-', $week_data)[0];
        $year = explode('-', $week_data)[1];

        $channel = Channel::find($id_channel);

        $plaza = Plaza::find($id_plaza);

        $programs = ChannelProgram::where('id_channel','=', $id_channel)->where('week','=', $week)->where('year','=', $year)->orderBy('date','asc')->orderBy('start','asc')->get();

        foreach($programs as $p) {
            $p->date = date('Y-m-d', strtotime($p->date));
            $p->save();
        }

        $programs = ChannelProgram::where('id_channel','=', $id_channel)->where('week','=', $week)->where('year','=', $year)->orderBy('id_channel', 'asc')->orderBy('date','asc')->orderBy('start','asc')->get();

        $filename = time() . '_MMTV.xlsx';

        if(!is_dir(env('FILESYSTEM') . 'public/exports/')) {
            mkdir(env('FILESYSTEM') . 'public/exports/');
        }

        foreach($programs as $p) {
            if($p->Program->Genre == NULL) {
              echo json_encode(array('status' => false, 'message' => "El programa " . $p->program_name . ' no tiene asignada una opción de Género válida.'));
              exit;
            }
        }

        return Excel::download(new NielsenExport($programs), $filename);
    }

    private function exportXLS2($programs, $filename) {
        
    }

    public function fixData() {
        $programs = ChannelProgram::where('date','>', '2021-12-01')->get();

        foreach ($programs as $p) {
            $week = date('W', strtotime($p->date));
            $year = date('Y', strtotime($p->date));
            $p->week = $week;
            $p->year = $year;
            $p->save();
        }
    }

    public function getProgramsBroadcasts(Request $request) {
        $id_channel_program = $request->id_channel_program;
        $date = $request->date;
        $start = $request->start;

        $actualProgram = ChannelProgram::find($id_channel_program);

        $programs = ChannelProgram::where('date',$date)->where('id_program',$actualProgram->id_program)->where('start',$start)->get();

        $data = array();

        foreach ($programs as $p) {
            if($p->id_channel_program != $id_channel_program) {
                $data[] = array('plaza' => $p->Channel->Plaza->name, 'id_plaza' => $p->Channel->id_plaza, 'channel' => $p->Channel->principal_channel . '.' . $p->Channel->secondary_channel, 'id_channel' => $p->id_channel, 'id_channel_program' => $p->id_channel_program);
            }
        }

        if(count($data) > 0) {
            return response()->json(['status' => true, 'data' => $data]);
        } else {
            return response()->json(['status' => false]);
        }
    }
}
