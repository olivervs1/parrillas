<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class GridController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request) {
        $plazas = Plaza::orderBy('name')->get();
        

        return view('pages.grids.grids');
    }
}
