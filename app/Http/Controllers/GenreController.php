<?php

namespace App\Http\Controllers;

use App\User;
use App\Genre;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $genres = Genre::get();
        
        return view('pages.genres.genres-list', compact('genres'));
    }

    public function add() {
        
        return view('pages.genres.genres-form');
    }

    public function addDB(Request $request) {
        $name = $request->name;
        
        $exist = Genre::where('name', '=', $name)->first();

        if($exist != NULL) {
            echo json_encode(array('status' => false, 'message' => 'Este Canal ya esta registrado.'));
            exit;
        }

        $genre = new Genre;
        $genre->name = $name;
        $genre->shortname = $request->shortname;
                
        if($genre->save()) {
            echo json_encode(array('status' => true, 'redirectTo' => url('genres')));
            exit;
        } else {
            echo json_encode(array('status' => false, 'message' => 'No se pudo agregar el Género, por favor intentalo nuevamente.'));
            exit;
        }
    }

    public function edit(Request $request) {
        $genre = Genre::find($request->id_genre);
        
        return view('pages.genres.genres-form', compact('genre'));   
    }

    public function editDB(Request $request) {
        $genre = Genre::find($request->id_genre);
        $name = $request->name;
        
        if($genre instanceof Genre) {
            $genre->name = $request->name;
            $genre->shortname = $request->shortname;
            
            if($genre->save()) {
                echo json_encode(array('status' => true, 'redirectTo' => url('genres')));
                exit;  
            }
        } else {
            echo json_encode(array('status' => false, 'message' => 'No se pudo editar el Género, por favor intentalo nuevamente.'));
            exit;
        }
    }
}
