<?php

namespace App\Http\Controllers;

use App\Devices;
use Illuminate\Http\Request;

class DevicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $devices = Devices::get();

        return view('pages.devices.devices-list', compact('devices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('pages.devices.devices-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $station_name = $request->station_name;
        $ip_address = $request->ip_address;
        $model_number = $request->model_number;
        $serial_number = $request->serial_number;
        $time_zone = $request->time_zone;
        $dst_adjustment = $request->dst_adjustment;
        $dst_start_date = $request->dst_start_date;
        $dst_start_time = $request->dst_start_time;
        $dst_end_date = $request->dst_end_date;
        $dst_end_time = $request->dst_end_time;
        $channels_names = $request->channel_name;
        $principal_channels = $request->principal_channel;
        $secondary_channels = $request->secondary_channel;

        $channels = array();

        foreach ($channels_names as $k => $v) {
            $channels[] = array('channel_name' => $v, 'principal_channel' => $principal_channels[$k], 'secondary_channel' => $secondary_channels[$k]);
        }

        $device = new Devices;
        $device->station_name = $station_name;
        $device->ip_address = $ip_address;
        $device->model_number = $model_number;
        $device->serial_number = $serial_number;
        $device->time_zone = $time_zone;
        $device->dst_adjustment = $dst_adjustment;
        $device->dst_start_date = $dst_start_date;
        $device->dst_start_time = $dst_start_time;
        $device->dst_end_date = $dst_end_date;
        $device->dst_end_time = $dst_end_time;
        $device->channels = json_encode($channels);

        if($device->save()) {
            return redirect(url('/devices/' . $device->id));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $device = Devices::find($id);

        if($device instanceof Devices) {
            return view('pages.devices.devices-record', compact('device'));
        }

        return redirect(url('/devices'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $device = Devices::find($id);

        if($device instanceof Devices) {
            return view('pages.devices.devices-form', compact('device'));
        }

        return redirect(url('/devices'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $device = Devices::find($id);

        if(!$device instanceof Devices) {
            return redirect(url('/devices'));   
        }

        $station_name = $request->station_name;
        $ip_address = $request->ip_address;
        $model_number = $request->model_number;
        $serial_number = $request->serial_number;
        $time_zone = $request->time_zone;
        $dst_adjustment = $request->dst_adjustment;
        $dst_start_date = $request->dst_start_date;
        $dst_start_time = $request->dst_start_time;
        $dst_end_date = $request->dst_end_date;
        $dst_end_time = $request->dst_end_time;
        $channels_names = $request->channel_name;
        $principal_channels = $request->principal_channel;
        $secondary_channels = $request->secondary_channel;

        $channels = array();

        foreach ($channels_names as $k => $v) {
            $channels[] = array('channel_name' => $v, 'principal_channel' => $principal_channels[$k], 'secondary_channel' => $secondary_channels[$k]);
        }

        $device->station_name = $station_name;
        $device->ip_address = $ip_address;
        $device->model_number = $model_number;
        $device->serial_number = $serial_number;
        $device->time_zone = $time_zone;
        $device->dst_adjustment = $dst_adjustment;
        $device->dst_start_date = $dst_start_date;
        $device->dst_start_time = $dst_start_time;
        $device->dst_end_date = $dst_end_date;
        $device->dst_end_time = $dst_end_time;
        $device->channels = json_encode($channels);

        if($device->save()) {
            return redirect(url('/devices/' . $device->id));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
