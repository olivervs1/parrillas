<?php

namespace App\Http\Controllers;

use App\User;
use App\Rating;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $ratings = Rating::get();
        
        return view('pages.ratings.ratings-list', compact('ratings'));
    }

    public function add() {
        
        return view('pages.ratings.ratings-form');
    }

    public function addDB(Request $request) {
        $name = $request->name;
        
        $exist = Rating::where('name', '=', $name)->first();

        if($exist != NULL) {
            echo json_encode(array('status' => false, 'message' => 'Esta Clasificación ya esta registrada.'));
            exit;
        }

        $rating = new Rating;
        $rating->name = $name;
                
        if($rating->save()) {
            echo json_encode(array('status' => true, 'redirectTo' => url('ratings')));
            exit;
        } else {
            echo json_encode(array('status' => false, 'message' => 'No se pudo agregar la Clasificación, por favor intentalo nuevamente.'));
            exit;
        }
    }

    public function edit(Request $request) {
        $rating = Rating::find($request->id_rating);
        
        return view('pages.ratings.ratings-form', compact('rating'));   
    }

    public function editDB(Request $request) {
        $rating = Rating::find($request->id_rating);
        $name = $request->name;
        
        if($rating instanceof Rating) {
            $rating->name = $request->name;
            
            if($rating->save()) {
                echo json_encode(array('status' => true, 'redirectTo' => url('ratings')));
                exit;  
            }
        } else {
            echo json_encode(array('status' => false, 'message' => 'No se pudo editar la Clasificación, por favor intentalo nuevamente.'));
            exit;
        }
    }
}
