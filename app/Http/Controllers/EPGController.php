<?php

namespace App\Http\Controllers;

use App\User;
use App\EPG;
use Illuminate\Http\Request;

class EPGController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $epgs = EPG::get();
        
        return view('pages.epgs.epgs-list', compact('epgs'));
    }

    public function record(Request $request) {
        $epg = EPG::find($request->id_epg);

        return view('pages.epgs.epgs-record', compact('epg'));
    }

    public function add() {
        $users = User::orderBy('name')->get();
        return view('pages.epgs.epgs-form', compact('users'));
    }

    public function addDB(Request $request) {
        $name = $request->name;

        $exist = EPG::where('name', '=', $name)->first();

        if($exist != NULL) {
            echo json_encode(array('status' => false, 'message' => 'Esta EPG ya esta registrada.'));
            exit;
        }

        $epg = new EPG;
        $epg->name = $name;
        
        if($epg->save()) {
            echo json_encode(array('status' => true, 'redirectTo' => url('epgs')));
            exit;
        } else {
            echo json_encode(array('status' => false, 'message' => 'No se pudo agregar la EPG, por favor intentalo nuevamente.'));
            exit;
        }
    }

    public function edit(Request $request) {
        $epg = EPG::find($request->id_epg);

        return view('pages.epgs.epgs-form', compact('epg'));   
    }

    public function editDB(Request $request) {
        $epg = EPG::find($request->id_epg);

        if($epg instanceof EPG) {
            $epg->name = $request->name;
            if($epg->save()) {
                echo json_encode(array('status' => true, 'redirectTo' => url('epgs')));
                exit;  
            }
        } else {
            echo json_encode(array('status' => false, 'message' => 'No se pudo editar la EPG, por favor intentalo nuevamente.'));
            exit;
        }
    }
}
