<?php

namespace App\Http\Controllers;

use App\User;
use App\Audio;
use Illuminate\Http\Request;

class AudioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $audios = Audio::get();
        
        return view('pages.audios.audios-list', compact('audios'));
    }

    public function add() {
        
        return view('pages.audios.audios-form');
    }

    public function addDB(Request $request) {
        $name = $request->name;
        
        $exist = Audio::where('name', '=', $name)->first();

        if($exist != NULL) {
            echo json_encode(array('status' => false, 'message' => 'Este tipo de Audio ya esta registrado.'));
            exit;
        }

        $audio = new Audio;
        $audio->name = $name;
                
        if($audio->save()) {
            echo json_encode(array('status' => true, 'redirectTo' => url('audios')));
            exit;
        } else {
            echo json_encode(array('status' => false, 'message' => 'No se pudo agregar el tipo de Audio, por favor intentalo nuevamente.'));
            exit;
        }
    }

    public function edit(Request $request) {
        $audio = Audio::find($request->id_audio);
        
        return view('pages.audios.audios-form', compact('audio'));   
    }

    public function editDB(Request $request) {
        $audio = Audio::find($request->id_audio);
        $name = $request->name;
        
        if($audio instanceof Audio) {
            $audio->name = $request->name;
            
            if($audio->save()) {
                echo json_encode(array('status' => true, 'redirectTo' => url('audios')));
                exit;  
            }
        } else {
            echo json_encode(array('status' => false, 'message' => 'No se pudo editar el Género, por favor intentalo nuevamente.'));
            exit;
        }
    }
}
