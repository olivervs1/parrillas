<?php

namespace App\Http\Controllers;

use App\User;
use App\Subtitle;
use Illuminate\Http\Request;

class SubtitleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $subtitles = Subtitle::get();
        
        return view('pages.subtitles.subtitles-list', compact('subtitles'));
    }

    public function add() {
        
        return view('pages.subtitles.subtitles-form');
    }

    public function addDB(Request $request) {
        $name = $request->name;
        
        $exist = Subtitle::where('name', '=', $name)->first();

        if($exist != NULL) {
            echo json_encode(array('status' => false, 'message' => 'Este Subtítulo ya esta registrado.'));
            exit;
        }

        $subtitle = new Subtitle;
        $subtitle->name = $name;
                
        if($subtitle->save()) {
            echo json_encode(array('status' => true, 'redirectTo' => url('subtitles')));
            exit;
        } else {
            echo json_encode(array('status' => false, 'message' => 'No se pudo agregar el Subtítulo, por favor intentalo nuevamente.'));
            exit;
        }
    }

    public function edit(Request $request) {
        $subtitle = Subtitle::find($request->id_subtitle);
        
        return view('pages.subtitles.subtitles-form', compact('subtitle'));   
    }

    public function editDB(Request $request) {
        $subtitle = Subtitle::find($request->id_subtitle);
        $name = $request->name;
        
        if($subtitle instanceof Subtitle) {
            $subtitle->name = $request->name;
            
            if($subtitle->save()) {
                echo json_encode(array('status' => true, 'redirectTo' => url('subtitles')));
                exit;  
            }
        } else {
            echo json_encode(array('status' => false, 'message' => 'No se pudo editar el Subtítulo, por favor intentalo nuevamente.'));
            exit;
        }
    }
}
