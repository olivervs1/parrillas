<?php
namespace App\Http\Controllers;

ini_set('memory_limit', '-1');

use App\EPG;
use App\User;
use App\Plaza;
use App\Emails;
use App\Channel;
use App\Program;
use App\ShowGroup;
use App\PlazaUser;
use App\PlazaEmails;
use App\PlazaChannel;
use App\ChannelProgram;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PlazaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $plazas = Plaza::get();
        
        return view('pages.plazas.plazas-list', compact('plazas'));
    }

    public function record(Request $request) { 
        $plaza = Plaza::find($request->id_plaza);
        $plazas = Plaza::all();
        $programs = Program::where('status','=',1)->orderBy('name')->get();
        $channel_programs = [];
        $show_groups = ShowGroup::all();

        if($plaza->Channels->count() > 0) {
            if(isset($request->id_channel)) {
                $channel_programs = ChannelProgram::where('id_channel','=', $request->id_channel)->get();            
            } else {
                $channel_programs = ChannelProgram::with(['Program'])->where('id_channel','=', $plaza->channels->first()->id_channel)->get();
            }
        }

        $programation = [];
      
        foreach ($channel_programs as $p) {            
            $actDate = $p->date . ' ' . $p->start;
            $end = date('d-m-Y H:i:s',strtotime($actDate . ' +' . $p->duration . ' minutes'));

            $record = [
                'id'                => $p->id_channel_program,
                'id_program'        => $p->id_program,
                'id_channel'        => $p->id_channel,
                'title'             => $p->chapter_title != "" ? $p->program_name . ' - ' . $p->chapter_title : $p->program_name,
                'chapter_title'     => $p->chapter_title,
                'duration'          => $p->duration,
                'allDay'            => false,
                'year'              => date('Y', strtotime($actDate)),
                'month'             => date('m', strtotime($actDate)), 
                'day'               => date('d', strtotime($actDate)),
                'hours'             => date('H', strtotime($actDate)),
                'minutes'           => date('i', strtotime($actDate)),
                'end_year'          => date('Y', strtotime($end)),
                'end_month'         => date('m', strtotime($end)),
                'end_day'           => date('d', strtotime($end)),
                'end_hours'         => date('H', strtotime($end)),
                'end_minutes'       => date('i', strtotime($end)),
                'backgroundColor'   => $p->Program->color,
                'description'       => $p->description,
                'cast1'             => $p->cast1 != "" ? $p->cast1 : $p->Program->cast1,
                'cast2'             => $p->cast2 != "" ? $p->cast2 : $p->Program->cast2,
                'cast3'             => $p->cast3 != "" ? $p->cast3 : $p->Program->cast3
            ];

            array_push($programation, $record);
        }

        $availableWeeks = ChannelProgram::where('year','=', date('Y'))->groupBy('week')->orderBy('week','asc')->get();
        $dataWeeks = array();

        foreach ($availableWeeks as $w) {
            $timestamp = mktime( 0, 0, 0, 1, 1,  $w->year ) + ( $w->week * 7 * 24 * 60 * 60 );
            $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
            $from = date( 'd-m-Y', $timestamp_for_monday);
            $dataWeeks[$w->week] = array('id' => $w->week . '-' . $w->year,'week' => $w->week, 'from' => $from, 'to' => date('d-m-Y', strtotime('+6 days', strtotime($from))));
        }

        //var_dump($dataWeeks); exit;

        return view('pages.plazas.plazas-record', compact('plaza', 'programs', 'programation', 'dataWeeks', 'plazas', 'show_groups'));
    }

    public function add() {
        $users = User::orderBy('name')->get();
        $epgs = EPG::orderBy('name')->get();
        $channels = Channel::orderBy('principal_channel')->get();
        return view('pages.plazas.plazas-form', compact('users', 'epgs', 'channels'));
    }

    public function addDB(Request $request) {
        $name = $request->name;
        $shortname = $request->shortname;
        $id_epg = $request->id_epg;
        $jet_lag = $request->jet_lag;

        $exist = Plaza::where('name', '=', $name)->first();

        if($exist != NULL) {
            echo json_encode(array('status' => false, 'message' => 'Esta plaza ya esta registrada.'));
            exit;
        }

        $plaza = new Plaza;
        $plaza->name = $name;
        $plaza->shortname = $shortname;
        $plaza->id_epg = $id_epg;
        $plaza->jet_lag = $jet_lag;
        
        if($plaza->save()) {
            if(isset($request->users)) {
                foreach ($request->users as $u) {
                    $plazaUser = new PlazaUser;
                    $plazaUser->id_plaza = $plaza->id_plaza;
                    $plazaUser->id_user = $u;
                    $plazaUser->save();
                }
            }

            if(isset($request->channels)) {
                foreach ($request->channels as $c) {
                    $plazaChannel = new PlazaChannel;
                    $plazaChannel->id_plaza = $plaza->id_plaza;
                    $plazaChannel->id_channel = $c;
                    $plazaChannel->save();
                }
            }

            echo json_encode(array('status' => true, 'redirectTo' => url('plazas')));
            exit;
        } else {
            echo json_encode(array('status' => false, 'message' => 'No se pudo agregar la plaza, por favor intentalo nuevamente.'));
            exit;
        }
    }

    public function edit(Request $request) {
        $plaza = Plaza::find($request->id_plaza);
        $users = User::orderBy('name')->get();
        $epgs = EPG::orderBy('name')->get();
        $channels = Channel::orderBy('principal_channel')->get();

        return view('pages.plazas.plazas-form', compact('plaza', 'users', 'epgs', 'channels'));   
    }

    public function editDB(Request $request) {
        $plaza = Plaza::find($request->id_plaza);
        $emails = $request->emails;

        if($plaza instanceof Plaza) {
            $plaza->name = $request->name;
            $plaza->shortname = $request->shortname;
            $plaza->id_epg = $request->id_epg;
            $plaza->jet_lag = $request->jet_lag;

            if(isset($request->users)) {
                foreach ($plaza->Users as $u) {
                    $u->delete();
                }

                foreach ($request->users as $u) {
                    $plazaUser = new PlazaUser;
                    $plazaUser->id_plaza = $plaza->id_plaza;
                    $plazaUser->id_user = $u;
                    $plazaUser->save();
                }
            }

            if(isset($request->channels)) {
                foreach ($plaza->Channels as $c) {
                    $c->delete();
                }

                foreach ($request->channels as $c) {
                    $plazaChannel = new PlazaChannel;
                    $plazaChannel->id_plaza = $plaza->id_plaza;
                    $plazaChannel->id_channel = $c;
                    $plazaChannel->save();
                }
            }

            if($plaza->save()) {
                echo json_encode(array('status' => true, 'redirectTo' => url('plazas')));
                exit;
            }
        } else {
            echo json_encode(array('status' => true, 'message' => 'Esta plaza no ha podido ser editada, por favor, inténtalo nuevamente.'));
            exit;
        }
    }

    public function duplicate() {
        $channel_programs = ChannelProgram::get();

        foreach ($channel_programs as $cp) {
            
            $nCP = new ChannelProgram;
            $nCP->id_channel = $cp->id_channel;
            $nCP->id_program = $cp->id_program;
            $nCP->program_name = $cp->program_name;
            $nCP->chapter_title = $cp->chapter_title;
            $nCP->duration = $cp->duration;
            $nCP->week = $cp->week + 1;
            $date = strtotime($cp->date);
            $nCP->year = date('Y', strtotime($cp->date));
            $nCP->date = date('Y-m-d', strtotime('+7 days', $date));
            $nCP->start = $cp->start;
            $nCP->description = $cp->description;
            $nCP->save();
        }

        echo "Terminado";
    }

    public function addEmailToPlaza($id_plaza, Request $request) {
        $plaza = Plaza::find($id_plaza);
        $email = $request->email;

        $plazaEmail = PlazaEmails::where('email','=', $email)->where('plaza_id','=', $plaza->id_plaza)->first();

        if(!$plazaEmail instanceof PlazaEmails) {
            $plazaEmailObj = new PlazaEmails;
            $plazaEmailObj->plaza_id = $plaza->id_plaza;
            $plazaEmailObj->email = $email;
            $plazaEmailObj->save();

            $plazaEmail = $plazaEmailObj;

            return response()->json(['status' => true, 'email' => $plazaEmail]);
        }

        return response()->json(['status' => true]);

                
    }

    public function removeEmailFromPlaza($id_plaza, Request $request) {
        $plaza = Plaza::find($id_plaza);
        $email_id = $request->email_id;

        $plazaEmail = PlazaEmails::find($email_id);

        if($plazaEmail instanceof PlazaEmails) {
            $plazaEmail->delete();
        }

        return response()->json(['status' => true]);
    }

    public function getJSON($plaza_key, $channel) {
        $plaza = Plaza::where('shortname','=', $plaza_key)->first();

        if($plaza instanceof PLaza) {
            $channelArr = explode('.', $channel);

            $channelObj = Channel::where('principal_channel','=', $channelArr[0])->where('secondary_channel','=', $channelArr[1])->where('id_plaza','=', $plaza->id_plaza)->first();

            $programs = ChannelProgram::where('id_channel','=', $channelObj->id_channel)->whereBetween('date',[date('Y-m-d', strtotime('+1 day')), date('Y-m-d', strtotime('+7 days'))])->orderBy('date', 'asc')->orderBy('start', 'asc')->get();

            $data = array();

            foreach ($programs as $p) {
                $data[] = array(
                    'program' => $p->Program->name,
                    'chapter_title' => $p->chapter_title,
                    'duration' => $p->duration,
                    'cast_1' => $p->cast1,
                    'description' => $p->description,
                    'program_name' => $p->program_name,
                    'program_group' => $p->Program->ShowGroup->name,
                    'plaza' => $plaza->name,
                    'plaza_shortname' => $plaza->shortname,
                    'start' => $p->start,
                    'date' => $p->date); 
            }

            return response()->json(['status' => true, 'data' => $data]);

        } else {
            return response()->json(['status' => false, 'message' => 'La clave de la plaza no es válida.']);
        }
    }
}

