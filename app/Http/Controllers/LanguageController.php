<?php

namespace App\Http\Controllers;

use App\User;
use App\Language;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $languages = Language::get();
        
        return view('pages.languages.languages-list', compact('languages'));
    }

    public function add() {
        
        return view('pages.languages.languages-form');
    }

    public function addDB(Request $request) {
        $name = $request->name;
        
        $exist = Language::where('name', '=', $name)->first();

        if($exist != NULL) {
            echo json_encode(array('status' => false, 'message' => 'Este idioma ya esta registrado.'));
            exit;
        }

        $language = new Language;
        $language->name = $name;
                
        if($language->save()) {
            echo json_encode(array('status' => true, 'redirectTo' => url('languages')));
            exit;
        } else {
            echo json_encode(array('status' => false, 'message' => 'No se pudo agregar el Idioma, por favor intentalo nuevamente.'));
            exit;
        }
    }

    public function edit(Request $request) {
        $language = Language::find($request->id_language);
        
        return view('pages.languages.languages-form', compact('language'));   
    }

    public function editDB(Request $request) {
        $language = Language::find($request->id_language);
        $name = $request->name;
        
        if($language instanceof Language) {
            $language->name = $request->name;
            
            if($language->save()) {
                echo json_encode(array('status' => true, 'redirectTo' => url('languages')));
                exit;  
            }
        } else {
            echo json_encode(array('status' => false, 'message' => 'No se pudo editar el Idioma, por favor intentalo nuevamente.'));
            exit;
        }
    }
}
