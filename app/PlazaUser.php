<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class PlazaUser extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id_plaza_user';

    protected $table = 'plazas_users';

    public function User() {
        return $this->belongsTo('App\User', 'id_user', 'id');
    }

    public function Plaza() {
        return $this->belongsTo('App\Plaza', 'id_plaza', 'id_plaza');
    }
}
