<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class EPG extends Model
{
    use SoftDeletes;

    protected $table = "epg";

    protected $primaryKey = 'id_epg';
}
