<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Channel extends Model
{
    use SoftDeletes;

    protected $table = "channel";

    protected $primaryKey = 'id_channel';

    public function Plaza() {
        return $this->belongsTo('App\Plaza', 'id_plaza', 'id_plaza');
    }

    public function Device() {
        return $this->belongsTo('App\Devices', 'id_device', 'id');
    }
}
