<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlazaChannel extends Model
{

	use SoftDeletes;

	protected $table = 'plazas_channels';

    protected $primaryKey = 'id_plaza_channel';

    public function Channel() {
        return $this->belongsTo('App\Channel', 'id_channel', 'id_channel');
    }

    public function Plaza() {
        return $this->belongsTo('App\Plaza', 'id_plaza', 'id_plaza');
    }
}
