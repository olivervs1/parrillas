<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Subtitle extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id_subtitle';
}
