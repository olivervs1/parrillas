<?php

namespace App\Exports;

use App\Channel;
use App\ChannelProgram;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class NielsenExport implements FromView, WithStyles
{	
	public $programs;

	/**
     * @return array
     */
    public function columnFormats(): array {
    }

	public function styles(Worksheet $sheet) {
		$sheet->getStyle('B')->applyFromArray([
			'alignment' => [
     			'horizontal' => Alignment::HORIZONTAL_LEFT
     		]
		]);

		$sheet->getStyle('B')
		    ->getNumberFormat()
		    ->setFormatCode(
		        NumberFormat::FORMAT_TEXT
		    );

		$sheet->getStyle('C')
		    ->getNumberFormat()
		    ->setFormatCode(
		        NumberFormat::FORMAT_TEXT
		    );

	    $sheet->getStyle('E')
	        ->getNumberFormat()
	        ->setFormatCode(
	            NumberFormat::FORMAT_TEXT
	        );

        $sheet->getStyle('H')
            ->getNumberFormat()
            ->setFormatCode(
                NumberFormat::FORMAT_TEXT
            );

        $sheet->getStyle('I')
            ->getNumberFormat()
            ->setFormatCode(
                NumberFormat::FORMAT_TEXT
            );
    }

	public function __construct($programs) {
		$this->programs = $programs;
	}

	public function view(): View {
	 	$programs = $this->programs;
	    return view('pages.programs.nielsen-export', compact('programs'));
	}
}
