<?php

namespace App\Exports;

use App\Channel;
use App\ChannelProgram;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;


class ProgramsExport implements FromView, WithStyles, WithDrawings
{	
	public $id_channel;
	public $week;


	public function styles(Worksheet $sheet) {
		$styles = array(
			'borders' =>[
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
			    	'color' => ['argb' => '000000'],
			   		]
			   ]
			);

		$sheet->getStyle('A5:I102')->applyFromArray($styles);
		$sheet->getStyle('A5:I102')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle('A5:I102')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		$sheet->getStyle('A1:I102')->getAlignment()->setWrapText(true);
		$sheet->getDefaultColumnDimension('B')->setWidth(20);
		
		$styles = array(
		    'font'  => array(
		        'size'  => 12,
		        'name'  => 'Century Gothic'
		    ));

		$sheet->getStyle('B5:H102')->applyFromArray($styles);

		$styles = array(
		    'font'  => [
		        'size'  => 16,
		        'name'  => 'Century Gothic'
		    ],
			'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ]
                );

		$sheet->getStyle('B1')->applyFromArray($styles);
		//$sheet->getStyle('A1:I54')->setFont('Century Gothic');
		//$sheet->getFont()->setSize(14);


		$sheet->getStyle('A5:I6')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('000000');
		$sheet->getStyle('A5:I6')->getFont()->getColor()->setARGB('FFFFFF');
		$sheet->getPageSetup()->setFitToWidth(1);

        return [
            // Style the first row as bold text.
            1    => [
            	'font' => ['bold' => true], 
        	],
        	2    => [
            	'font' => ['bold' => true], 
        	],
        	3    => [
            	'font' => ['bold' => true], 
        	],
        	4    => [
            	'font' => ['bold' => true], 
        	],
        	5    => [
            	'font' => ['bold' => true], 
        	]
        ];
    }

    public function drawings() {
    	$channel = Channel::find($this->id_channel); 

    	if($channel->thumbnail != "") {
    		$drawing = new Drawing();
    		$drawing->setName('Logo');
    		$drawing->setDescription('This is my logo');
    		$drawing->setPath(public_path('img/channels/' . $channel->thumbnail));
    		$drawing->setHeight(75);
    		$drawing->setCoordinates('A1');
    		return $drawing;
    	} else {
    		$drawing = new Drawing();
    		$drawing->setName('Logo');
    		$drawing->setDescription('This is my logo');
    		$drawing->setPath(public_path('img/channels/ffffff.jpg'));
    		$drawing->setHeight(75);
    		$drawing->setCoordinates('A1');
    		return $drawing;
    	}

        
    }

	public function __construct($id_channel, $week, $year) {
		$this->id_channel = $id_channel;
		$this->week = $week;
		$this->year = $year;
	}


	public function time_to_decimal($time) {
	    $timeArr = explode(':', $time);
	    $decTime = ($timeArr[0]*60) + ($timeArr[1]);

	    return $decTime;
	} 

	public function view(): View {

	    $id_channel = $this->id_channel;
	    $week = $this->week;
	    $year = $this->year;
	    $channel = Channel::find($id_channel); 

	    $programsArr = array();
	    if($channel instanceof Channel) {
	        $programs = ChannelProgram::where('id_channel','=', $id_channel)->where('week','=', $week)->where('year','=', $year)->orderBy('start','asc')->get();
	        
	        $programsConsult = array();
	        foreach($programs as $p) {
	        	$day = date('w', strtotime($p->date)) == 0 ? 7 : date('w', strtotime($p->date));
	        	$programsConsult[$day][$p->id_program][$p->start] = true;
	        }

	        foreach ($programs as $p) {
	        	$rows = 1;
	            $day = date('w', strtotime($p->date)) == 0 ? 7 : date('w', strtotime($p->date));
	            $title = $p->chapter_title != "" ? $p->program_name .' - ' . $p->chapter_title : $p->program_name;
	            $rows = ceil($p->duration / 15); //intervalos de 15 minutos
	            $start = $p->start; //hora de inicio

	            $programsArr[$day][$start] = array('title' => $title, 'rowspan' => $rows, 'start' => $start);

	            //AGREGE ESTA CONVERSION A DECIMALES 
	            $hours = $p->duration / 60; //duracion en horas decimales.
	            $decimal_start = $this->time_to_decimal($start)/60;

	            //logger("{$title}: horas antes de suma : {$hours}  start:{$start} y decimal start : {$decimal_start}"); //2.25 qu eson 2 horas y cuarto. para es show el musical.
	            
	            $startArr = explode(':', $start); //array con horas y minutos YA NO SERIA NECESARIO

	            //logger("start : {$start}");

	            /** FUNCION ORIGINAL OLIVER
	            $hours = $hours + $startArr[0]; //en hours primero viene la duracion con decimales y solamente las horas para saber si me paso, aqui hay un error. 
	            //esto no jalaria si empiezo a las 22.45 y el programa dura 1.45 horas
				**/

				$hours = $hours + $decimal_start;

	            //logger("{$title}: horas despues de suma : {$hours}"); //2.25 qu eson 2 horas y cuarto. para es show el musical.
	            //para partirlo en 2
	            if($hours > 24) { //aqwui cambie a solo mayor, no mayor o igual.
	            	if($day != 7) {
	            		$newDay = $day+1;
	            		$diff = $hours - 24; //aqui voy a tener las horas decimales que se pasan al siguiente dia


	            		//$mins = $startArr[1];
						logger("Programa: {$title}   inicio: {$start}  dia:{$newDay}   diff:{$diff}");
	            		
	            		/** CALCULO DE OLIVER PARA SACAR LOS ROWS

	            		if($diff > 0 || $mins != "00") {

	            			if($diff > 0) {
	            				$rows = 1 / $diff == 1 ? 4 : 1 / $diff;
	            			} else {
	            				$rows = $mins / 15;
	            				$mins = "00";
	            			}

	            			if($mins != "00") {
	            				$rows = $rows + ($mins / 15);	
	            			}

						


	            			$programsArr[$newDay]['00:00'] = array('title' => $title, 'rowspan' => $rows, 'start' => '00:00');
	            		}
	            		**/
						$rows = floor($diff/.25); //nuevo calculo de intervalos.
						$programsArr[$newDay]['00:00'] = array('title' => $title, 'rowspan' => $rows, 'start' => '00:00');
	            	}
	            }
	        }

            $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
            $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
            $monday = date( 'd-m-Y', $timestamp_for_monday );
            $sunday = date('d-m-Y', strtotime('+6 days', strtotime($monday)));
            $plaza = $channel->Plaza->name;

            $lastSunday = date('Y-m-d', strtotime('-1 days', strtotime($monday)));
            $lastSundayPrograms = ChannelProgram::where('id_channel','=', $id_channel)->where('date','=', $lastSunday)->orderBy('start','asc')->get();

            foreach ($lastSundayPrograms as $p) {
            	$title = $p->chapter_title != "" ? $p->program_name .' - ' . $p->chapter_title : $p->program_name;
            	$rows = $p->duration / 15;
            	$start = $p->start;
            	//$programsArr[$day][$start] = array('title' => $title, 'rowspan' => $rows, 'start' => $start);

            	$hours = $p->duration / 60;
            	$startArr = explode(':', $start);

            	$hours = $hours + $startArr[0];
            	
            	if($hours > 24) {
        			$newDay = 1;
        			$diff = $hours - 24;
        			$mins = $startArr[1];
        			$rows = 1 / $diff == 1 ? 4 : 1 / $diff;
        			if($mins != "00") {
        				$rows = $diff + ($mins / 15);	
        			}

        			$programsArr[$newDay]['00:00'] = array('title' => $title, 'rowspan' => $rows, 'start' => '00:00');
            	}
            }

	    	return view('pages.programs.programs-excel', compact('programs', 'programsArr', 'monday', 'sunday', 'plaza', 'week'));
		}
	}
}
