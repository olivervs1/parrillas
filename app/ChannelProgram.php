<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChannelProgram extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id_channel_program';

    protected $table = 'channels_programs';
    
    public function Channel() {
        return $this->belongsTo('App\Channel', 'id_channel', 'id_channel');
    }
    public function Program() {
        return $this->belongsTo('App\Program', 'id_program', 'id_program');
    }

    protected static function booted()
    {
        static::updating(function ($channel) {
            $channel->year = date('Y', strtotime($channel->date));
            $channel->week = date('W', strtotime($channel->date));
        });

        static::saving(function ($channel) {
            $channel->year = date('Y', strtotime($channel->date));
            $channel->week = date('W', strtotime($channel->date));
        });
    }
}
