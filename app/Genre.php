<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Genre extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id_genre';
}
