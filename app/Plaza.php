<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Plaza extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id_plaza';
    
    public function EPG() {
        return $this->belongsTo('App\EPG', 'id_epg', 'id_epg');
    }

    public function Users() {
        return $this->hasMany('App\PlazaUser', 'id_plaza', 'id_plaza');
    }

    public function Channels() {
        return $this->hasMany('App\Channel', 'id_plaza', 'id_plaza');
    }

    public function Emails() {
        return $this->hasMany('App\PlazaEmails', 'plaza_id', 'id_plaza');
    }
}
