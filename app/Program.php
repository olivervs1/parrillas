<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Program extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id_program';

    public function Plaza() {
        return $this->belongsTo('App\Plaza', 'id_plaza', 'id_plaza');
    }

    public function Language() {
        return $this->belongsTo('App\Language', 'id_language', 'id_language');
    }

    public function Rating() {
        return $this->belongsTo('App\Rating', 'id_rating', 'id_rating');
    }

    public function Genre() {
        return $this->belongsTo('App\Genre', 'id_genre', 'id_genre');
    }

    public function Audio() {
        return $this->belongsTo('App\Audio', 'id_audio', 'id_audio');
    }

    public function Video() {
        return $this->belongsTo('App\Video', 'id_video', 'id_video');
    }

    public function Subtitle() {
        return $this->belongsTo('App\Subtitle', 'id_subtitle', 'id_subtitle');
    }

    public function ShowGroup() {
        return $this->belongsTo('App\ShowGroup', 'id_show_group', 'id');
    }
}
