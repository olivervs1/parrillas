<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create somethiOPng great!
|
*/
Auth::routes();

Route::group( ['middleware' => 'auth' ], function() {
	Route::get('/', 'HomeController@index')->name('home');
	Route::get('/home', 'HomeController@index')->name('home');

	##### USUARIOS #####
	Route::get('/users', 'UserController@index');
	Route::get('/users/add', 'UserController@add');
	Route::post('/users/add', 'UserController@addDB');
	Route::get('/users/edit/{id_user}', 'UserController@edit');
	Route::post('/users/edit/{id_user}', 'UserController@editDB');

	##### EQUIPOS #####
	Route::get('/devices', 'DevicesController@index');
	Route::get('/devices/add', 'DevicesController@create');
	Route::post('/devices/add', 'DevicesController@store');
	Route::get('/devices/{id}', 'DevicesController@show');
	Route::get('/devices/edit/{id}', 'DevicesController@edit');
	Route::post('/devices/edit/{id}', 'DevicesController@update');
	Route::post('/devices/remove/{id}', 'DevicesController@destroy');

	##### PLAZAS #####
	Route::get('/plazas', 'PlazaController@index');
	Route::get('/plazas/duplicate', 'PlazaController@duplicate');
	Route::get('/plazas/add', 'PlazaController@add');
	Route::post('/plazas/add', 'PlazaController@addDB');
	Route::get('/plazas/edit/{id_plaza}', 'PlazaController@edit');
	Route::post('/plazas/edit/{id_plaza}', 'PlazaController@editDB');
	Route::get('/plazas/{id_plaza}', 'PlazaController@record');
	Route::post('/plazas/{id_plaza}/add-email', 'PlazaController@addEmailToPlaza');
	Route::post('/plazas/{id_plaza}/remove-email', 'PlazaController@removeEmailFromPlaza');

	##### EPG #####
	Route::get('/epgs', 'EPGController@index');
	Route::get('/epgs/add', 'EPGController@add');
	Route::post('/epgs/add', 'EPGController@addDB');
	Route::get('/epgs/edit/{id_epg}', 'EPGController@edit');
	Route::post('/epgs/edit/{id_epg}', 'EPGController@editDB');
	Route::get('/epgs/{id_epg}', 'EPGController@record');

	##### CANALES #####
	Route::get('/channels', 'ChannelController@index');
	Route::get('/channels/add', 'ChannelController@add');
	Route::post('/channels/add', 'ChannelController@addDB');
	Route::get('/channels/edit/{id_channel}', 'ChannelController@edit');
	Route::post('/channels/edit/{id_channel}', 'ChannelController@editDB');
	Route::get('/channels/{id_channel}', 'ChannelController@record');

	##### GENEROS #####
	Route::get('/genres', 'GenreController@index');
	Route::get('/genres/add', 'GenreController@add');
	Route::post('/genres/add', 'GenreController@addDB');
	Route::get('/genres/edit/{id_genre}', 'GenreController@edit');
	Route::post('/genres/edit/{id_genre}', 'GenreController@editDB');

	##### CLASIFICACIONES #####
	Route::get('/ratings', 'RatingController@index');
	Route::get('/ratings/add', 'RatingController@add');
	Route::post('/ratings/add', 'RatingController@addDB');
	Route::get('/ratings/edit/{id_rating}', 'RatingController@edit');
	Route::post('/ratings/edit/{id_rating}', 'RatingController@editDB');

	##### AUDIOS #####
	Route::get('/audios', 'AudioController@index');
	Route::get('/audios/add', 'AudioController@add');
	Route::post('/audios/add', 'AudioController@addDB');
	Route::get('/audios/edit/{id_audio}', 'AudioController@edit');
	Route::post('/audios/edit/{id_audio}', 'AudioController@editDB');

	##### IDIOMAS #####
	Route::get('/languages', 'LanguageController@index');
	Route::get('/languages/add', 'LanguageController@add');
	Route::post('/languages/add', 'LanguageController@addDB');
	Route::get('/languages/edit/{id_language}', 'LanguageController@edit');
	Route::post('/languages/edit/{id_language}', 'LanguageController@editDB');

	##### AUDIOS #####
	Route::get('/subtitles', 'SubtitleController@index');
	Route::get('/subtitles/add', 'SubtitleController@add');
	Route::post('/subtitles/add', 'SubtitleController@addDB');
	Route::get('/subtitles/edit/{id_subtitle}', 'SubtitleController@edit');
	Route::post('/subtitles/edit/{id_subtitle}', 'SubtitleController@editDB');

	##### GRUPOS DE PROGRAMAS #####
	Route::get('/show-group', 'ShowGroupController@index');
	Route::get('/show-group/add', 'ShowGroupController@create');
	Route::post('/show-group/add', 'ShowGroupController@store');
	Route::get('/show-group/{id}', 'ShowGroupController@show');
	Route::get('/show-group/edit/{id}', 'ShowGroupController@edit');
	Route::post('/show-group/edit/{id}', 'ShowGroupController@update');
	Route::post('/show-group/remove/{id}', 'ShowGroupController@destroy');

	##### PROGRAMAS #####
	Route::get('/programs', 'ProgramController@index');
	Route::get('/programs/add', 'ProgramController@add');
	Route::post('/programs/add', 'ProgramController@addDB');
	Route::get('/programs/edit/{id_program}', 'ProgramController@edit');
	Route::get('/programs/show/{id_program}', 'ProgramController@show');
	Route::post('/programs/remove/{id_program}', 'ProgramController@destroy');
	Route::post('/programs/edit/{id_program}', 'ProgramController@editDB');
	Route::post('/programs/add-to-calendar', 'ProgramController@addToCalendar');
	Route::post('/programs/update-calendar', 'ProgramController@updateCalendarEvent');
	Route::post('/programs/remove-from-calendar', 'ProgramController@removeCalendarEvent');
	Route::post('/programs/clone-week', 'ProgramController@cloneWeek');
	Route::post('/programs/export-week', 'ProgramController@exportWeek');
	Route::post('programs/get-program-broadcasts', 'ProgramController@getProgramsBroadcasts');
	Route::post('/programs/get-channels', 'ProgramController@getChannels');
	Route::post('/programs/get-weeks', 'ProgramController@getWeeks');
	Route::post('/programs/create-excel', 'ProgramController@createExcel');
	Route::get('/programs/render-excel/{id_channel}/{week}', 'ProgramController@renderExcelView');
	Route::get('/programs/render-nielsen/{id_channel}/{week}', 'ProgramController@exportNielsen');
	Route::get('/programs/render-pdf/{id_channel}/{week}', 'ProgramController@createPDF');
});


	##### EXTERNOS #####
	Route::get('/external/schedule/{channel}', 'ExternalController@schedule');
	Route::get('/api/plazas/{plaza_key}/{channel}', 'PlazaController@getJSON');
