<!DOCTYPE html>
<html>
<head>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<style>

		.program:hover{
			background-color: #141414 !important;
		}

		.nav-link{
			background-color: gray !important;
			color: black !important;
			font-weight: bold;
			font-size: 20px;
			border:  2px solid black !important;
		}

		.nav-link:hover{
			border:  2px solid black !important;
		}


		.nav-link.active{
			background-color: #ffc107 !important;
			color: black !important;
			border:  2px solid black !important;
			font-weight: bold;
			font-size: 20px;
		}
		a{ 
			color: inherit; 
		} 

	</style>
</head>
<body class="bg-dark">



<div class="container">
	<!--MODAL-->
	<div class="modal fade" id="modal_descripcion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header text-center" style="background-color: #ffc107;">
	        <h5 class="modal-title" id="modal_title">Descripción</h5>
	      </div>
	      <div class="modal-body p-5 text-center font-weight-bold" style="font-size:20px;" id="modal_body_descripcion">
	        
	      </div>
	    </div>
	  </div>
	</div>
	<!--END MODAL-->


	<h4 class="text-warning text-center">{{$channel->name}}</h4>
	<div class="row">
		<nav>
			<div class="nav nav-justified nav-tabs d-flex flex-nowrap" id="week" role="tablist">
				<button class="p-0 nav-link active" id="nav-lunes-tab" data-bs-toggle="tab" data-bs-target="#nav-lunes" type="button" role="tab" aria-controls="nav-lunes" aria-selected="true">Lunes</button>
				<button class="p-0 nav-link" id="nav-martes-tab" data-bs-toggle="tab" data-bs-target="#nav-martes" type="button" role="tab" aria-controls="nav-martes" aria-selected="false">Martes</button>
				<button class="p-0 nav-link" id="nav-miercoles-tab" data-bs-toggle="tab" data-bs-target="#nav-miercoles" type="button" role="tab" aria-controls="nav-miercoles" aria-selected="false">Miércoles</button>
				<button class="p-0 nav-link" id="nav-jueves-tab" data-bs-toggle="tab" data-bs-target="#nav-jueves" type="button" role="tab" aria-controls="nav-jueves" aria-selected="false">Jueves</button>
				<button class="p-0 nav-link" id="nav-viernes-tab" data-bs-toggle="tab" data-bs-target="#nav-viernes" type="button" role="tab" aria-controls="nav-viernes" aria-selected="false">Viernes</button>
				<button class="p-0 nav-link" id="nav-sabado-tab" data-bs-toggle="tab" data-bs-target="#nav-sabado" type="button" role="tab" aria-controls="nav-sabado" aria-selected="false">Sábado</button>
				<button class="p-0 nav-link" id="nav-domingo-tab" data-bs-toggle="tab" data-bs-target="#nav-domingo" type="button" role="tab" aria-controls="nav-domingo" aria-selected="false">Domingo</button>
			</div>
		</nav>
		<div class="tab-content" id="week_content">
			<div class="tab-pane fade show active" id="nav-lunes" role="tabpanel" aria-labelledby="nav-lunes-tab">
				@foreach($programs_monday as $program)
				<div class="program row bg-black text-white p-3" style="border-bottom: 1px dashed gray; cursor:pointer;">
					<div class="col-3 fs-5 text-secondary">
						{{$program->start}}
					</div>
					<div class="col-7 fs-3">
						{{$program->program_name}}
					</div>
					<div class="col-2">
						<span style="display:none;">{{$program->program_name}}</span>
						<span style="display:none;">{{$program->description}}</span>
						<i class="fas fa-info-circle info" style="font-size:30px; float:left;"></i>
						<a href="{{$program->program->link}}" target='_blank'><i class="fas fa-chevron-right" style="font-size:30px; float:right;"></i></a>
					</div>
				</div>
				@endforeach
			</div>
			<div class="tab-pane fade" id="nav-martes" role="tabpanel" aria-labelledby="nav-martes-tab">
				@foreach($programs_tuesday as $program)
				<div class="program row bg-black text-white p-3" style="border-bottom: 1px dashed gray; cursor:pointer;">
					<div class="col-3 fs-5 text-secondary">
						{{$program->start}}
					</div>
					<div class="col-7 fs-3">
						{{$program->program_name}}
					</div>
					<div class="col-2">
						<span style="display:none;">{{$program->program_name}}</span>
						<span style="display:none;">{{$program->description}}</span>
						<i class="fas fa-info-circle info" style="font-size:30px; float:left;"></i>
						<a href="{{$program->program->link}}" target='_blank'><i class="fas fa-chevron-right" style="font-size:30px; float:right;"></i></a>
					</div>
				</div>
				@endforeach
			</div>
			<div class="tab-pane fade" id="nav-miercoles" role="tabpanel" aria-labelledby="nav-miercoles-tab">
				@foreach($programs_wednesday as $program)
				<div class="program row bg-black text-white p-3" style="border-bottom: 1px dashed gray; cursor:pointer;" >
					<div class="col-3 fs-5 text-secondary">
						{{$program->start}}
					</div>
					<div class="col-7 fs-3">
						{{$program->program_name}}
					</div>
					<div class="col-2">
						<span style="display:none;">{{$program->program_name}}</span>
						<span style="display:none;">{{$program->description}}</span>
						<i class="fas fa-info-circle info" style="font-size:30px; float:left;"></i>
						<a href="{{$program->program->link}}" target='_blank'><i class="fas fa-chevron-right" style="font-size:30px; float:right;"></i></a>
					</div>
				</div>
				@endforeach
			</div>
			<div class="tab-pane fade" id="nav-jueves" role="tabpanel" aria-labelledby="nav-jueves-tab">
				@foreach($programs_thursday as $program)
				<div class="program row bg-black text-white p-3" style="border-bottom: 1px dashed gray; cursor:pointer;">
					<div class="col-3 fs-5 text-secondary">
						{{$program->start}}
					</div>
					<div class="col-7 fs-3">
						{{$program->program_name}}
					</div>
					<div class="col-2">
						<span style="display:none;">{{$program->program_name}}</span>
						<span style="display:none;">{{$program->description}}</span>
						<i class="fas fa-info-circle info" style="font-size:30px; float:left;"></i>
						<a href="{{$program->program->link}}" target='_blank'><i class="fas fa-chevron-right" style="font-size:30px; float:right;"></i></a>
					</div>
				</div>
				@endforeach
			</div>
			<div class="tab-pane fade" id="nav-viernes" role="tabpanel" aria-labelledby="nav-viernes-tab">
				@foreach($programs_friday as $program)
				<div class="program row bg-black text-white p-3" style="border-bottom: 1px dashed gray; cursor:pointer;">
					<div class="col-3 fs-5 text-secondary">
						{{$program->start}}
					</div>
					<div class="col-7 fs-3">
						{{$program->program_name}}
					</div>
					<div class="col-2">
						<span style="display:none;">{{$program->program_name}}</span>
						<span style="display:none;">{{$program->description}}</span>
						<i class="fas fa-info-circle info" style="font-size:30px; float:left;"></i>
						<a href="{{$program->program->link}}" target='_blank'><i class="fas fa-chevron-right" style="font-size:30px; float:right;"></i></a>
					</div>
				</div>
				@endforeach
			</div>
			<div class="tab-pane fade" id="nav-sabado" role="tabpanel" aria-labelledby="nav-sabado-tab">
				@foreach($programs_saturday as $program)
				<div class="program row bg-black text-white p-3" style="border-bottom: 1px dashed gray; cursor:pointer;">
					<div class="col-3 fs-5 text-secondary">
						{{$program->start}}
					</div>
					<div class="col-7 fs-3">
						{{$program->program_name}}
					</div>
					<div class="col-2">
						<span style="display:none;">{{$program->program_name}}</span>
						<span style="display:none;">{{$program->description}}</span>
						<i class="fas fa-info-circle info" style="font-size:30px; float:left;"></i>
						<a href="{{$program->program->link}}" target='_blank'><i class="fas fa-chevron-right" style="font-size:30px; float:right;"></i></a>
					</div>
				</div>
				@endforeach
			</div>
			<div class="tab-pane fade" id="nav-domingo" role="tabpanel" aria-labelledby="nav-domingo-tab">
				@foreach($programs_sunday as $program)
				<div class="program row bg-black text-white p-3" style="border-bottom: 1px dashed gray; cursor:pointer;">
					<div class="col-3 fs-5 text-secondary">
						{{$program->start}}
					</div>
					<div class="col-7 fs-3">
						{{$program->program_name}}
					</div>
					<div class="col-2">
						<span style="display:none;">{{$program->program_name}}</span>
						<span style="display:none;">{{$program->description}}</span>
						<i class="fas fa-info-circle info" style="font-size:30px; float:left;"></i>
						<a href="{{url($program->program->link)}}" target='_blank'><i class="fas fa-chevron-right" style="font-size:30px; float:right;"></i></a>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</div>



</body>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
	<script>
		$('.info').on('click',function(){
			var descripcion = $(this).prev().text();
			var program_name = $(this).prev().prev().text();
			$('#modal_body_descripcion').text(descripcion);
			$('#modal_title').text(program_name);
			$('#modal_descripcion').modal('show');
		});
	</script>

</html>
