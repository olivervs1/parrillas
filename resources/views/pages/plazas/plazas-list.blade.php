@extends('template.main')
@section('content')
<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                  <div class="row">
                    <div class="col-2">
                  <a href="{{url('plazas/add')}}"><div class='btn btn-primary'>Nuevo</div></a>                     
                    </div>
                    <div class="col-10">
                      
                  <h1 class="m-0 text-dark">Plazas</h1>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="employees" class="table table-bordered table-hover" style="width:100%">
                  <thead>
                  <tr>
                    <th></th>
                    <th>id</th>
                    <th>Plaza</th>
                    <th>Nombre corto</th>
                    <th>Tipo de EPG</th>
                    <th>Fecha de creación</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($plazas as $p)
                  <tr>
                    <td>
                      <a href="{{url('plazas/'.$p->id_plaza)}}"><i class="far fa-eye text-secondary mr-2"></i></a>
                      <a href="{{url('plazas/edit/'.$p->id_plaza)}}"><i class="far fa-edit text-info mr-2"></i></a>
                      <a href="#" data-id_plaza="{{ $p->id_plaza }}"><i class="far fa-trash-alt text-danger"></i></a>
                    </td>
                    <td>{{$p->id_plaza}}</td>
                    <td>{{$p->name}}</td>
                    <td>{{$p->shortname}}</td>
                    <td>{{$p->EPG != NULL ? $p->EPG->name : "Aún no se ha seleccionado"}}</td>
                    <td>{{ date('d-m-Y', strtotime($p->created_at)) }}</td>
                  </tr>
                  @endforeach
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
                </div>
            </div>
        </div>
    </section>

</div>
@endsection

@section('scripts')
<script>
    $(document).ready( function () {
    $('#employees').DataTable({
      responsive: true
    });
    $('#employees').show();
    $(window).trigger('resize');
} );
</script>
@endsection