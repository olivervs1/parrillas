@extends('template.main')

@section('stylesheets')
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/fullcalendar/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/fullcalendar-daygrid/main.min.css')}}">
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/fullcalendar-timegrid/main.min.css')}}">
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/fullcalendar-bootstrap/main.min.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('admin_lte/css/adminlte.min.css')}}">
<style type="text/css">
  .hidden {
    display: none;
  }
</style>
@endsection

@section('content')
@php($id_channel = "")
<div class="d-flex justify-content-center">
  <div class="spinner-border" role="status">
    <span class="sr-only">Loading...</span>
  </div>
</div>

<input type="hidden" id="id_plaza" value="{{ $plaza->id_plaza }}"/>

<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('plazas')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              <h1 class="m-0 text-dark">Plazas</h1>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($plaza)
                <a href="{{ url('/plazas/edit/' . $plaza->id_plaza) }}" id="submit_btn" class='btn btn-success float-right'>Editar</a>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-3">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Nombre de plaza</span>
                    <span class="d-block">{{ $plaza->name }}</span>
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Clave de plaza</span>
                    <span class="d-block">{{ $plaza->shortname }}</span>
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Tipo de EPG</span>
                    <span class="d-block">{{ $plaza->EPG != NULL ? $plaza->EPG->name : 'No se ha seleccionado EPG' }}</span>
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Desfase horario (horas)</span>
                    <span class="d-block">{{ $plaza->jet_lag }}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>      
        </div> 
      </div>

      @if(isset($plaza))
        @if(count($plaza->channels) > 0)
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <ul class="nav nav-tabs" id="channels_list" role="tablist">
                    @foreach($plaza->channels as $k => $c)
                      <li class="nav-item">
                        @if(!isset($_GET['id_channel']))
                          @if($k == 0)
                            @php($id_channel = $c->id_channel)
                          @endif
                          <a class="nav-link {{ $k == 0 ? 'active' : '' }}" id="c_{{ $c->id_channel }}-tab" data-toggle="tab" href="#c_{{ $c->id_channel }}" role="tab" aria-controls="c_{{ $c->id_channel }}" aria-selected="true"  data-link="{{ url('/plazas/' . $plaza->id_plaza . '?id_channel=' . $c->id_channel) }}">{{ $c->principal_channel . '.' . $c->secondary_channel }}</a>
                        @else
                          @php($id_channel = $_GET['id_channel'])
                          <a class="nav-link {{ $c->id_channel == $_GET['id_channel'] ? 'active' : '' }}" id="c_{{ $c->id_channel }}-tab" data-toggle="tab" href="#c_{{ $c->id_channel }}" role="tab" aria-controls="c_{{ $c->id_channel }}" aria-selected="true" data-link="{{ url('/plazas/' . $plaza->id_plaza . '?id_channel=' . $c->id_channel) }}">{{ $c->principal_channel . '.' . $c->secondary_channel }}</a>
                        @endif
                      </li>
                    @endforeach
                  </ul>
                  <div class="tab-content" id="myTabContent">
                    @foreach($plaza->channels as $k => $c)
                      @if(!isset($_GET['id_channel']))
                        <div class="tab-pane fade {{ $k == 0 ? 'show active' : '' }} p-3" id="c_{{ $c->id_channel }}" role="tabpanel" aria-labelledby="c_{{ $c->id_channel }}-tab">
                      @else                        <div class="tab-pane fade {{ $c->id_channel == $_GET['id_channel'] ? 'show active' : '' }} p-3" id="c_{{ $c->id_channel }}" role="tabpanel" aria-labelledby="c_{{ $c->id_channel }}-tab">
                      @endif
                        <div class="row">
                          <?php if((!isset($_GET['id_channel']) && $k == 0) || (isset($_GET['id_channel']) && $c->id_channel == $_GET['id_channel'])) : ?>
                          <div class="col-3">
                            <div class="card programs_bar">
                              <div class="card-header">
                                <h4 class="card-title w-100 text-center">Programas</h4>
                              </div>
                              <div class="pt-2 pr-4 pl-4 pb-1">
                                <label>Busqueda</label>
                                <input type="text" class="form-control program-search" placeholder="Buscar..." />
                              </div>
                              <div class="pb-2 pr-4 pl-4 pt-0">
                                <label for="show_groups">Plazas</label>
                                <select id="show_plaza" class="form-control">
                                  <option value="0">Todas</option>
                                  @foreach($plazas as $p)
                                    <option value="{{ $p->id_plaza }}">{{ $p->name }}</option>
                                  @endforeach
                                </select>
                              </div>
                              <div class="pb-2 pr-4 pl-4 pt-0">
                                <label for="show_groups">Grupo de programa</label>
                                <select id="show_group" class="form-control">
                                  <option value="0">Todos</option>
                                  @foreach($show_groups as $sg)
                                    <option value="{{ $sg->id }}">{{ $sg->name }}</option>
                                  @endforeach
                                </select>
                              </div>
                              <hr/>
                              <div class="card-body">
                                <!-- Aqui van los tipos de actividad copn su respectivo color. al soltar mostrar modal con indo d-->
                                <?php if((!isset($_GET['id_channel']) && $k == 0) || (isset($_GET['id_channel']) && $c->id_channel == $_GET['id_channel'])) : ?>
                                  <div id="external-events">
                                    @foreach($programs as $p)
                                      <div id="p_{{ $p->id_program }}" class="external-event" style="background-color: {{ $p->color }};" data-duration="{{ $p->duration }}" data-name="{{ $p->name }}" data-description="{{ $p->description }}" data-channel="{{ $c->id_channel }}" data-id_show_group="{{ $p->id_show_group }}" data-id_plaza="{{ $p->id_plaza }}" data-cast1="{{ $p->cast1 }}"  data-cast2="{{ $p->cast2 }}" data-cast3="{{ $p->cast3 }}">{{ $p->name }}</div>
                                    @endforeach
                                    <div class="checkbox" class="hide" style="display:none;">
                                      <label for="drop-remove">
                                        <input type="checkbox" id="drop-remove">
                                        remove after drop
                                      </label>
                                    </div>
                                  </div>
                                <?php endif;?>
                              </div>
                            </div>
                          </div>
                          <div class="col-9">
                            <div class="row col-12">
                              <div class="col-12 text-right">
                                <span class="btn btn-primary create_new_program_modal" data-id_channel="{{ $c->id_channel }}">
                                  Alta masiva
                                  <i class="fas fa-tv ml-2"></i>
                                </span>
                                <span class="btn btn-primary clone_week_modal" data-id_channel="{{ $c->id_channel }}">
                                  Clonar semana
                                  <i class="fas fa-calendar-alt ml-2"></i>
                                </span>
                                <span class="btn btn-primary export_week_modal" data-id_channel="{{ $c->id_channel }}">
                                  Exportar programación
                                  <i class="fas fa-file-export ml-2"></i>
                                </span>
                                <span class="btn btn-primary" id="send_email_modal" data-id_channel="{{ $c->id_channel }}">
                                  Enviar correo
                                  <i class="fas fa-envelope ml-2"></i>
                                </span>
                                <span class="btn btn-primary create_excel_modal" data-id_channel="{{ $c->id_channel }}">
                                  Crear Excel
                                  <i class="fas fa-file-excel ml-2"></i>
                                </span>
                                <span class="btn btn-primary create_pdf_modal" data-id_channel="{{ $c->id_channel }}">
                                  Crear PDF
                                  <i class="fas fa-file-pdf ml-2"></i>
                                </span>
                              </div>
                                <div id="calendar"></div>
                            </div>
                          </div>
                          <?php endif; ?>
                        </div>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>      
            </div> 
          </div>
        @endif
      @endif
    </div>
  </section>

</div>

<!-- Modal agregar programa-->
<div class="modal" tabindex="-1" role="dialog" id="new_program">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar programa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="program_form" action="{{ url('programs/add-to-calendar') }}" method="POST">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="program_name">Programa</label>
            <input type="text" class="form-control" name="program_name" id="program_name"/>
            <input type="hidden" name="id_channel_program" id="id_channel_program" value="0"/>
            <input type="hidden" name="id_program" id="id_program" value="0"/>
            <input type="hidden" name="id_channel" id="id_channel" value="0"/>
            <input type="hidden" name="date" id="date" value=""/>
          </div>
          <div class="form-group">
            <label for="chapter_title">Título del capítulo</label>
            <input type="text" class="form-control" name="chapter_title" id="chapter_title"/>
          </div>
          <div class="form-group">
            <label for="begins">Inicio</label>
            <input type="text" name="begins" id="begins" class="form-control"/>
          </div>
          <div class="form-group">
            <label for="duration">Duración (minutos)</label>
            <input type="text" class="form-control" name="duration" id="duration"/>
          </div>
          <div class="form-group days_container">
            <label for="days">Días</label>
            <div class="row col-12">
              <div class="form-group col-3">
                <input type="checkbox" name="days[]" value="1" id="monday" class="day_1 show_days"/>  
                <label for="monday">Lunes</label>
              </div>
              <div class="form-group col-3">
                <input type="checkbox" name="days[]" value="2" id="tuesday" class="day_2 show_days"/>  
                <label for="tuesday">Martes</label>
              </div>
              <div class="form-group col-3">
                <input type="checkbox" name="days[]" value="3" id="wednesday" class="day_3 show_days"/>  
                <label for="wednesday">Miércoles</label>
              </div>
              <div class="form-group col-3">
                <input type="checkbox" name="days[]" value="4" id="thursday" class="day_4 show_days"/>  
                <label for="thursday">Jueves</label>
              </div>
              <div class="form-group col-3">
                <input type="checkbox" name="days[]" value="5" id="friday" class="day_5 show_days"/>  
                <label for="friday">Viernes</label>
              </div>
              <div class="form-group col-3">
                <input type="checkbox" name="days[]" value="6" id="saturday" class="day_6 show_days"/>  
                <label for="saturday">Sábado</label>
              </div>
              <div class="form-group col-3">
                <input type="checkbox" name="days[]" value="7" id="sunday" class="day_7 show_days"/>  
                <label for="sunday">Domingo</label>
              </div>
            </div>
            
          </div>
          <div class="form-group">
            <label for="cast1">Cast1</label>
            <input type="text" class="form-control" name="cast1" id="cast1"/>
          </div>
          <div class="form-group">
            <label for="cast2">Cast2</label>
            <input type="text" class="form-control" name="cast2" id="cast2"/>
          </div>
          <div class="form-group">
            <label for="cast3">Cast3</label>
            <input type="text" class="form-control" name="cast3" id="cast3"/>
          </div>
          <div class="form-group">
            <label for="note">Descripción</label>
            <textarea id="description" class="form-control" name="description"></textarea>
          </div>
          <div class="form-group other_broadcasts">
            <label for="note">Otras transmisiones</label>
            <div class="container row"></div>

          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button id="remove_program" type="button" class="btn btn-danger"><i class="fas fa-trash-alt mr-2"></i>Eliminar</button>
        <button id="save_program" type="button" class="btn btn-primary">Guardar</button>
        <button id="close_modal" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal alta masiva de programa -->
<div class="modal" tabindex="-1" role="dialog" id="new_massive_program">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar programa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="massive_program_form" action="{{ url('programs/add-to-calendar') }}" method="POST">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="program_name">Programa</label>
            <select class="form-control" id="massive_id_program" name="id_program">
              @foreach($programs as $p)
                <option value="{{ $p->id_program }}">{{ $p->name }}</div>
              @endforeach
            </select>
            <input type="hidden" name="id_channel" id="massive_id_channel" value="{{ $id_channel }}"/>
            <input type="hidden" name="is_massive" id="is_massive" value="1"/>
          </div>
          <div class="form-group">
            <label for="chapter_title">Título del capítulo</label>
            <input type="text" class="form-control" name="chapter_title" id="massive_chapter_title"/>
          </div>
          <div class="form-group">
            <label for="date">Fecha</label>
            <input type="date" name="date" id="massive_date" class="form-control"/>
          </div>
          <div class="form-group">
            <label for="begins">Inicio</label>
            <input type="text" name="begins" id="massive_begins" class="form-control"/>
          </div>
          <div class="form-group">
            <label for="duration">Duración (minutos)</label>
            <input type="text" class="form-control" name="duration" id="massive_duration"/>
          </div>
          <div class="form-group days_container">
            <label for="days">Días</label>
            <div class="row col-12">
              <div class="form-group col-3">
                <input type="checkbox" name="days[]" value="1" id="monday" class="day_1 massive_show_days"/>  
                <label for="monday">Lunes</label>
              </div>
              <div class="form-group col-3">
                <input type="checkbox" name="days[]" value="2" id="tuesday" class="day_2 massive_show_days"/>  
                <label for="tuesday">Martes</label>
              </div>
              <div class="form-group col-3">
                <input type="checkbox" name="days[]" value="3" id="wednesday" class="day_3 massive_show_days"/>  
                <label for="wednesday">Miércoles</label>
              </div>
              <div class="form-group col-3">
                <input type="checkbox" name="days[]" value="4" id="thursday" class="day_4 massive_show_days"/>  
                <label for="thursday">Jueves</label>
              </div>
              <div class="form-group col-3">
                <input type="checkbox" name="days[]" value="5" id="friday" class="day_5 massive_show_days"/>  
                <label for="friday">Viernes</label>
              </div>
              <div class="form-group col-3">
                <input type="checkbox" name="days[]" value="6" id="saturday" class="day_6 massive_show_days"/>  
                <label for="saturday">Sábado</label>
              </div>
              <div class="form-group col-3">
                <input type="checkbox" name="days[]" value="7" id="sunday" class="day_7 massive_show_days"/>  
                <label for="sunday">Domingo</label>
              </div>
            </div>
            
          </div>
          <div class="form-group">
            <label for="cast1">Cast1</label>
            <input type="text" class="form-control" name="cast1" id="cast1"/>
          </div>
          <div class="form-group">
            <label for="cast2">Cast2</label>
            <input type="text" class="form-control" name="cast2" id="cast2"/>
          </div>
          <div class="form-group">
            <label for="cast3">Cast3</label>
            <input type="text" class="form-control" name="cast3" id="cast3"/>
          </div>
          
          <div class="form-group">
            <label for="note">Descripción</label>
            <textarea id="description" class="form-control" name="description"></textarea>
          </div>
          <div class="form-group">
            <label for="note">Otras transmisiones</label>
            <div class="container row massive_other_broadcasts" style="max-height: 200px; overflow-y: scroll;">
              @foreach($plazas as $p)
                <div class="row">            
                  <span class="font-weight-bold">{{ $p->name }}</span>
                  <div class="row col-12">            
                    @foreach($p->Channels as $k => $ch)
                      <div class="col-3">
                        <input type="checkbox" value="{{ $ch->id_channel }}" id="{{ $p->id_plaza . '_' . $ch->id_channel }}"/>
                        <label for="{{ $p->id_plaza . '_' . $ch->id_channel }}">{{ $ch->principal_channel . '.' . $ch->secondary_channel }}</label>
                      </div>
                    @endforeach
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button id="massive_save_program" type="button" class="btn btn-primary">Guardar</button>
        <button id="close_modal" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal clonar semana -->
<div class="modal" tabindex="-1" role="dialog" id="clone_week">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Clonar semana</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="clone_week_form" action="{{ url('programs/add-to-calendar') }}" method="POST">
          {{ csrf_field() }}
          <div class="row col-12">
            <h5 class="col-12">Clonar de:</h5>
            <div class="form-group col-6">
              <label for="program_name">Plaza</label>
              <select class="form-control" id="from_plaza">
                <option value="0">Plaza</option>
                @foreach($plazas as $p)
                  <option value="{{ $p->id_plaza }}" {{ $plaza->id_plaza == $p->id_plaza ? "selected" : "" }}>{{ $p->name }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-6">
              <label for="program_name">Canal</label>
              <select class="form-control" id="from_channel">
                <option value="0">Canal</option>
                @foreach($plaza->channels as $c)
                  <option value="{{ $c->id_channel }}">{{ $c->principal_channel . '.' . $c->secondary_channel }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-12">
              <label for="program_name">Clonar semana</label>
              <select class="form-control" id="from_week">
                <option value="0">Semana</option>
                @foreach($dataWeeks as $w)
                  <option value="{{ $w['id'] }}" selected>S. {{ $w['week'] }} - ({{ $w['from'] }} - {{ $w['to'] }})</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="row col-12">
            <h5 class="col-12">A: </h5>
            <div class="form-group col-6">
              <label for="program_name">Plaza</label>
              <select class="form-control" id="to_plaza">
                <option value="0">Plaza</option>
                @foreach($plazas as $p)
                  <option value="{{ $p->id_plaza }}" {{ $plaza->id_plaza == $p->id_plaza ? "selected" : "" }}>{{ $p->name }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-6">
              <label for="program_name">Canal</label>
              <select class="form-control" id="to_channel">
                <option value="0">Canal</option>
                @foreach($plaza->channels as $c)
                  <option value="{{ $c->id_channel }}">{{ $c->principal_channel . '.' . $c->secondary_channel }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-12">
              <label for="program_name">Clonar semana</label>
              <select class="form-control" id="to_week">
                <option value="0">Semana</option>
                @php($actualWeek = date('W')-3)
                @for($i = 0; $i < 13; $i++)
                  @php($timestamp = mktime( 0, 0, 0, 1, 1,  date('Y') ) + ( $actualWeek * 7 * 24 * 60 * 60 ))
                  @php($timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 ))
                  @php($from = date( 'd-m-Y', $timestamp_for_monday ))
                  @php($to = date('d-m-Y', strtotime('+6 days', strtotime($from))))
                  <option value="{{ $actualWeek }}-{{ date('Y', strtotime($from)) }}">S. {{ $actualWeek }} - ({{ $from }} - {{ $to }})</option>
                  @if($actualWeek + 1 > 52)
                    @php($actualWeek = 1)
                  @else
                    @php($actualWeek++)
                  @endif
                @endfor
              </select>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button id="clone" type="button" class="btn btn-primary">Clonar</button>
        <button id="close_modal" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Exportar programación -->
<div class="modal" tabindex="-1" role="dialog" id="export_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Exportar programación</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="program_form" action="{{ url('programs/add-to-calendar') }}" method="POST">
          {{ csrf_field() }}
          <div class="row col-12">
            <div class="form-group col">
              <label for="program_name">Canal</label>
              <select class="form-control" id="export_channel">
                <option value="0">Canal</option>
                @foreach($plaza->channels as $c)
                  <option value="{{ $c->id_channel }}">{{ $c->principal_channel . '.' . $c->secondary_channel }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col">
              <label for="epg_name">Formato</label>
              <select class="form-control" name="epg_name" id="epg_name">
                <option value="EPG 1">.DAT</option>
                <option value="AVG">.TXT</option>
                <option value="XLS">.XLS</option>
                <option value="XLS2">.XLS2</option>
              </select>
            </div>
          </div>
          <div class="row col-12 fade select_days hidden">
            <div class="form-group col" >
              <label for="from">Desde</label>
              <input type="date" id="from" class="form-control" value="{{ date('Y-m-d') }}"/>
            </div>
            <div class="form-group col" >
              <label for="from">Días totales</label>
              <input type="text" id="days_qty" class="form-control" value="10"/>
            </div>
          </div>
          <div class="row col-12 select_week fade hidden">
            <div class="form-group col">
              <label for="program_name">Semana</label>
              <select class="form-control" id="export_week">
                <option value="0">Semana</option>
                @foreach($dataWeeks as $w)
                  <option value="{{ $w['week'] }}-{{ date('Y', strtotime($w['from'])) }}" selected>S. {{ $w['week'] }} - ({{ $w['from'] }} - {{ $w['to'] }})</option>
                @endforeach
              </select>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button id="export_btn" type="button" class="btn btn-primary">Exportar</button>
        <button id="close_modal" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Exportar programación -->
<div class="modal" tabindex="-1" role="dialog" id="excel_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Crear archivo Excel</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="program_form" action="{{ url('programs/add-to-calendar') }}" method="POST">
          {{ csrf_field() }}
          <div class="row col-12">
            <div class="form-group col-6">
              <label for="program_name">Canal</label>
              <select class="form-control" id="excel_channel">
                <option value="0">Canal</option>
                @foreach($plaza->channels as $c)
                  <option value="{{ $c->id_channel }}">{{ $c->principal_channel . '.' . $c->secondary_channel }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-6">
              <label for="program_name">Semana</label>
              <select class="form-control" id="excel_week">
                <option value="0">Semana</option>
                @foreach($dataWeeks as $w)
                  <option value="{{ $w['week'] }}-{{ date('Y', strtotime($w['from'])) }}" selected>S. {{ $w['week'] }} - ({{ $w['from'] }} - {{ $w['to'] }})</option>
                @endforeach
              </select>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button id="export_excel_btn" type="button" class="btn btn-primary">Crear archivo</button>
        <button id="close_modal" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Exportar programación (PDF) -->
<div class="modal" tabindex="-1" role="dialog" id="pdf_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Crear archivo PDF</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="program_form" action="{{ url('programs/add-to-calendar') }}" method="POST">
          {{ csrf_field() }}
          <div class="row col-12">
            <div class="form-group col-6">
              <label for="program_name">Canal</label>
              <select class="form-control" id="pdf_channel">
                <option value="0">Canal</option>
                @foreach($plaza->channels as $c)
                  <option value="{{ $c->id_channel }}">{{ $c->principal_channel . '.' . $c->secondary_channel }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-6">
              <label for="program_name">Semana</label>
              <select class="form-control" id="pdf_week">
                <option value="0">Semana</option>
                @foreach($dataWeeks as $w)
                  <option value="{{ $w['week'] }}-{{ date('Y', strtotime($w['from'])) }}" selected>S. {{ $w['week'] }} - ({{ $w['from'] }} - {{ $w['to'] }})</option>
                @endforeach
              </select>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button id="export_pdf_btn" type="button" class="btn btn-primary">Crear archivo</button>
        <button id="close_modal" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Enviar correo -->
<div class="modal" tabindex="-1" role="dialog" id="email_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Enviar correo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="program_form" action="{{ url('programs/send_email') }}" method="POST">
          {{ csrf_field() }}
          <div class="row col-12">
            <div class="form-group col">
              <label for="program_name">Canal</label>
              <select class="form-control" id="email_channel">
                <option value="0">Canal</option>
                @foreach($plaza->channels as $c)
                  <option value="{{ $c->id_channel }}">{{ $c->principal_channel . '.' . $c->secondary_channel }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col">
              <label for="program_name">Semana</label>
              <select class="form-control" id="email_week">
                <option value="0">Semana</option>
                @foreach($dataWeeks as $w)
                  <option value="{{ $w['week'] }}-{{ date('Y', strtotime($w['from'])) }}" selected>S. {{ $w['week'] }} - ({{ $w['from'] }} - {{ $w['to'] }})</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col">
              <label for="epg_name">Formato</label>
              <select class="form-control" name="epg_name" id="epg_name">
                <option value="PDF">PDF</option>
                <option value="EXCEL">EXCEL</option>
                <option value="EPG 1">.DAT</option>
                <option value="AVG">.TXT</option>
                <option value="XLS">.XLS</option>
              </select>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button id="send_email_btn" type="button" class="btn btn-primary">Enviar correo</button>
        <button id="close_modal" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal actualizar en plazas y canales -->
<div class="modal" tabindex="-1" role="dialog" id="update_all_broadcasts">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Actualización masiva</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="program_form" action="{{ url('programs/add-to-calendar') }}" method="POST">
          {{ csrf_field() }}
          <div class="row col-12">
            <div class="form-group col-12">
              <label for="program_name">Selecciona en que plazas y canales deseas actualizar este programa</label>
            </div>
            <div class="form-group col-12" id="broadcasts_data">
              
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button id="select_broadcasts_channels" type="button" class="btn btn-primary">Seleccionar</button>
        <button id="close_modal" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<input type="hidden" id="programs" value="{{ json_encode($programation) }}"/>

@endsection

@section('scripts')
<!-- fullCalendar 2.2.5 -->
<script src="{{ asset('admin_lte/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/fullcalendar/main.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/fullcalendar-daygrid/main.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/fullcalendar-timegrid/main.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/fullcalendar-interaction/main.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/fullcalendar-bootstrap/main.min.js') }}"></script>

<!-- Page specific script -->
<script>
  var $calendar = "";
  $(function () {

      /* initialize the external events
      -----------------------------------------------------------------*/
      function ini_events(ele) {
        ele.each(function () {

          // create an Event Object (https://fullcalendar.io/docs/event-object)
          // it doesn't need to have a start or end
          var eventObject = {
            title: $.trim($(this).text()) // use the element's text as the event title
        }

          // store the Event Object in the DOM element so we can get to it later
          $(this).data('eventObject', eventObject)

          // make the event draggable using jQuery UI
          $(this).draggable({
            zIndex        : 1070,
            revert        : true, // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        })

      })
      }

      ini_events($('#external-events div.external-event'))

      /* initialize the calendar
      -----------------------------------------------------------------*/
      //Date for the calendar events (dummy data)
      var date = new Date()
      var d    = date.getDate(),
      m    = date.getMonth(),
      y    = date.getFullYear()

      var Calendar = FullCalendar.Calendar;
      var Draggable = FullCalendarInteraction.Draggable;

      var containerEl = document.getElementById('external-events');
      var checkbox = document.getElementById('drop-remove');
      var calendarEl = document.getElementById('calendar');

      // initialize the external events
      // -----------------------------------------------------------------

      new Draggable(containerEl, {
        itemSelector: '.external-event',
        eventData: function(eventEl) {
          var duration = $(eventEl).data('duration');

          if(duration >= 60) {
            var hours = Math.floor(duration / 60);
            var minutes = duration - (hours * 60);
            duration = hours + ':' + minutes;
          } else {
            duration = '00:' + duration;
          }

          return {
            title: eventEl.innerText,
            duration: duration,
            backgroundColor: window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
            borderColor: window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
            textColor: window.getComputedStyle( eventEl ,null).getPropertyValue('color'),
          };
        }
      });

      var eventsData = [];
      $.each($.parseJSON($('input#programs').val()), function(i, ev) {
        
        var start = new Date(ev.year, ev.month -1 , ev.day, ev.hours, ev.minutes);
        var end = new Date(ev.end_year, ev.end_month -1, ev.end_day, ev.end_hours, ev.end_minutes);

        var program = {
          'title' : ev.title + '-' + ev.id,
          'start' : start ,
          'end' : end,
          'backgroundColor' : ev.backgroundColor,
          'allDay' : false,
          'id': ev.id,
          'id_program' : ev.id_program,
          'id_channel' : ev.id_channel,
          'chapter_title' : ev.chapter_title,
          'description' : ev.description,
          'duration'  : ev.duration,
          'cast1' : ev.cast1,
          'cast2' : ev.cast2,
          'cast3' : ev.cast3
        }

        eventsData.push(program);
      });

      var add = null;

      var calendar = new Calendar(calendarEl, {
        defaultView: 'timeGridWeek',
        locales: 'es',
        firstDay: 1,
        defaultDate: "{{ isset($_GET['date']) ? $_GET['date'] . 'T00:00:00' : date('Y-m-d') . 'T00:00:00' }}",
        plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid','dayGridDay'],
        header    : {
          left  : 'prev,next today',
          center: 'title',
          right : ''
        },
        slotLabelFormat: { 
          hour12: false, 
          hour: '2-digit', 
          minute: '2-digit' 
        },
        slotDuration: '00:15:00',
        snapDuration: '00:05:00',
        slotLabelInterval: 15,
        slotMinutes: 15,
        'themeSystem': 'bootstrap',
        buttonText: {
              today: "Hoy",
              month: "Mes",
              week: "Semana",
              day: "Día",
              list: "Agenda"
          },
          weekLabel: "Sm",
          allDayHtml: "Todo<br/>el día",
          eventLimitText: "más",
          noEventsMessage: "No hay eventos para mostrar",
        //Random default events
        events    : eventsData,
        editable  : true,
        droppable : true, // this allows things to be dropped onto the calendar !!!
      drop : function(info) {
          clearModal();

          var month = info.date.getMonth() + 1;
          var y = info.date.getFullYear();
          var m = ("0" + month ).slice(-2);
          var d = ("0" + info.date.getDate()).slice(-2);
          
          $('input#date').val(y + '-' + m + '-' + d);

          if(info.view.type != "dayGridMonth") {
            var H = ("0" + info.date.getHours()).slice(-2);
            var M = ("0" + info.date.getMinutes()).slice(-2);
            
            $('input#begins').val(H + ':' + M);
          }

          var day = info.date.getDay() == 0 ? 7 : info.date.getDay();
          $('input.day_' + day).prop('checked', 'checked').prop('disabled', 'disabled');
        },
      eventReceive: function(info) { 
        var id_program = $(info.draggedEl).attr('id').replace('p_','');

        $('input#id_program').val(id_program);
        $('input#id_channel').val($(info.draggedEl).data('channel'));
        $('input#program_name').val($(info.draggedEl).data('name'));
        $('input#duration').val($(info.draggedEl).data('duration'));
        $('input#cast1').val($(info.draggedEl).data('cast1'));
        $('input#cast2').val($(info.draggedEl).data('cast2'));
        $('input#cast3').val($(info.draggedEl).data('cast3'));
        $('textarea#description').text($(info.draggedEl).data('description'));
        $('div.days_container').slideDown();

        $('#new_program').modal();

        $('div.other_broadcasts').fadeOut();
        
        $('#new_program button#close_modal').on('click', function() {
          info.event.remove();
        });

        $('#new_program button.close').on('click', function() {
          info.event.remove();
        });
      },
      eventClick: function(info) {
        var ev_id = info.event.id;
        var id_program = info.event.extendedProps.id_program;
        var id_channel = info.event.extendedProps.id_channel;
        var duration = info.event.extendedProps.duration;
        var start_date = info.event.start;
        var program_name =info.event.title; 
        var chapter_title =info.event.extendedProps.chapter_title; 
        var description = info.event.extendedProps.description;
        var cast1 = info.event.extendedProps.cast1;
        var cast2 = info.event.extendedProps.cast2;
        var cast3 = info.event.extendedProps.cast3;

        var month = info.event.start.getMonth() + 1;
        var y = info.event.start.getFullYear();
        var m = ("0" + month ).slice(-2);
        var d = ("0" + info.event.start.getDate()).slice(-2);
          
        $('input#date').val(y + '-' + m + '-' + d);

        $('div.other_broadcasts').fadeIn();
        $('div.other_broadcasts div.container').html("");
        
        if(info.view.type != "dayGridMonth") {
          var H = ("0" + info.event.start.getHours()).slice(-2);
          var M = ("0" + info.event.start.getMinutes()).slice(-2);
          
          $('input#begins').val(H + ':' + M);           
        }

        getProgramBroadcasts(ev_id, $('input#begins').val(), $('input#date').val());
        
        $('button#remove_program').fadeIn();
        $('#new_program').modal('show');

        $('input#id_channel_program').val(ev_id);
        $('input#id_program').val(id_program);
        $('input#id_channel').val(id_channel);
        $('input#program_name').val(program_name);
        $('input#chapter_title').val(chapter_title);
        $('input#duration').val(duration);
        $('input#cast1').val(cast1);
        $('input#cast2').val(cast2);
        $('input#cast3').val(cast3);
        $('textarea#description').text(description);
        $('div.days_container').slideUp();
      },  
      eventResize: function(info) {
        var ev_id = info.event.id;
        var duration = 0;
        var start_date = info.event.start;

          if(info.view.type != "dayGridMonth") {
            var SH = ("0" + info.event.start.getHours()).slice(-2);
            var SM = ("0" + info.event.start.getMinutes()).slice(-2);

            var EH = ("0" + info.event.end.getHours()).slice(-2);
            var EM = ("0" + info.event.end.getMinutes()).slice(-2);

            var SHM = SH * 60;
            var EHM = EH * 60;

            var startM = SM + SHM;
            var endM = EM + EHM;
            var diff = Math.abs(info.event.start - info.event.end);

            duration = (diff / 1000) / 60;
            start_date = SH + ':' + SM;
          }

          var data = new FormData;
          data.append('id_channel_program', ev_id);
          data.append('duration', duration);
          data.append('start', start_date);

          $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('input[name="_token"]').val()
            },
              method: 'POST',
              url: url + '/programs/update-calendar',
              data: data,
              dataType:'json',
              processData: false,
            contentType: false,
            success:function(response) {
              if(response.message) {
                createAlert(response.message);
              } 
            },
          });
      },
      eventDrop: function(info) {
        var ev_id = info.event.id;
        var duration = 0;
        var start_date = info.event.start;
        var y = info.event.start.getFullYear();
        var month = info.event.start.getMonth() + 1;
        var m = ("0" + month ).slice(-2);
        var d = ("0" + info.event.start.getDate()).slice(-2);
        var h = 0;
        var i = 0;

          if(info.view.type != "dayGridMonth") {
            h = ("0" + info.event.start.getHours()).slice(-2);
            i = ("0" + info.event.start.getMinutes()).slice(-2);

            var SH = ("0" + info.event.start.getHours()).slice(-2);
            var SM = ("0" + info.event.start.getMinutes()).slice(-2);

            var EH = ("0" + info.event.end.getHours()).slice(-2);
            var EM = ("0" + info.event.end.getMinutes()).slice(-2);

            var SHM = SH * 60;
            var EHM = EH * 60;

            var startM = SM + SHM;
            var endM = EM + EHM;
            var diff = Math.abs(info.event.start - info.event.end);

            duration = (diff / 1000) / 60;
            start_date = SH + ':' + SM;
          }

          var data = new FormData;
          data.append('id_channel_program', ev_id);
          data.append('duration', duration);
          data.append('date', y + '-' + m + '-' + d);
          data.append('start', start_date);

          $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('input[name="_token"]').val()
            },
              method: 'POST',
              url: url + '/programs/update-calendar',
              data: data,
              dataType:'json',
              processData: false,
            contentType: false,
            success:function(response) {
              if(response.message) {
                createAlert(response.message);
              } 
            },
          });
      }
    });

      calendar.render();

      $calendar = calendar;
  });
</script>

<script src="{{ asset('js/pages/plazas/calendar.js') }}"></script>
@endsection