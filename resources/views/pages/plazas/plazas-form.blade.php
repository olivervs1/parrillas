@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('plazas')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              <h1 class="m-0 text-dark">Plazas</h1>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($plaza)
                <div id="submit_btn" class='btn btn-success float-right'>Actualizar</div>
              @else
                <div id="submit_btn" class='btn btn-success float-right'>Guardar</div>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              @isset($plaza)
                <form id="plazas_form" method="POST" action="{{url('/plazas/edit/'.$plaza->id_plaza)}}">
              @else
                <form id="plazas_form" method="POST" action="{{url('/plazas/add')}}">
              @endisset
              
                @csrf
                <div class="row">
                  <div class="col-3">
                    <div class="form-group">
                      <label for="name">Nombre de plaza</label>
                      <input type="text" class="form-control required" id="name" name="name" placeholder="Nombre de plaza" value="{{ isset($plaza->name) ? $plaza->name : ''}}"/>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label  for="active">Clave de plaza</label>
                      <input type="text" class="form-control" id="shortname" name="shortname" placeholder="Clave de plaza" value="{{ isset($plaza->shortname) ? $plaza->shortname : ''}}"/>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label  for="active">Tipo de EPG</label>
                      <select class="form-control" name="id_epg" id="id_epg">
                        <option value="0">--- Selecciona una EPG ---</option>
                        @foreach($epgs as $e)
                          <option value="{{ $e->id_epg }}" {{ isset($plaza) ? $plaza->id_epg == $e->id_epg ? "selected" : "" : "" }}>{{ $e->name }}</option>
                        @endforeach
                      </select>                       
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label for="jet_lag">Desfase horario</label>
                      <input type="text" class="form-control" id="jet_lag" name="jet_lag" placeholder="Ej. (+5, -1, 0)" value="{{ isset($plaza) ? $plaza->jet_lag : ''}}"/>
                    </div>
                  </div>
                </div>

                <div class="row col-12"> 
                  <div class="col-6">
                    <h5><i class="fas fa-user-plus mr-1"></i> Asignar usuarios</h5>
                    <div class="form-group row">
                      <div class="col-4">
                        <select id="users" class="form-control">
                          @foreach($users as $u)
                            <option value="{{ $u->id }}">{{ $u->name }}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="col-2">
                        <span class="btn btn-primary add_user">
                          <i class="fas fa-plus"></i>
                        </span>
                      </div>
                    </div>
                    <div class="form-group">
                      <table id="assigned_users" class="table">
                        <thead>
                          <tr>
                            <td>Usuarios</td>
                            <td></td>
                          </tr>
                        </thead>
                        <tbody>
                          @if(isset($plaza))
                            @foreach($plaza->Users as $k => $u)
                              @if($u->User != NULL)
                                <tr id="u_{{ $u->id_user }}">
                                  <td>{{ $u->User->name }}<input type="hidden" name="users[]" value="{{ $u->id_user }}"/></td>
                                  <td><span class="btn btn-danger remove_user"><i class="fas fa-times"></i></span></td>
                                </tr>
                              @endif
                            @endforeach
                          @endif
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="col-6">
                      <h5><i class="fas fa-user-plus mr-1"></i> Asignar correos</h5>
                      <div class="form-group row">
                        <div class="col-8">
                          <input id="assigned_email" type="text" placeholder="hola@ejemplo.com" class="form-control"/>
                        </div>
                        <div class="col-2">
                          <span class="btn btn-primary add_email" data-url="{{ url('/plazas/' . $plaza->id_plaza . '/add-email') }}">
                            <i class="fas fa-plus"></i>
                          </span>
                          <input type="hidden" id="remove_email_link" value="{{ url('/plazas/' . $plaza->id_plaza . '/remove-email') }}"/>
                        </div>
                      </div>
                      <div class="form-group">
                        <table id="assigned_emails" class="table">
                          <thead>
                            <tr>
                              <td>Correo</td>
                              <td></td>
                            </tr>
                          </thead>
                          <tbody>
                            @if(isset($plaza))
                              @foreach($plaza->Emails as $e)
                                <tr id="e_{{ $e->id }}">
                                  <td>{{ $e->email }}</td>
                                  <td><span class="btn btn-danger remove_email" data-email_id="{{ $e->id }}"><i class="fas fa-times"></i></span></td>
                                </tr>
                              @endforeach
                            @endif
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/pages/plazas/plazas-form.js') }}"></script>
@endsection
