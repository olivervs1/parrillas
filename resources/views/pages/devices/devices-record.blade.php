@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('devices')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              <h1 class="m-0 text-dark">Equipos</h1>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              <a href="{{ url('/devices/edit/' . $device->id ) }}" class='btn btn-success float-right'>Editar</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="form-group col-3">
                    <label for="name">STATION_NAME</label>
                    <span class="d-block">{{ $device->station_name }}</span>
                  </div>
                  <div class="form-group col-3">
                    <label for="name">IP_ADDRESS</label>
                    <span class="d-block">{{ $device->ip_address }}</span>
                  </div>
                  <div class="form-group col-3">
                    <label for="name">MODEL_NUMBER</label>
                    <span class="d-block">{{ $device->model_number }}</span>
                  </div>
                  <div class="form-group col-3">
                    <label for="name">SERIAL_NUMBER</label>
                    <span class="d-block">{{ $device->serial_number }}</span>
                  </div>
                  <div class="form-group col-3">
                    <label for="name">TIME_ZONE</label>
                    <span class="d-block">{{ $device->time_zone }}</span>
                  </div>
                  <div class="form-group col-3">
                    <label for="name">DST_ADJUSTMENT</label>
                    <span class="d-block">{{ $device->dst_adjustment }}</span>
                  </div>
                  <div class="form-group col-3">
                    <label for="name">DST_START_DATE</label>
                    <span class="d-block">{{ $device->dst_start_date }}</span>
                  </div>
                  <div class="form-group col-3">
                    <label for="name">DST_START_TIME</label>
                    <span class="d-block">{{ $device->dst_start_time }}</span>
                  </div>
                  <div class="form-group col-3">
                    <label for="name">DST_END_DATE</label>
                    <span class="d-block">{{ $device->dst_end_date }}</span>
                  </div>
                  <div class="form-group col-3">
                    <label for="name">DST_END_TIME</label>
                    <span class="d-block">{{ $device->dst_end_time }}</span>
                  </div>
                </div>

                <div class="row">
                  <h4>Canales</h3>
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Nombre</th>
                          <th class="text-center">Canal principal</th>
                          <th class="text-center">Canal secundario</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach(json_decode($device->channels) as $d)
                          <tr>
                            <td>{{ $d->channel_name != "NULL" ? $d->channel_name : "" }}</td>
                            <td class="text-center">{{ $d->principal_channel }}</td>
                            <td class="text-center">{{ $d->secondary_channel }}</td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  </div>
  @endsection

  @section('scripts')
  
  @endsection