@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('devices')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              <h1 class="m-0 text-dark">Equipos</h1>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($device)
                <div id="submit_btn" class='btn btn-success float-right' onclick="$('form#devices_form').submit()">Actualizar</div>
              @else
                <div id="submit_btn" class='btn btn-success float-right' onclick="$('form#devices_form').submit()">Guardar</div>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                @isset($device)
                  <form id="devices_form" method="POST" action="{{url('/devices/edit/'.$device->id)}}">
                @else
                  <form id="devices_form" method="POST" action="{{url('/devices/add')}}">
                @endisset
                  @csrf
                  <div class="row">
                    <div class="form-group col-3">
                      <label for="name">STATION_NAME</label>
                      <input type="text" class="form-control" id="name" name="station_name" placeholder="STATION_NAME" value="{{ isset($device) ? $device->station_name : '' }}"/>
                    </div>
                    <div class="form-group col-3">
                      <label for="name">IP_ADDRESS</label>
                      <input type="text" class="form-control" id="name" name="ip_address" placeholder="IP_ADDRESS" value="{{ isset($device) ? $device->ip_address : '' }}"/>
                    </div>
                    <div class="form-group col-3">
                      <label for="name">MODEL_NUMBER</label>
                      <input type="text" class="form-control" id="name" name="model_number" placeholder="MODEL_NUMBER" value="{{ isset($device) ? $device->model_number : '' }}"/>
                    </div>
                    <div class="form-group col-3">
                      <label for="name">SERIAL_NUMBER</label>
                      <input type="text" class="form-control" id="name" name="serial_number" placeholder="SERIAL_NUMBER" value="{{ isset($device) ? $device->serial_number : '' }}"/>
                    </div>
                    <div class="form-group col-3">
                      <label for="name">TIME_ZONE</label>
                      <input type="text" class="form-control" id="name" name="time_zone" placeholder="TIME_ZONE" value="{{ isset($device) ? $device->time_zone : '' }}"/>
                    </div>
                    <div class="form-group col-3">
                      <label for="name">DST_ADJUSTMENT</label>
                      <input type="text" class="form-control" id="name" name="dst_adjustment" placeholder="DST_ADJUSTMENT" value="{{ isset($device) ? $device->dst_adjustment : '' }}"/>
                    </div>
                    <div class="form-group col-3">
                      <label for="name">DST_START_DATE</label>
                      <input type="text" class="form-control" id="name" name="dst_start_date" placeholder="DST_START_DATE" value="{{ isset($device) ? $device->dst_start_date : '' }}"/>
                    </div>
                    <div class="form-group col-3">
                      <label for="name">DST_START_TIME</label>
                      <input type="text" class="form-control" id="name" name="dst_start_time" placeholder="DST_START_TIME" value="{{ isset($device) ? $device->dst_start_time : '' }}"/>
                    </div>
                    <div class="form-group col-3">
                      <label for="name">DST_END_DATE</label>
                      <input type="text" class="form-control" id="name" name="dst_end_date" placeholder="DST_END_DATE" value="{{ isset($device) ? $device->dst_end_date : '' }}"/>
                    </div>
                    <div class="form-group col-3">
                      <label for="name">DST_END_TIME</label>
                      <input type="text" class="form-control" id="name" name="dst_end_time" placeholder="DST_END_TIME" value="{{ isset($device) ? $device->dst_end_time : '' }}"/>
                    </div>
                  </div>

                  <div class="row">
                    <h4>Canales</h3>
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Nombre</th>
                            <th>Canal principal</th>
                            <th>Canal secundario</th>
                          </tr>
                        </thead>
                        <tbody>
                          @isset($device)
                            @foreach(json_decode($device->channels) as $c)
                              <tr>
                                <td>
                                  <input type="text" name="channel_name[]" placeholder="Nombre del canal" class="form-control-sm form-control" value="{{ $c->channel_name }}">
                                </td>
                                <td>
                                  <input type="text" name="principal_channel[]" placeholder="Canal principal" class="form-control-sm form-control" value="{{ $c->principal_channel }}">
                                </td>
                                <td>
                                  <input type="text" name="secondary_channel[]" placeholder="Canal secundario" class="form-control-sm form-control" value="{{ $c->secondary_channel }}">
                                </td>
                              </tr>
                            @endforeach
                          @else
                            @for($i = 1; $i <= 8; $i++)
                              <tr>
                                <td><input type="text" name="channel_name[]" placeholder="Nombre del canal" class="form-control-sm form-control"></td>
                                <td><input type="text" name="principal_channel[]" placeholder="Canal principal" value="3" class="form-control-sm form-control"></td>
                                <td><input type="text" name="secondary_channel[]" placeholder="Canal secundario" value="{{ $i }}" class="form-control-sm form-control"></td>
                              </tr>
                            @endfor
                          @endisset
                        </tbody>
                      </table>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  </div>
  @endsection

  @section('scripts')
  
  @endsection