@extends('template.main')
@section('content')
<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                  <div class="row">
                    <div class="col-2">
                  <a href="{{url('ratings/add')}}"><div class='btn btn-primary'>Nuevo</div></a>                     
                    </div>
                    <div class="col-10">
                      
                  <h1 class="m-0 text-dark">Clasificaciones</h1>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="employees" class="table table-bordered table-hover" style="width:100%">
                  <thead>
                  <tr>
                    <th></th>
                    <th>ID</th>
                    <th>Clasificación</th>
                    <th>Fecha de creación</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($ratings as $r)
                  <tr>
                    <td>
                      <a href="{{url('ratings/edit/'.$r->id_rating)}}"><i class="far fa-edit text-info mr-2"></i></a>
                      <a href="{{url('ratings/delete/'.$r->id_rating)}}"><i class="far fa-trash-alt text-danger"></i></a>
                    </td>
                    <td>{{$r->id_rating}}</td>
                    <td>{{$r->name}}</td>
                    <td>{{ date('d-m-Y', strtotime($r->created_at)) }}</td>
                  </tr>
                  @endforeach
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
                </div>
            </div>
        </div>
    </section>

</div>
@endsection

@section('scripts')
<script>
    $(document).ready( function () {
    $('#employees').DataTable({
      responsive: true
    });
    $('#employees').show();
    $(window).trigger('resize');
} );
</script>
@endsection