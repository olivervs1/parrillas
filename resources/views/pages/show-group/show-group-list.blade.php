@extends('template.main')
@section('content')
<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                  <div class="row">
                    <div class="col-2">
                  <a href="{{url('show-group/add')}}"><div class='btn btn-primary'>Nuevo</div></a>                     
                    </div>
                    <div class="col-10">
                      
                  <h1 class="m-0 text-dark">Grupos de programas</h1>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="employees" class="table table-bordered table-hover" style="width:100%">
                  <thead>
                  <tr>
                    <th></th>
                    <th>Nombre</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($show_groups as $sg)
                  <tr>
                    <td>
                      <a href="{{url('show-group/'.$sg->id)}}"><i class="far fa-eye text-secondary mr-2"></i></a>
                      <a href="{{url('show-group/edit/'.$sg->id)}}"><i class="far fa-edit text-info mr-2"></i></a>
                      <a href="{{url('show-group/delete/'.$sg->id)}}"><i class="far fa-trash-alt text-danger"></i></a>
                    </td>
                    <td>{{$sg->name}}</td>
                  </tr>
                  @endforeach
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
                </div>
            </div>
        </div>
    </section>

</div>
@endsection

@section('scripts')
<script>
    $(document).ready( function () {
    $('#employees').DataTable({
      responsive: true
    });
    $('#employees').show();
    $(window).trigger('resize');
} );
</script>
@endsection