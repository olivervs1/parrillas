@extends('template.main')

@section('stylesheets')
@endsection

@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('show-group')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              <h1 class="m-0 text-dark">Plazas</h1>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($show_group)
                <a href="{{ url('/shw-group/edit/' . $show_group->id) }}" id="submit_btn" class='btn btn-success float-right'>Editar</a>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Nombre de grupo</span>
                    <span class="d-block">{{ $show_group->name }}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>      
        </div> 
      </div>

      @if(count($show_group->Shows) > 0)
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <table id="programs" class="table">
                  <thead>
                    <tr>
                      <th>Programa</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($show_group->Shows as $s)
                      <tr>
                        <td><a href="{{ url('/programs/show/' . $s->id_program) }}" target="_blank">{{ $s->name }}</a></td>
                      </tr>  
                    @endforeach    
                  </tbody>        
                </table>
              </div>
            </div>
          </div> 
        </div>
      @endif
    </div>
  </section>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready( function () {
    $('#programs').DataTable({
      responsive: true
    });
    $('#programs').show();
    $(window).trigger('resize');
} );
</script>
@endsection