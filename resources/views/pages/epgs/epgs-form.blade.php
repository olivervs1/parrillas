@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('epgs')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              <h1 class="m-0 text-dark">EPGs</h1>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($epg)
                <div id="submit_btn" class='btn btn-success float-right'>Actualizar</div>
              @else
                <div id="submit_btn" class='btn btn-success float-right'>Guardar</div>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              @isset($epg)
                <form id="epgs_form" method="POST" action="{{url('/epgs/edit/'.$epg->id_epg)}}">
              @else
                <form id="epgs_form" method="POST" action="{{url('/epgs/add')}}">
              @endisset
              
                @csrf
                <div class="row">
                  <div class="col-4">
                    <div class="form-group">
                      <label for="name">Nombre de EPG</label>
                      <input type="text" class="form-control required" id="name" name="name" placeholder="Nombre de EPG" value="{{ isset($epg) ? $epg->name : '' }}"/>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/pages/epgs/epgs-form.js') }}"></script>
@endsection