@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('languages')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              <h1 class="m-0 text-dark">Idiomas</h1>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($language)
                <div id="submit_btn" class='btn btn-success float-right'>Actualizar</div>
              @else
                <div id="submit_btn" class='btn btn-success float-right'>Guardar</div>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              @isset($language)
                <form id="languages_form" method="POST" action="{{url('/languages/edit/'.$language->id_language)}}">
              @else
                <form id="languages_form" method="POST" action="{{url('/languages/add')}}">
              @endisset
              
                @csrf
                <div class="row">
                  <div class="col-4">
                    <div class="form-group">
                      <label for="name">Idioma</label>
                      <input type="text" class="form-control required" id="name" name="name" placeholder="idioma" value="{{ isset($language) ? $language->name : '' }}"/>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/pages/languages/languages-form.js') }}"></script>
@endsection