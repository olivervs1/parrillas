@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('audios')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              <h1 class="m-0 text-dark">Tipos de Audio</h1>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($audio)
                <div id="submit_btn" class='btn btn-success float-right'>Actualizar</div>
              @else
                <div id="submit_btn" class='btn btn-success float-right'>Guardar</div>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              @isset($audio)
                <form id="audios_form" method="POST" action="{{url('/audios/edit/'.$audio->id_audio)}}">
              @else
                <form id="audios_form" method="POST" action="{{url('/audios/add')}}">
              @endisset
              
                @csrf
                <div class="row">
                  <div class="col-4">
                    <div class="form-group">
                      <label for="name">Audio</label>
                      <input type="text" class="form-control required" id="name" name="name" placeholder="Audio" value="{{ isset($audio) ? $audio->name : '' }}"/>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/pages/audios/audios-form.js') }}"></script>
@endsection