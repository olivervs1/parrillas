@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('channels')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              <h1 class="m-0 text-dark">Canales</h1>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($channel)
                <a href="{{ url('/channels/edit/' . $channel->id_channel) }}" class='btn btn-success float-right'>Editar</a>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Nombre de Canal</span>
                    <span>{{ $channel->name }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Canal principal</span>
                    <span>{{ $channel->principal_channel }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Canal secundario</span>
                    <span>{{ $channel->secondary_channel }}</span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Plaza</span>
                    <span>{{ $channel->Plaza->name }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Equipo</span>
                    <span>{{ $channel->Device != NULL ? $channel->Device->station_name : "" }}</span>
                  </div>
                </div>
                @if($channel->thumbnail != "")
                  <div class="col-4">
                    <img src="{{ asset('/img/channels/' . $channel->thumbnail) }}" height="50px"/>
                  </div> 
                @endif
              </div>
            </div>
          </div>
      </div>
    </div>
  </section>

</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/pages/channels/channels-form.js') }}"></script>
@endsection