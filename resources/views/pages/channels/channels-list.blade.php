@extends('template.main')
@section('content')
<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                  <div class="row">
                    <div class="col-2">
                  <a href="{{url('channels/add')}}"><div class='btn btn-primary'>Nuevo</div></a>                     
                    </div>
                    <div class="col-10">
                      
                  <h1 class="m-0 text-dark">Canales</h1>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="employees" class="table table-bordered table-hover" style="width:100%">
                  <thead>
                  <tr>
                    <th></th>
                    <th>ID</th>
                    <th>Canal</th>
                    <th>Canal principal</th>
                    <th>Canal secundario</th>
                    <th>Plaza</th>
                    <th>Fecha de creación</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($channels as $c)
                  <tr>
                    <td>
                      <a href="{{url('channels/'.$c->id_channel)}}"><i class="far fa-eye text-secondary mr-2"></i></a>
                      <a href="{{url('channels/edit/'.$c->id_channel)}}"><i class="far fa-edit text-info mr-2"></i></a>
                      <a href="{{url('channels/delete/'.$c->id_channel)}}"><i class="far fa-trash-alt text-danger"></i></a>
                    </td>
                    <td>{{$c->id_channel}}</td>
                    <td>{{$c->name}}</td>
                    <td>{{$c->principal_channel}}</td>
                    <td>{{$c->secondary_channel}}</td>
                    <td>{{$c->Plaza != NULL ? $c->Plaza->name : 'Aún no esta asignada'}}</td>
                    <td>{{ date('d-m-Y', strtotime($c->created_at)) }}</td>
                  </tr>
                  @endforeach
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
                </div>
            </div>
        </div>
    </section>

</div>
@endsection

@section('scripts')
<script>
    $(document).ready( function () {
    $('#employees').DataTable({
      responsive: true
    });
    $('#employees').show();
    $(window).trigger('resize');
} );
</script>
@endsection