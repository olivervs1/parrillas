@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('channels')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              <h1 class="m-0 text-dark">Canales</h1>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($channel)
                <div id="submit_btn" class='btn btn-success float-right'>Actualizar</div>
              @else
                <div id="submit_btn" class='btn btn-success float-right'>Guardar</div>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          @isset($channel)
            <form id="channels_form" method="POST" action="{{url('/channels/edit/'.$channel->id_channel)}}" enctype="multipart/form-data">
          @else
            <form id="channels_form" method="POST" action="{{url('/channels/add')}}" enctype="multipart/form-data">
          @endisset
          <div class="card">
            <div class="card-body">
                @csrf
                <div class="row">
                  <div class="col-4">
                    <div class="form-group">
                      <label for="name">Nombre de Canal</label>
                      <input type="text" class="form-control required" id="name" name="name" placeholder="Nombre de Canal" value="{{ isset($channel) ? $channel->name : '' }}"/>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="principal_channel">Canal principal</label>
                      <input type="text" class="form-control required" id="principal_channel" name="principal_channel" placeholder="Canal principal" value="{{ isset($channel) ? $channel->principal_channel : '' }}"/>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="secondary_channel">Canal secundario</label>
                      <input type="text" class="form-control" id="secondary_channel" name="secondary_channel" placeholder="Canal secundario" value="{{ isset($channel) ? $channel->secondary_channel : '' }}"/>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-4">
                    <div class="form-group">
                      <label for="plaza">Plaza</label>
                      <select id="id_plaza" name="id_plaza" class="form-control">
                        <option value="0">--- Selecciona una plaza ---</option>
                        @foreach($plazas as $p)
                          <option value="{{ $p->id_plaza }}" {{ isset($channel) ? $channel->id_plaza == $p->id_plaza ? "selected" : "" : "" }}>{{ $p->name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="plaza">Equipo</label>
                      <select id="id_device" name="id_device" class="form-control">
                        <option value="0">--- Selecciona un equipo ---</option>
                        @foreach($devices as $d)
                          <option value="{{ $d->id }}" {{ isset($channel) ? $channel->id_device == $d->id ? "selected" : "" : "" }}>{{ $d->station_name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="secondary_channel">Imágen de canal</label>
                      <input type="file" class="form-control" id="thumbnail" name="thumbnail" accept=".jpg, .png, .jpeg, .svg" />
                    </div>
                  </div>
                </div>
              </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/pages/channels/channels-form.js') }}"></script>
@endsection