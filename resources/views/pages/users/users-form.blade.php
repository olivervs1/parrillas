@extends('template.main')

@section('content')
<div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <div class="row">
              <div class="col-2">
                <a href="{{url('users')}}"><div class='btn btn-secondary'>Listado</div></a>                   
              </div>
              <div class="col-10">
                <h1 class="m-0 text-dark">Usuarios</h1>
              </div>
            </div>
          </div>
          <div class="col-6">
            <div class="row">
              <div class="col-12 ">
                @isset($epg)
                  <div id="submit_btn" class='btn btn-success float-right'>Actualizar</div>
                @else
                  <div id="submit_btn" class='btn btn-success float-right'>Guardar</div>
                @endisset
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    @isset($user)
                        <form id="users_form" method="POST" action="{{ url('users/edit/' . $user->id_user) }}">
                    @else
                        <form id="users_form" method="POST" action="{{ url('users/add/') }}">
                    @endisset
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Nombre completo</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control required" name="name" value="{{ isset($user) ? $user->name : '' }}" required autocomplete="name" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Correo electrónico</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control required" name="email" value="{{ isset($user) ? $user->email : '' }}" required autocomplete="email">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Constraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
