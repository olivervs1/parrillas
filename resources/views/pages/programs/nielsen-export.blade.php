@php($months = array('', 'ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'))
  <table>
    @foreach($programs as $p)
      @php($date = date('n/j/Y', strtotime($p->date)))

      @php($hours = floor($p->duration / 60))
      @php($minutes = $p->duration - ($hours * 60))
      @php($minutes = str_pad($minutes, 2 ,'0', STR_PAD_LEFT))
      @php($days = $hours / 24)

      @php($end = date('H:i:s', strtotime('+' . $p->duration . ' minutes', strtotime($p->start))))

      @php($programName = mb_convert_encoding($p->program_name, 'UTF-8'))

      @if($p->chapter_title != "")
          @php($chapterTitle = $p->chapter_title)
      @endif

      @if($days > 1)
          @for ($i=0; $i < $days; $i++)
              @php($nDate = date('m/d/Y', strtotime('+' . $i . ' days', strtotime($p->date))))
              @php($remain = $p->duration - ($i * (24 * 60)))

              @php($end = "")

              @if($remain > (24 * 60))
                  @php($end = '24:00:00')
              @else
                  @php($hoursRemain = floor($remain / 60))
                  @php($minutes = $remain - ($hoursRemain * 60))
                  @php($minutes = str_pad($minutes, 2,'0', STR_PAD_LEFT))
                  @php($end = $hoursRemain . ':' . $minutes . ':00')
              @endif

              <tr>
                <td></td>
                <td>{{ str_pad(str_replace(':', '', $p->start), 2, '0', STR_PAD_LEFT) }}</td>
                <td>MULTIMEDIOS {{ $p->Channel->Plaza->name }}</td>
                <td></td>
                <td>{{ $programName }}</td>
                <td></td>
                <td></td>
                <td>{{ $p->Program->Genre->shortname }}</td>
                <td>{{ $months[date('n', strtotime($nDate))] . str_pad(date('d', strtotime($nDate)), 2, 0, STR_PAD_LEFT) }}</td>
              </tr>
          @endfor
      @else
        <tr>
          <td></td>
          <td>{{ str_pad(str_replace(':', '', $p->start), 2, '0', STR_PAD_LEFT) }}</td>
          <td>MULTIMEDIOS {{ $p->Channel->Plaza->name }}</td>
          <td></td>
          <td>{{ $programName }}</td>
          <td></td>
          <td></td>
          <td>{{ $p->Program->Genre->shortname }}</td>
          <td>{{ $months[date('n', strtotime($date))] . str_pad(date('d', strtotime($date)), 2, 0, STR_PAD_LEFT) }}</td>
        </tr>
      @endif
    @endforeach
  </table>