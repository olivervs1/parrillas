@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('programs')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              <h1 class="m-0 text-dark">Programas</h1>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($program)
                <div id="submit_btn" class='btn btn-success float-right'>Actualizar</div>
              @else
                <div id="submit_btn" class='btn btn-success float-right'>Guardar</div>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              @isset($program)
                <form id="programs_form" method="POST" action="{{url('/programs/edit/'.$program->id_program)}}">
              @else
                <form id="programs_form" method="POST" action="{{url('/programs/add')}}">
              @endisset
              
                @csrf
                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label for="name">Nombre del programa (Oficial)</label>
                      <input type="text" class="form-control required" id="name" name="name" placeholder="Nombre del programa" value="{{ isset($program) ? $program->name : '' }}"/>
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label for="guide_name">Nombre para guía (uso interno)</label>
                      <input type="text" class="form-control required" id="guide_name" name="guide_name" placeholder="Nombre del programa" value="{{ isset($program) ? $program->guide_name : '' }}"/>
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label for="duration">Duración (en minutos)</label>
                      <input type="text" class="form-control" id="duration" name="duration" placeholder="Duración" value="{{ isset($program) ? $program->duration : '' }}"/>
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label for="color">Color</label>
                      <input type="color" class="form-control" id="color" name="color" value="{{ isset($program) ? $program->color : '#FFC300' }}"/>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label for="release_year">Año de lanzamiento</label>
                      <input type="number" class="form-control required" id="release_year" name="release_year"value="{{ isset($program) ? $program->release_year : date('Y') }}"/>
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label for="cast1">Cast1</label>
                      <input type="text" class="form-control required" id="cast1" name="cast1"value="{{ isset($program) ? $program->cast1 : 'Cast1' }}"/>
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label for="cast2">Cast2</label>
                      <input type="text" class="form-control required" id="cast2" name="cast2"value="{{ isset($program) ? $program->cast2 : 'Cast2' }}"/>
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label for="cast3">Cast3</label>
                      <input type="text" class="form-control required" id="cast3" name="cast3"value="{{ isset($program) ? $program->cast3 : 'Cast3' }}"/>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label for="name">Descripción</label>
                      <textarea class="form-control" name="description">{{ isset($program) ? $program->description : '' }}</textarea>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-3">
                    <div class="form-group">
                      <label for="id_plaza">Plaza</label>
                      <select class="form-control" name="id_plaza">
                        <option value="0">--- Selecciona una plaza ---</option>
                        @foreach($plazas as $p)
                          <option value="{{ $p->id_plaza }}" {{ isset($program) ? $program->id_plaza == $p->id_plaza ? "selected" : "" : "" }}>{{ $p->name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label for="name">Clasificación</label>
                      <select class="form-control" name="id_rating">
                        <option value="0">--- Selecciona una clasificación ---</option>
                        @foreach($ratings as $r)
                          <option value="{{ $r->id_rating }}" {{ isset($program) ? $program->id_rating == $r->id_rating ? "selected" : "" : "" }}>{{ $r->name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label for="name">Género</label>
                      <select class="form-control" name="id_genre">
                        <option value="0">--- Selecciona un género ---</option>
                        @foreach($genres as $g)
                          <option value="{{ $g->id_genre }}" {{ isset($program) ? $program->id_genre == $g->id_genre ? "selected" : "" : "" }}>{{ $g->name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label for="id_language">Idioma</label>
                      <select class="form-control" name="id_language">
                        <option value="0">--- Selecciona un idioma ---</option>
                        @foreach($languages as $l)
                          <option value="{{ $l->id_language }}" {{ isset($program) ? $program->id_language == $l->id_language ? "selected" : "" : "" }}>{{ $l->name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label for="id_audio">Audio</label>
                      <select class="form-control" name="id_audio">
                        <option value="0">--- Selecciona un audio ---</option>
                        @foreach($audios as $a)
                          <option value="{{ $a->id_audio }}" {{ isset($program) ? $program->id_audio == $a->id_audio ? "selected" : "" : "" }}>{{ $a->name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label for="id_subtitle">Subtítulos</label>
                      <select class="form-control" name="id_subtitle">
                        <option value="0">--- Selecciona un subtitulo ---</option>
                        @foreach($subtitles as $s)
                          <option value="{{ $s->id_subtitle }}" {{ isset($program) ? $program->id_subtitle == $s->id_subtitle ? "selected" : "" : "" }}>{{ $s->name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div> 
                  <div class="col-3">
                    <div class="form-group">
                      <label for="sexual_content">Contenido sexual</label>
                      <select class="form-control" name="sexual_content">
                        <option value="0" {{ isset($program) ? $program->sexual_content == 0 ? "selected" : "" : "" }}>No</option>
                        <option value="1" {{ isset($program) ? $program->sexual_content == 1 ? "selected" : "" : "" }}>Si</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label for="violence">Violencia</label>
                      <select class="form-control" name="violence">
                        <option value="0" {{ isset($program) ? $program->violence == 0 ? "selected" : "" : "" }}>No</option>
                        <option value="1" {{ isset($program) ? $program->violence == 1 ? "selected" : "" : "" }}>Si</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label for="premiere">Premier</label>
                      <select class="form-control" name="premiere">
                        <option value="0" {{ isset($program) ? $program->premiere == 0 ? "selected" : "" : "" }}>No</option>
                        <option value="1" {{ isset($program) ? $program->premiere == 1 ? "selected" : "" : "" }}>Si</option>
                      </select>
                    </div>
                  </div> 
                  <div class="col-3">
                    <div class="form-group">
                      <label for="hd">Alta definición</label>
                      <select class="form-control" name="hd">
                        <option value="0" {{ isset($program) ? $program->hd == 0 ? "selected" : "" : "" }}>No</option>
                        <option value="1" {{ isset($program) ? $program->hd == 1 ? "selected" : "" : "" }}>Si</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label for="production_type">Tipo de producción</label>
                      <select class="form-control" name="production_type">
                        <option value="0" {{ isset($program) ? $program->production_type == 0 ? "selected" : "" : "" }}>Propio</option>
                        <option value="1" {{ isset($program) ? $program->production_type == 1 ? "selected" : "" : "" }}>Externo</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label for="id_show_group">Grupo de programas</label>
                      <select class="form-control" name="id_show_group">
                        <option value="0">--- Selecciona un grupo de programa ---</option>
                        @foreach($show_groups as $sg)
                          <option value="{{ $sg->id }}" {{ isset($program) ? $program->id_show_group == $sg->id ? "selected" : "" : "" }}>{{ $sg->name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label for="link">Link</label>
                        <input type="text" class="form-control required" id="link" name="link" value="{{ isset($program) ? $program->link : 'https://www.multimedios.com/' }}"/>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/pages/programs/programs-form.js') }}"></script>
@endsection