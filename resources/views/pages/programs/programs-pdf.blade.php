<style type="text/css">
  * {
    @if($intervals_of_15 > 0)
      font-size: 8px;
    @else
      font-size: 12px;
    @endif
    font-family: Georgia, serif;
  }
  table {
    max-width: 100%;
    border-collapse: collapse;
  }
  .w-100 {
    width: 100%;
  }
  .fs-16 {
    font-size: 16px;
  }
  .fs-10 tr td, 
  .fs-10 th {
    @if($intervals_of_15 > 0)
      font-size: 6px;
    @else
      font-size: 10px;
    @endif
  }
  .fs-8 {
    @if($intervals_of_15 > 0)
      font-size: 6px !important;
    @else
      font-size: 10px !important;
    @endif
    
  }
  .text-center {
    text-align: center;
  }
  .bordered tr td {
    border: 0.25px solid #000;
    border-spacing: 0px;
  }
  .bg-black {
    background-color: #000;
    color: #FFF;
  }
  .float-left {
    float: left;
  }
  .mt--10 {
    margin-top: -30px;
  }
</style>
  @php($days = array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'))
  @php($months = array('', 'ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul','ago','sep','oct','nov','dic'))
  @php($time = 24 * 30 )
  <table class="w-100 mt--10">
      <tr>
        <td style="width: 5%;">
          @if($channel->thumbnail != "")
            <img src="{{ asset('img/channels/' . $channel->thumbnail) }}" height="50px"/>
          @endif
        </td>
        <td rowspan="2" class="text-center fs-16" colspan="8">
          {{ $plaza }} <br/>
          Semana: {{ $week }}
        </td>
      </tr>
  </table>
  <table class="bordered">
    <thead>
      <tr class="bg-black">
        <th></th>
        @foreach($days as $k => $d)
          <th class="text-center">
            {{ strtoupper($d) }} <br/>
            @php($actualDay = date('d-m-Y', strtotime('+' . $k . ' day', strtotime($monday))))
            {{ date('d-', strtotime($actualDay)) }} {{ $months[date('n', strtotime($actualDay))] }}
          </th>
        @endforeach
        <th></th>
      </tr>
    </thead>
    <tbody class="fs-10">
        @php($days = array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado','Domingo'))
        @php($squares = 24 * 2)
        @php($intervals = $intervals_of_15 > 0 ? 15 : 30)
        @if($intervals_of_15 > 0)
          @php($squares = 24 * 4)
        @endif

        @php($hasTD = array())
        @for($i = 0; $i < $squares; $i++)
          <tr>
            @php($time = $i * $intervals)
            @php($hours = str_pad(floor($time / 60), '2', '0', STR_PAD_LEFT))
            @php($mins = str_pad($time - ($hours * 60), '2', '0', STR_PAD_LEFT))
            @php($hour = $hours . ':' . $mins)

            @php($timeF = ($i+1) * $intervals)
            @php($hoursF = str_pad(floor($timeF / 60), '2', '0', STR_PAD_LEFT))
            @php($minsF = str_pad($timeF - ($hoursF * 60), '2', '0', STR_PAD_LEFT))
            @php($hourF = $hoursF . ':' . $minsF)
            <td class="fs-8">
              {{ $hour }}
            </td>
            @foreach($days as $k => $d)
              @php($k++)

              @isset($programsArr[$k][$hour])
                @for($j = 0; $j < $programsArr[$k][$hour]['rowspan']; $j++)
                  @php($timeH = $i * $intervals)
                  @if($j > 0)
                    @php($timeH = ($i+$j) * $intervals)
                  @endif
                  @php($hoursH = str_pad(floor($timeH / 60), '2', '0', STR_PAD_LEFT))
                  @php($minsH = str_pad($timeH - ($hoursH * 60), '2', '0', STR_PAD_LEFT))
                  @php($hourH = $hoursH . ':' . $minsH)

                  @php($hasTD[$k][$hourH] = true)
                @endfor

                @if(($i + $programsArr[$k][$hour]['rowspan']) > $squares)
                  @php ($maxRP = $squares - $i)
                  <td rowspan="{{ $maxRP }}" class="align-middle text-center">
                    {{ $programsArr[$k][$hour]['title'] }}
                  </td>
                @else
                  <td rowspan="{{ $programsArr[$k][$hour]['rowspan'] }}" class="align-middle text-center">
                    {{ $programsArr[$k][$hour]['title'] }}
                  </td>
                @endif
              @else
                @if(!isset($hasTD[$k][$hour]) && $i+1 <= $squares)
                  <td></td>
                @endif
              @endisset
            @endforeach
            <td class="fs-8">
              {{ $hour }}
            </td>
          </tr>
        @endfor
    </tbody>
  </table>