@extends('template.main')
@section('content')
<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                  <div class="row">
                    <div class="col-2">
                  <a href="{{url('programs/add')}}"><div class='btn btn-primary'>Nuevo</div></a>                     
                    </div>
                    <div class="col-10">
                      
                  <h1 class="m-0 text-dark">Programas</h1>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="programs" class="table table-bordered table-hover" style="width:100%">
                  <thead>
                  <tr>
                    <th></th>
                    <th>ID</th>
                    <th>Programa</th>
                    <th>Grupo de programa</th>
                    <th>Link</th>
                    <th>Fecha de creación</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($programs as $p)
                  <tr>
                    <td>
                      <a href="{{url('programs/show/'.$p->id_program)}}"><i class="far fa-eye text-info mr-2"></i></a>
                      <a href="{{url('programs/edit/'.$p->id_program)}}" target="_blank"><i class="far fa-edit text-info mr-2"></i></a>
                      <i class="far fa-trash-alt text-danger remove_program" data-id_program="{{ $p->id_program }}"></i>
                    </td>
                    <td>{{$p->id_program}}</td>
                    <td>{{$p->name}}</td>
                    <td>{{$p->ShowGroup != NULL ? $p->ShowGroup->name : '' }}</td>
                    <td>{{$p->link}}</td>
                    <td>{{ date('d-m-Y', strtotime($p->created_at)) }}</td>
                  </tr>
                  @endforeach
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="remove_program">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">¿Estás seguro que quieres eliminar este programa?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Una ves eliminado no se podrá recuperar este programa o su información.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button id="removeConfirm" type="button" class="btn btn-danger">Confirmar <i class="fas fa-trash-alt ml-2"></i></button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready( function () {
    $('#programs').DataTable({
      responsive: true
    });
    $('#programs').show();
    $(window).trigger('resize');
} );
</script>

<script type="text/javascript" src="{{ asset('js/pages/programs/programs-list.js') }}"></script>
@endsection