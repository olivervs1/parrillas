@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('programs')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              <h1 class="m-0 text-dark">Programas</h1>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
                <a href="{{ url('/programs/edit/' . $program->id_program) }}" class='btn btn-success float-right'>Editar</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <div class="form-group">
                    <label for="name">Nombre del programa (Oficial)</label>
                    <span class="d-block"/>{{ $program->name }}</span>
                  </div>
                </div>
                <div class="col">
                  <div class="form-group">
                    <label for="guide_name">Nombre para guía (uso interno)</label>
                    <span class="d-block"/>{{ $program->guide_name }}</span>
                  </div>
                </div>
                <div class="col">
                  <div class="form-group">
                    <label for="duration">Duración (en minutos)</label>
                    <span class="d-block"/>{{ $program->duration }}</span>
                  </div>
                </div>
                <div class="col">
                  <div class="form-group">
                    <label for="color">Color</label>
                    <span class="alert d-block" style="background-color: {{ $program->color != '' ? $program->color : '#FFC300' }}"></span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <div class="form-group">
                    <label for="name">Descripción</label>
                    <p>{{ $program->description }}</p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-3">
                  <div class="form-group">
                    <label for="id_plaza">Plaza</label>
                    <span>{{ $program->Plaza != NULL ? $program->Plaza->name : '' }}</span>
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <label for="name">Clasificación</label>
                    <span>{{ $program->Rating != NULL ? $program->Rating->name : '' }}</span>
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <label for="name">Género</label>
                    <span>{{ $program->Genre != NULL ? $program->Genre->name : '' }}</span>
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <label for="id_language">Idioma</label>
                    <span>{{ $program->Language != NULL ? $program->Language->name : '' }}</span>
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <label for="id_audio">Audio</label>
                    <span>{{ $program->Audio != NULL ? $program->Audio->name : '' }}</span>
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <label for="id_subtitle">Subtítulos</label>
                    <span>{{ $program->Subtitle != NULL ? $program->Subtitle->name : '' }}</span>
                  </div>
                </div> 
                <div class="col-3">
                  <div class="form-group">
                    <label for="sexual_content">Contenido sexual</label>
                    <span>{{ $program->sexual_content == 1 ? 'Si' : 'No' }}</span>
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <label for="violence">Violencia</label>
                    <span>{{ $program->violence == 1 ? 'Si' : 'No' }}</span>
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <label for="premiere">Contenido de pago</label>
                    <span>{{ $program->premiere == 1 ? 'Si' : 'No' }}</span>
                  </div>
                </div> 
                <div class="col-3">
                  <div class="form-group">
                    <label for="hd">Alta definición</label>
                    <span>{{ $program->hd == 1 ? 'Si' : 'No' }}</span>
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <label for="production_type">Tipo de producción</label>
                    <span>{{ $program->production_type == 1 ? 'Si' : 'No' }}</span>
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <label for="id_show_group">Grupo de programas</label>
                    <span>{{ $program->ShowGroup != NULL ? $program->ShowGroup->name : '' }}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/pages/programs/programs-form.js') }}"></script>
@endsection