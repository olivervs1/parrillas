  @php($days = array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'))
  @php($months = array('', 'ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul','ago','sep','oct','nov','dic'))
  @php($time = 24 * 30 )
  <table>
      <tr>
        <td></td>
        <td colspan="8" rowspan="2" class="text-center">{{ $plaza }}</td>
      </tr>
      <tr>
      </tr>
      <tr>
        <td></td>
        <td colspan="8">Semana: {{ $week }}</td>
      </tr>
  </table>
  <table>
    <thead>
      <tr>
        <th></th>
        @foreach($days as $d)
          <th class="text-center">{{ strtoupper($d) }}</th>
        @endforeach
        <th></th>
      </tr>
      <tr>
        <th></th>
        @for($i = 0; $i < 7; $i++)
          @php($actualDay = date('d-m-Y', strtotime('+' . $i . ' day', strtotime($monday))))
          <th class="text-center">{{ date('d-', strtotime($actualDay)) }} {{ $months[date('n', strtotime($actualDay))] }}</th>
        @endfor
        <th></th>
      </tr>
    </thead>
    <tbody>
        @php($days = array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado','Domingo'))
        @php($squares = 24 * 4)
        @php($hasTD = array())
        @for($i = 0; $i < $squares; $i++)
          <tr>
            @php($time = $i * 15)
            @php($hours = str_pad(floor($time / 60), '2', '0', STR_PAD_LEFT))
            @php($mins = str_pad($time - ($hours * 60), '2', '0', STR_PAD_LEFT))
            @php($hour = $hours . ':' . $mins)

            @php($timeF = ($i+1) * 15)
            @php($hoursF = str_pad(floor($timeF / 60), '2', '0', STR_PAD_LEFT))
            @php($minsF = str_pad($timeF - ($hoursF * 60), '2', '0', STR_PAD_LEFT))
            @php($hourF = $hoursF . ':' . $minsF)
            <td>
              {{ $hour }}
            </td>
            @foreach($days as $k => $d)
              @php($k++)

              @isset($programsArr[$k][$hour])
                @for($j = 0; $j < $programsArr[$k][$hour]['rowspan']; $j++)
                  @php($timeH = $i * 15)
                  @if($j > 0)
                    @php($timeH = ($i+$j) * 15)
                  @endif
                  @php($hoursH = str_pad(floor($timeH / 60), '2', '0', STR_PAD_LEFT))
                  @php($minsH = str_pad($timeH - ($hoursH * 60), '2', '0', STR_PAD_LEFT))
                  @php($hourH = $hoursH . ':' . $minsH)

                  @php($hasTD[$k][$hourH] = true)
                @endfor

                @if(($i + $programsArr[$k][$hour]['rowspan']) > $squares)
                  @php ($maxRP = $squares - $i)
                  <td rowspan="{{ $maxRP }}" class="align-middle text-center">
                    {{ $programsArr[$k][$hour]['title'] }}
                  </td>
                @else
                  <td rowspan="{{ $programsArr[$k][$hour]['rowspan'] }}" class="align-middle text-center">
                    {{ $programsArr[$k][$hour]['title'] }}
                  </td>
                @endif
              @else
                @if(!isset($hasTD[$k][$hour]) && $i+1 <= $squares)
                  <td></td>
                @endif
              @endisset
            @endforeach
            <td>
              {{ $hour }}
            </td>
          </tr>
        @endfor
    </tbody>
  </table>