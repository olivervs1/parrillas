<script src="{{asset('admin_lte/plugins/jquery/jquery.min.js')}}"></script>

<!-- ESTA COMENTADO PORQUE ME TRUENA EL DATATABLES

<script src="{{ asset('js/app.js') }}" defer></script>

 -->


<!-- jQuery UI 1.11.4 -->
<script src="{{asset('admin_lte/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->

<script src="{{asset('admin_lte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('admin_lte/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('admin_lte/plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<script src="{{asset('admin_lte/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('admin_lte/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('admin_lte/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('admin_lte/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('admin_lte/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('admin_lte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('admin_lte/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('admin_lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('admin_lte/js/adminlte.js')}}"></script>

<!-- DATA TABLES -->
<script src="{{asset('admin_lte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin_lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('admin_lte/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('admin_lte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>


<script src="{{asset('admin_lte/plugins/bootstrap-slider/bootstrap-slider.min.js')}}"></script>

<script src="{{ asset('js/timetable.min.js') }}" defer></script>

<!-- Funciones generales -->
<script type="text/javascript" src="{{ asset('js/functions.js') }}"></script>
