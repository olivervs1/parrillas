<aside class="main-sidebar sidebar-dark-warning elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link navbar-dark">
      <img src="{{asset('img/logo.svg')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: 1">
      <span class="brand-text font-weight-light">EPG</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('admin_lte/img/avatar5.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          
          <li class="nav-header"></li>
          <li class="nav-header">Módulos</li>
          <li class="nav-item">
            <a href="{{url('users')}}" class="nav-link">
              <i class="nav-icon fas fa-user-alt"></i>
              <p>
                Usuarios
              </p>
            </a>
          </li>
          
          <li class="nav-header">Catálogos</li>
          <li class="nav-item">
            <a href="{{url('plazas')}}" class="nav-link">
              <i class="nav-icon fas fa-map-marked-alt"></i>
              <p>
                Plazas
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('epgs')}}" class="nav-link">
              <i class="nav-icon fas fa-list"></i>
              <p>
                EPGs
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('channels')}}" class="nav-link">
              <i class="nav-icon fas fa-tv"></i>
              <p>
                Canales
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('genres')}}" class="nav-link">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Géneros
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('ratings')}}" class="nav-link">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Clasificaciones
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('audios')}}" class="nav-link">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Tipos de Audio
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('languages')}}" class="nav-link">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Idiomas
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('subtitles')}}" class="nav-link">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Subtítulos
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('programs')}}" class="nav-link">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Programas
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('devices')}}" class="nav-link">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Equipos
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('show-group')}}" class="nav-link">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Grupos de programas
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
