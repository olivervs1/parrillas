$(document).ready(function() {
	$('table#programs').on('click', 'i.remove_program', removeShow);
});

var removeShow = function() {
	var id_show = $(this).data('id_program');

	$('div#remove_program').modal();

	$('button#removeConfirm').on('click', function() {
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('input[name="_token"]').val()
			},
		    method: 'POST',
		    url: 'programs/remove/' + id_show,
		    dataType:'json',
		    processData: false,
			contentType: false,
			success:function(response){				

				if(response.message) {
					createAlert(response.message);

					$('.alertWindowOk').on('click', function() {
						if(response.status) {
							location.reload();
						}
					})
				}
			},
		});
	});
}