$(document).ready(function() {
	$('div#submit_btn').on('click', submitForm);
	$('span.add_user').on('click', addUser);
	$('span.add_channel').on('click', addChannel);
	$('.remove_user').on('click', removeUser);
	$('.remove_channel').on('click', removeChannel);
	$('span.add_email').on('click', addUser);
	$('span.remove_email').on('click', removeEmail);
});

var submitForm = function(e) {
	e.preventDefault();

	if(validateForm()) {
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('input[name="_token"]').val()
			},
		    method: 'POST',
		    url: $('form#plazas_form').attr('action'),
		    data: new FormData($('form#plazas_form')[0]),
		    dataType:'json',
		    processData: false,
			contentType: false,
			success:function(response){				

				if(response.message) {
					createAlert(response.message);
				}

				if(response.status) {
					if(response.redirectTo) {
						window.location = response.redirectTo;
					}
				}	
			},
		});
	}
}

var addUser = function() {
	var id_user = $('select#users').val();
	var name = $('select#users option:selected').text();
	
	if($('tr#u_' + id_user).length == 0) {
		var html = '<tr	id="u_' + id_user + '">';
		html += '<td>' + name + '<input type="hidden" name="users[]" value="' + id_user + '"/></td>';
		html += '<td class="text-center"><input type="checkbox" name="send_email['+ id_user + ']" checked/></td>';
		html += '<td><span class="btn btn-danger remove_user"><i class="fas fa-times"></i></span></td>';
		html += '</tr>';

		$('table#assigned_users tbody').append(html);
		$('.remove_user').unbind('click');
		$('.remove_user').on('click', removeUser);
	}
}

var removeUser = function() {
	var _this = $(this);
	createAlert('¿Estás seguro que deseas eliminar a este usuario de esta plaza?', 'Advertencia','<i class="fas fa-exclamation-circle"></i>',2);

	$('.alertWindowOk').on('click', function() {
		_this.parents('tr').remove();
	})
}

var addChannel = function() {
	var id_channel = $('select#channels').val();
	var channel = $('select#channels option:selected').text();
	
	if($('tr#c_' + id_channel).length == 0) {
		var html = '<tr	id="c_' + id_channel + '">';
		html += '<td>' + channel + '<input type="hidden" name="channels[]" value="' + id_channel + '"/></td>';
		html += '<td><span class="btn btn-danger remove_channel"><i class="fas fa-times"></i></span></td>';
		html += '</tr>';

		$('table#assigned_channels tbody').append(html);
		$('.remove_channel').unbind('click');
		$('.remove_channel').on('click', removeChannel);
	}
}

var removeChannel = function() {
	var _this = $(this);
	createAlert('s¿Estás seguro que deseas eliminar a este canal de esta plaza?', 'Advertencia','<i class="fas fa-exclamation-circle"></i>',2);

	$('.alertWindowOk').on('click', function() {
		_this.parents('tr').remove();
	})
}

var addUser = function() {
	var email = $('input#assigned_email').val();
	var url = $(this).data('url');
	var add = true;

	if(email == undefined || email == "") {
		add = false;
		$('input#assigned_email').addClass('border-danger');
	}

	if(add) {
		$('input#assigned_email').removeClass('border-danger');	
		
		var data = new FormData;
		data.append('email', email);

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('input[name="_token"]').val()
			},
		    method: 'POST',
		    url: url,
		    data: data,
		    dataType:'json',
		    processData: false,
			contentType: false,
			beforeSend: function() {
				$('span.add_email').html('<i class="fas fa-circle-notch fa-spin"></i>');
				$('span.add_email').prop('disabled', 'disabled');
			},
			success:function(response) {				
				if(response.status) {
					$('input#assigned_email').val("");
					$('span.add_email').html('<i class="fas fa-plus"></i>');
					$('span.add_email').prop('disabled', '');

					if(response.email) {
						var html = '<tr id="e_' + response.email.id +'">';
							html += '<td>';
								html += email;
							html += '</td>';
							html += '<td>';
								html += '<span class="btn btn-danger remove_email" data-email_id="' + response.email.id + '"><i class="fas fa-times">';
							html += '</td>';
						html += '</tr>';

						$('table#assigned_emails tbody').append(html);
						$('span.remove_email').unbind('click');
						$('span.remove_email').on('click', removeEmail);
					}
				}	
			},
		});
	}
}

var removeEmail = function() {
	var _this = $(this);
	var email_id = $(this).data('email_id');
	var removeLink = $('input#remove_email_link').val();
	var data = new FormData;
	data.append('email_id', email_id);

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('input[name="_token"]').val()
		},
	    method: 'POST',
	    url: removeLink,
	    data: data,
	    dataType:'json',
	    processData: false,
		contentType: false,
		beforeSend: function() {
			_this.html('<i class="fas fa-circle-notch fa-spin"></i>');
			_this.prop('disabled', 'disabled');
		},
		success:function(response) {				
			if(response.status) {
				$('tr#e_' + email_id).remove();
			}	
		},
	});
}