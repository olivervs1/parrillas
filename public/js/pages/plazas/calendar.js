var xhr = null;

$(document).ready(function() {
	$('button#show_new_activity_modal').on('click', clearModal);
	$('button#save_program').on('click', saveProgram);
	$('button#massive_save_program').on('click', saveMassiveProgram);
	$('button#remove_program').on('click', removeProgram);
	
	/***** BUSQUEDA *****/	
	$('select#show_group').on('change', search);
	$('select#show_plaza').on('change', search);
	$('input.program-search').on('keyup', search);

	resizeProgramsCards();

	/***** CREAR EXCEL *****/
	$('span.create_excel_modal').on('click', createExcelModal);
	$('button#export_excel_btn').on('click', exportExcel);

	/***** CREAR PDF *****/
	$('span.create_pdf_modal').on('click', createPDFModal);
	$('button#export_pdf_btn').on('click', exportPDF);

	/***** EXPORTAR *****/
	$('span.export_week_modal').on('click', exportWeekModal);
	$('button#export_btn').on('click', exportWeek);


	/***** ENVIAR CORREO *****/
	$('span#send_email_modal').on('click', sendEmailModal);

	/***** CLONAR *****/
	$('select#from_plaza').on('change', reloadChannels);
	$('select#to_plaza').on('change', reloadToChannels);

	$('select#from_channel').on('change', reloadWeeks);
	$('span.clone_week_modal').on('click', cloneWeekModal);
	$('button#clone').on('click', clone);

	$('ul#channels_list li a').on('click', viewChannel);

	$('select#epg_name').on('change', showExportOptions);

	$('span.create_new_program_modal').on('click', showMassiveCreateProgramModal);

	showExportOptions();
});

var showMassiveCreateProgramModal = function() {
	$('div#new_massive_program').modal();
}

var getProgramBroadcasts = function(id_channel_program, start, date) {
	var spinner = '<div class="w-100 text-center py-4"><i class="fa fa-spinner" aria-hidden="true"></i></div>';
	$('div.other_broadcasts div.container').html(spinner);
  var data = new FormData;
  data.append('id_channel_program', id_channel_program);
  data.append('date', date);
  data.append('start', start);

  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('input[name="_token"]').val()
    },
      method: 'POST',
      url: url + '/programs/get-program-broadcasts',
      data: data,
      dataType:'json',
      processData: false,
    contentType: false,
    success:function(response) {
      if(response.status) {
        if(response.data) {
          var html = "";
          $.each(response.data, function(i, elem) {
            html += '<div class="col-4"><input type="checkbox" value="'+ elem.id_channel_program +'" id="b_'+i+'" checked="checked"/><label for="b_'+i+'">' + elem.plaza + ' - '  + elem.channel + '</label></input/></div>';
          });

          $('div.other_broadcasts div.container').html(html);
        }
      } else {
      	$('div.other_broadcasts').fadeOut();
      }
    },
  });
}

var clearModal = function() {
	$('input#id_program').val(0);
	$('input#date').val("");
	$('input#begins').val("");
	$('input#duration').val("30");
	$('input#chapter_title').val("");
	$('textarea#description').text("");
	$('input#id_channel_program').val(0);
	
	$.each($('input.show_days'), function(i, elem) {
		$(elem).removeAttr('checked');
		$(elem).removeAttr('disabled');
	});

	$('button#remove_program').fadeOut();
}

var saveProgram = function(e) {
	e.preventDefault();

	var add = true;
	var date = $('input#date').val();
	var begins = $('input#begins').val();
	var duration = $('input#duration').val();

	if(date == null || date == "" || date == undefined) {
		add = false;
	}

	if(begins == null || begins == "" || begins == undefined) {
		add = false;
	}

	if(duration == null || duration == "" || duration == undefined || isNaN(duration)) {
		add = false;
	}

	var days = [];
	var checked = 0;
	$.each($('input.show_days'), function(i, elem) {
		if($(elem).is(':checked')) {
			checked++;
			days.push($(elem).val());
		}
	});

	var id_channel_program = $('input#id_channel_program').val();
	if(checked == 0 && id_channel_program == 0) {
		add = false;
	}

	if(add) {
		var formData = new FormData($('form#program_form')[0]);
		formData.append('days', days);

		var updateIn = [];

		if($('div.other_broadcasts').is(':visible')) {
			$.each($('div.other_broadcasts div.container input:checked'), function(i, elem) {
				updateIn.push($(elem).val());
			});
		}

		formData.append('updateIn', updateIn);

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('input[name="_token"]').val()
			},
		    method: 'POST',
		    url: $('form#program_form').attr('action'),
		    data: formData,
		    dataType:'json',
		    processData: false,
			contentType: false,
			success:function(response){			
				if(response.status) {
					var $_GET = getUrlParams();

					if($_GET.id_channel != undefined) {
						document.location = document.location.href + '&date=' + date;	
					} else if($_GET.date != undefined) {
						document.location = document.location.href.replace('?date=' + $_GET.date, '?date=' + date);	
					} else {
						document.location = document.location.href + '?date=' + date;	
					}
				}	
			},
		});
	}
}

var saveMassiveProgram = function(e) {
	e.preventDefault();

	var add = true;
	var id_program = $('select#massive_program').val();
	var date = $('input#massive_date').val();
	var begins = $('input#massive_begins').val();
	var duration = $('input#massive_duration').val();

	if(date == null || date == "" || date == undefined) {
		add = false;
	}

	if(begins == null || begins == "" || begins == undefined) {
		add = false;
	}

	if(duration == null || duration == "" || duration == undefined || isNaN(duration)) {
		add = false;
	}

	var days = [];
	var checked = 0;
	$.each($('input.massive_show_days'), function(i, elem) {
		if($(elem).is(':checked')) {
			checked++;
			days.push($(elem).val());
		}
	});

	if(add) {
		var formData = new FormData($('form#massive_program_form')[0]);
		formData.append('days', days);

		var updateIn = [];

		if($('div.massive_other_broadcasts').is(':visible')) {
			$.each($('div.massive_other_broadcasts input:checked'), function(i, elem) {
				updateIn.push($(elem).val());
			});
		}

		formData.append('updateIn', updateIn);

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('input[name="_token"]').val()
			},
		    method: 'POST',
		    url: $('form#massive_program_form').attr('action'),
		    data: formData,
		    dataType:'json',
		    processData: false,
			contentType: false,
			success:function(response){			
				if(response.status) {
					var $_GET = getUrlParams();

					if($_GET.id_channel != undefined) {
						document.location = document.location.href + '&date=' + date;	
					} else if($_GET.date != undefined) {
						document.location = document.location.href.replace('?date=' + $_GET.date, '?date=' + date);	
					} else {
						document.location = document.location.href + '?date=' + date;	
					}
				}	
			},
		});
	}
}

var removeProgram = function() {
	var id_channel_program = $('input#id_channel_program').val();
	var id_channel = $('input#id_channel').val();
	var id_program = $('input#id_program').val();
	var date = $('input#date').val();

	createAlert('¿Estas seguro que deseas eliminar este programa?','','',2,'Si','Cancelar');

	$('.alertWindowCancel').unbind('click');

	$('.alertWindowOk').on('click', function() {
		$('div.alertWindow').remove();
		
		var data = new FormData;
		data.append('id_channel_program', id_channel_program);
		data.append('id_channel', id_channel);
		data.append('id_program', id_program);
		data.append('date', date);

		if($('div.other_broadcasts').is(':visible')) {
			var removeIn = [];

			if($('div.other_broadcasts').is(':visible')) {
				$.each($('div.other_broadcasts div.container input:checked'), function(i, elem) {
					removeIn.push($(elem).val());
				});
			}

			data.append('removeIn', removeIn);
		}

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('input[name="_token"]').val()
			},
		    method: 'POST',
		    url: url + '/programs/remove-from-calendar',
		    data: data,
		    dataType:'json',
		    processData: false,
			contentType: false,
			success:function(response){			
				if(response.status) {
					var $_GET = getUrlParams();

					if($_GET.id_channel != undefined) {
						document.location = document.location.href + '&date=' + date;	
					} else if($_GET.date != undefined) {
						document.location = document.location.href.replace('?date=' + $_GET.date, '?date=' + date);	
					} else {
						document.location = document.location.href + '?date=' + date;	
					}
				}	

				if(response.message) {
					createAlert(response.message);
				}
			},
		});
	});
	
	$('.alertWindowCancel').unbind('click');

	$('.alertWindowCancel').on('click', function() {
		$('div.alertWindow').remove();
	});
}

var resizeProgramsCards = function () {
	var h = $('div.fc-bootstrap').css('height');
	$('div.programs_bar').css('height', h);


	var h = $('div.fc-time-grid-container').css('height');
	$('div.fc-time-grid-container').css('height', h);

	$('div.fc-time-grid-container.fc-scroller').css('overflow', 'hidden scroll');

	var w = $('th.fc-axis').css('width');
	$('th.fc-axis').css('width', w);
}	

var cloneWeekModal = function() {
	var id_channel = $(this).data('id_channel');
	$('div#clone_week').modal();

	console.log(id_channel);

	$('select#from_channel option[value=' + id_channel + ']').prop('selected', 'selected');	
	$('select#to_channel option[value=' + id_channel + ']').prop('selected', 'selected');	
}

var clone = function() {
	if(xhr != null) {
		return false;
	}

	showLoader();

	var from_channel = $('select#from_channel').val();
	var from_week = $('select#from_week').val();
	var to_channel = $('select#to_channel').val();
	var to_week = $('select#to_week').val();
	var add = true;
	var date = convert($calendar.getDate());

	if(from_channel == 0) {
		createAlert('Debes seleccionar un canal de origen válido.');
		add = false;
		return 1;
	}

	if(from_week == 0) {
		createAlert('Debes seleccionar una semana de origen válida.');
		add = false;
		return 1;
	}

	if(to_channel == 0) {
		createAlert('Debes seleccionar el canal de destino válido.');
		add = false;
		return 1;
	}

	if(to_week == 0) {
		createAlert('Debes seleccionar una semana de destino válida.');
		add = false;
		return 1;
	}

	if(add) {
		var data = new FormData;
		data.append('from_channel', from_channel);
		data.append('from_week', from_week);
		data.append('to_channel', to_channel);
		data.append('to_week', to_week);

		xhr = $.ajax({
			headers: {
				'X-CSRF-TOKEN': $('input[name="_token"]').val()
			},
		    method: 'POST',
		    url: url + '/programs/clone-week',
		    data: data,
		    dataType:'json',
		    processData: false,
			contentType: false,
			success:function(response){		
				xhr = null;
				closeLoader();
				if(response.message) {
					createAlert(response.message);

					$('.alertWindowOk').on('click', function() {
						if(response.status) {
							var $_GET = getUrlParams();

							if($_GET.id_channel != undefined) {
								document.location = document.location.href + '&date=' + date;	
							} else if($_GET.date != undefined) {
								document.location = document.location.href.replace('?date=' + $_GET.date, '?date=' + date);	
							} else {
								document.location = document.location.href + '?date=' + date;	
							}
						}	
					});
				}
			},
		});
	}
}

var viewChannel = function() {
	var link = $(this).data('link');
	console.log(link);
	document.location = link;
}

var exportWeekModal = function() {
	var id_channel = $(this).data('id_channel');
	$('div#export_modal').modal();
	$('select#export_channel option[value=' + id_channel + ']').prop('selected', 'selected');
}

var exportWeek = function() {
	var id_plaza = $('input#id_plaza').val();
	var id_channel = $('select#export_channel').val();
	var week = $('select#export_week').val();
	var epg_name = $('select#epg_name').val();
	var from = $('input#from').val();
	var days = $('input#days_qty').val();

	var data = new FormData;
	data.append('id_plaza', id_plaza);
	data.append('id_channel', id_channel);
	data.append('week', week);
	data.append('epg_name', epg_name);
	data.append('from', from);
	data.append('days', days);

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('input[name="_token"]').val()
		},
	    method: 'POST',
	    url: url + '/programs/export-week',
	    data: data,
	    dataType:'json',
	    processData: false,
		contentType: false,
		success:function(response){			
			if(response.message) {
				createAlert(response.message);

				if(response.status) {
					$('.alertWindowOk').on('click', function() {
						window.open(response.file);
					});
				}
			}
		},
	});
}

var reloadChannels = function() {
	var id_plaza = $(this).val();

	if(id_plaza != null && id_plaza != 0) {
		var data = new FormData;
		data.append('id_plaza', id_plaza);

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('input[name="_token"]').val()
			},
		    method: 'POST',
		    url: url + '/programs/get-channels',
		    data: data,
		    dataType:'json',
		    processData: false,
			contentType: false,
			success:function(response){			
				if(response.status) {
					if(response.channels) {
						if(response.channels.length > 0) {
							var html = "";
							html += '<option value="0" selected>Canal</option>';
							$.each(response.channels, function(i, elem) {
								html += '<option value="' + elem.id_channel + '">' + elem.principal_channel + '.' + elem.secondary_channel + '</option>';
							});

							$('select#from_channel').html(html);
						} else {
							var html = "";
							$('select#from_channel').html(html);

							createAlert('Aún no se han asignado canales a esta plaza, por favor selecciona otra.');
						}
					}
				} else {
					createAlert(response.message);
				}
			},
		});
	}

	$('div#export_modal').modal('hide');
}

var reloadToChannels = function() {
	var id_plaza = $(this).val();

	if(id_plaza != null && id_plaza != 0) {
		var data = new FormData;
		data.append('id_plaza', id_plaza);

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('input[name="_token"]').val()
			},
		    method: 'POST',
		    url: url + '/programs/get-channels',
		    data: data,
		    dataType:'json',
		    processData: false,
			contentType: false,
			success:function(response){			
				if(response.status) {
					if(response.channels) {
						if(response.channels.length > 0) {
							var html = "";
							html += '<option value="0" selected>Canal</option>';
							$.each(response.channels, function(i, elem) {
								html += '<option value="' + elem.id_channel + '">' + elem.principal_channel + '.' + elem.secondary_channel + '</option>';
							});

							$('select#to_channel').html(html);
						} else {
							var html = "";
							$('select#to_channel').html(html);

							createAlert('Aún no se han asignado canales a esta plaza, por favor selecciona otra.');
						}
					}
				} else {
					createAlert(response.message);
				}
			},
		});
	}

	$('div#export_modal').modal('hide');
}

var reloadWeeks = function() {
	var id_channel = $(this).val();

	if(id_channel != 0 && id_channel != null) {
		var data = new FormData;
		data.append('id_channel', id_channel);

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('input[name="_token"]').val()
			},
		    method: 'POST',
		    url: url + '/programs/get-weeks',
		    data: data,
		    dataType:'json',
		    processData: false,
			contentType: false,
			success:function(response){			
				if(response.status) {
					if(response.message) {
						createAlert(response.message);
						return 3;	
					}

					if(response.weeks) {
						var html = "";
						html += '<option value="0" selected>Semana</option>';
						$.each(response.weeks, function(i, elem) {
							html += '<option value="' + elem.value + '">S.' + elem.week + ' - (' + elem.from + ' - ' + elem.to + ') </option>';
						});

						$('select#from_week').html(html);
					}
				}
			},
		});	
	}
}

var createExcelModal = function() {
	var id_channel = $(this).data('id_channel');
	$('div#excel_modal').modal();
	$('select#excel_channel option[value=' + id_channel + ']').prop('selected', 'selected');
}

var sendEmailModal = function() {
	var id_channel = $(this).data('id_channel');
	$('div#email_modal').modal();
	$('select#email_channel option[value=' + id_channel + ']').prop('selected', 'selected');
}

var createPDFModal = function() {
	var id_channel = $(this).data('id_channel');
	$('div#pdf_modal').modal();
	$('select#pdf_channel option[value=' + id_channel + ']').prop('selected', 'selected');
}

var exportExcel = function() {
	var id_channel = $('select#excel_channel').val();
	var week = $('select#excel_week').val();

	if(id_channel != 0 && week != 0) {
		window.open(url + '/programs/render-excel/' + id_channel + '/' + week);
	}
}

var exportPDF = function() {
	var id_channel = $('select#pdf_channel').val();
	var week = $('select#pdf_week').val();

	if(id_channel != 0 && week != 0) {
		window.open(url + '/programs/render-pdf/' + id_channel + '/' + week);
	}
}

var search = function() {
	var querystring = $('.program-search').val().toLowerCase();
	var id_show_group = $('select#show_group').val();
	var id_plaza = $('select#show_plaza').val();

	if(querystring.length > 0 ) {
		console.log('Entro aqui no se porque');
		$.each($('.external-event'), function(i, elem) {
			var visible = true;
			var show_group = $(elem).data('id_show_group');
			var plaza = $(elem).data('id_plaza');
			var  txt = $(elem).text().toLowerCase();
			
			if(txt.indexOf(querystring) != -1){
				if($(elem).is(':visible') == false) {
			    	$(elem).fadeIn('fast');
			    }
			} else {
				if($(elem).is(':visible')) {
					$(elem).fadeOut('fast');	
				}
				visible = false;
			}

			if(id_show_group != 0 && visible) {
				if(id_show_group == show_group) {
					if($(elem).is(':visible') == false) {
						$(elem).fadeIn('fast');	
					}
				} else {
					$(elem).fadeOut('fast');		
					visible = false;
				}
			}

			if(id_plaza != 0 && visible) {
				if(id_plaza == plaza) {
					if($(elem).is(':visible') == false) {
						$(elem).fadeIn('fast');	
					}
				} else {
					if($(elem).is(':visible')) {
						$(elem).fadeOut('fast');	
					}
					visible = false;
				}	
			}
		});
	} else {
		$.each($('.external-event'), function(i, elem) {
			var show_group = $(elem).data('id_show_group');
			var plaza = $(elem).data('id_plaza');
			var visible = true;

			if(id_show_group != 0) {
				if(id_show_group == show_group) {
					if($(elem).is(':visible') == false) {
						$(elem).fadeIn('fast');
					}
				} else {
					if($(elem).is(':visible') == true) {
						$(elem).fadeOut('fast');	
					}
					visible = false;
				}
			} else {
				visible = true;
			}

			if(id_plaza != 0 && visible) {
				if(id_plaza == plaza) {
					if($(elem).is(':visible') == false) {
						$(elem).fadeIn('fast');	
					}
				} else {
					if($(elem).is(':visible')) {
						$(elem).fadeOut('fast');	
					}
				}
			} else {
				visible = visible;
			}

			if(visible && $(elem).is(':visible') == false) {
				$(elem).fadeIn('fast');
			}
		});
	}
}

function getUrlParams() {
	var $_GET = {};

	document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
	    function decode(s) {
	        return decodeURIComponent(s.split("+").join(" "));
	    }

	    $_GET[decode(arguments[1])] = decode(arguments[2]);
	});

	return $_GET;
}

function convert(str) {
  var date = new Date(str),
    mnth = ("0" + (date.getMonth() + 1)).slice(-2),
    day = ("0" + date.getDate()).slice(-2);
  return [date.getFullYear(), mnth, day].join("-");
}

var showExportOptions = function() {
	var epg = $('select#epg_name').val();

	if(epg == "EPG 1") {
		$('div.select_days').removeClass('hidden');
		$('div.select_days').addClass('show');

		$('div.select_week').removeClass('show');
		$('div.select_week').addClass('hidden');
	} else {
		$('div.select_days').removeClass('show');
		$('div.select_days').addClass('hidden');

		$('div.select_week').removeClass('hidden');
		$('div.select_week').addClass('show');
	}
}