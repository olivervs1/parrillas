$(document).ready(function() {
	$('div#submit_btn').on('click', submitForm);
});

var submitForm = function(e) {
	e.preventDefault();

	if(validateForm()) {
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('input[name="_token"]').val()
			},
		    method: 'POST',
		    url: $('form#channels_form').attr('action'),
		    data: new FormData($('form#channels_form')[0]),
		    dataType:'json',
		    processData: false,
			contentType: false,
			success:function(response){								
				if(response.message) {
					createAlert(response.message);
				}

				if(response.status) {
					if(response.redirectTo) {
						window.location = response.redirectTo;
					}
				}	
			},
		});
	}
}