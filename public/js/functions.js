//var url = 'http://localhost/parrillas/public';
// var url = 'https://parrillasmm.mapper.mx';
var url = 'http://localhost:8000';

$(document).ready(function() {
	$('.required').on('keyup', removeBorderDangerClass);
	$('.required-select').on('change', removeBorderDangerClass);
});

function validateForm() {
	var validated = true;
	$.each($('.required'), function(i, elem) {
		var $e = $(elem);
		if($e.val() == "" || $e.val() == undefined || $e.val().length == 0) {
			validated = false;
			$e.addClass('border-danger');
		}
	});

	$.each($('.required-select'), function(i, elem) {
		var $e = $(elem);
		if($e.val() == "" || $e.val() == undefined || $e.val() == 0) {
			validated = false;
			$e.addClass('border-danger');
		}
	});

	return validated;
}

function removeBorderDangerClass() {
	$(this).removeClass('border-danger');
}

function createAlert(text, title, icon, buttons, ok, cancel, top, overlay) {
	if(title == undefined || title == "") {
		title = "Alerta";
	}

	if(icon == undefined || icon == "") {
		icon = '';
	}

	if(buttons == undefined || buttons == "") {
		buttons = 1;
	}

	if(ok == undefined || ok == "") {
		ok = "Aceptar";
	}

	if(cancel == undefined || cancel == "") {
		cancel = "Cancelar";
	}
	
	var btnCancel = "";

	$(':focus').blur();
	var $overlay = $('<div>', {'class' : 'overlay'});
	if(overlay != undefined && overlay != "") {
		$overlay.css('opacity', 0);
	}
	var $alertWindow = $('<div>', {'class': 'alertWindow green-card'});
	if(top != undefined && top != "") {
		$alertWindow.css('top', top);
	}
	var $wdwHeader = $('<h3>', {'class' : 'header'});
	var $wdwText = $('<p>', {'class' : 'wdwtext'});
	$('body').append($overlay);
	$overlay.css('height', $(document).height());
	var dw = Math.round(document.body.clientWidth/2) - 200;
	var dh = Math.round($(document).height()/2) - 200;
	$alertWindow.css({'left' : dw});
	var btn = $('<input type="button">').val(ok).addClass('alertWindowOk btn btn-primary btn-sm col-sm-3');
	var row = $('<div>').addClass('row justify-content-md-center col-12');
	if(buttons == 2) {
		$alertWindow.css({'min-width': '525px'});
		btnCancel = $('<input type="button">').val(cancel).addClass('alertWindowCancel btn btn-danger btn-sm col-5 fs-14');
		btnCancel.on('click', closeWindow);
		btn = $('<input type="button">').val(ok).addClass('alertWindowOk btn btn-primary col-5 fs-14 btn-sm');
	}
	btn.on('click', closeWindow);
	$('body').append($alertWindow.append($wdwHeader.html(icon).append(title)).append($wdwText.html(text)).append(row.append(btn).append(btnCancel)));
}

function closeWindow() {
	$('div.overlay').remove();
	$('div.alertWindow').remove();
	//$('div.modal').fadeOut();
	$('div.instructions').remove();
}

function showLoader() {
	var loaderHTML = '<div class="d-flex justify-content-center loader"><div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div></div>';
	var overlayHTML = '<div class="overlay"></div>';

	$('body').append(overlayHTML);
	$('body').append(loaderHTML);
}

function closeLoader() {
	$('div.overlay').remove();
	$('div.loader').remove();
}