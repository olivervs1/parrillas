CANAL PRINCIPAL	CANAL MENOR	FECHA DE INICIO	HORA INICIO	TERMINA	TITULO PROGRAMA	DESCRIPCI�N DEL PROGRAMA	G�NERO	CLASIFICACI�N	IDIOMA	SUBTITULOS	AUDIO
6	3	10/24/2022	00:00:00	1:30:00	[B]REP.  ADRIAN MARCELO	La irreverencia de Adri�n Marcelo tiene su propio espacio en el que se podr�n presenciar entrevistas descaradas, pl�ticas prohibidas, juegos peligrosos y muchas locura m�s. Grandes sorpresas e invitados nos esperan con este divertido y pol�mico conductor.	TALK SHOW	B	SPA	CC	STEREO
6	3	10/24/2022	01:30:00	1:00:00	[B]BUENAS NOCHES DON FEMATT	Programa con entrevistas informales, m�sica en vivo, y una mezcla de sketchs y stand up.	TALK SHOW	B	SPA	CC	STEREO
6	3	10/24/2022	02:30:00	0:30:00	[A]FUERA DEL CONTROL	El mejor Critico en videojuegos MEMO HIERVAS te trae mas nuevo del mundo del GAMING, criticas, rese�as , avances tecnol�gicos y mucho mas .	ENTRETENIMIENTO	A	SPA	CC	STEREO
6	3	10/24/2022	03:00:00	1:00:00	[A]FUERA DEL CONTROL	El mejor Critico en videojuegos MEMO HIERVAS te trae mas nuevo del mundo del GAMING, criticas, rese�as , avances tecnol�gicos y mucho mas .	ENTRETENIMIENTO	A	SPA	CC	STEREO
6	3	10/24/2022	04:00:00	1:30:00	[A]REP.  VIVALAVI MTY	Tips para en buen vivir, tecnolog�a, chismes, belleza... en Vivalavi.	REVISTA	A	SPA	CC	STEREO
6	3	10/24/2022	05:30:00	0:30:00	[A]WORK OUT	Programa con rutinas de ejercicio y tips de alimentaci�n	DEPORTES	A	SPA	CC	STEREO
6	3	10/24/2022	06:00:00	1:00:00	[B]REP. LAS RAPIDITAS	Noticias, Redes sociales, Farandulero, Deportes, Entrevistas, Reportajes de calle. Cada secci�n es un deleite , ya que tiene el sello y la experiencia de MAURICIO CASTILLO .	TALK SHOW	B	SPA	CC	STEREO
6	3	10/24/2022	07:00:00	0:30:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/24/2022	07:30:00	0:30:00	[A]IGLESIA UNIVERSAL	Publicidad Pagada Productos y recomendaciones para la salud	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/24/2022	08:00:00	1:00:00	[A-B]LA HORA DE WILLIE	Programa deportivo de pol�mica, que abarca temas del f�tbol y todos los deportes.	DEPORTES	A-B	SPA	CC	STEREO
6	3	10/24/2022	09:00:00	0:30:00	[A]PROD.NAT. ALMA	Publicidad pagada. Productos de salud y belleza.	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/24/2022	09:30:00	0:30:00	[A]PROSTALIVE   FABI	Publicidad pagada. Productos para la salud y bienestar	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/24/2022	10:00:00	2:00:00	[B]VIVALAVI CDMX	PORQUE LA VIDA ES AHORA, VIVALAVI...ES EL MATUTINO QUE NOS ENTRETIENE, INTERACTUAMOS Y NOS COMPARTE.. INFORMACION PRACTICA PARA LA VIDA COTIDIANA.	ENTRETENIMIENTO	B	SPA	CC	STEREO
6	3	10/24/2022	12:00:00	1:00:00	[A-B]LO MEJOR DEL CHISMORREO	Este programa te presenta una miscel�nea informativa sobre noticias, espect�culos, far�ndula nacional e internacional, redes sociales, invitados especialistas que nos ayudar�n a entender y debatir lo que esta en boca de todos.	ENTRETENIMIENTO	A-B	SPA	CC	STEREO
6	3	10/24/2022	13:00:00	1:00:00	[A]TRAICIONADOS		SERIE	A	SPA	CC	STEREO
6	3	10/24/2022	14:00:00	0:30:00	[A]IGLESIA UNIVERSAL	Publicidad Pagada Productos y recomendaciones para la salud	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/24/2022	14:30:00	0:30:00	[A]PROSTALIVE   FABI	Publicidad pagada. Productos para la salud y bienestar	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/24/2022	15:00:00	1:00:00	[A]HOMENAJE A	Conocer un poco de la vida de tu artista favorito, asi como sus pequenos y grandes logros artisticos, puedes verlo a traves de HOMENAJE A?.	ENTRETENIMIENTO	A	SPA	CC	STEREO
6	3	10/24/2022	16:00:00	1:00:00	[A]SABOTAJE EN  LA COCINA	�Qu� tan lejos est� dispuesto a ir un chef para ganar una competencia de cocina? En este programa se les dar� a cuatro chefs $25,000 y la oportunidad de gastar ese dinero para ayudarse a s� mismos o sabotear a sus competidores.	ENTRETENIMIENTO	A	SPA	CC	STEREO
6	3	10/24/2022	17:00:00	1:00:00	[A]PAKTEMOCIONES	Acompa�a a Temo Mendez en cada programa conociendo pueblos, leyendas, gastronom�a, bailes t�picos, turisteando y conociendo un poco de cada lugar y cada PUEBLO MAGICO de nuestro Pais.	CULTURAL	A	SPA	CC	STEREO
6	3	10/24/2022	18:00:00	0:30:00	[A]PROSTALIVE   FABI	Publicidad pagada. Productos para la salud y bienestar	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/24/2022	18:30:00	0:30:00	[A-B]UNA MAJA EN MEXICO	TURISTEANDO POR TODO MEXICO Y VISTO POR LOS OJOS DE UN TURISTA ESPANOL, QUE NOS PRESENTA LUGARES, CIUDADES, PAISAJES, CAMINOS Y PUEBLOS INCREIBLEMENTE BELLOS.	CULTURAL	A-B	SPA	CC	STEREO
6	3	10/24/2022	19:00:00	2:00:00	[B]ES SHOW EL MUSICAL	Programa de entretenimiento variado en donde Ernesto Chavana junto a su divertido elenco har�n re�r con sus ocurrencias y canto por las noches.	TALK SHOW	B	SPA	CC	STEREO
6	3	10/24/2022	21:00:00	1:00:00	[B]LAS RAPIDITAS	Noticias, Redes sociales, Farandulero, Deportes, Entrevistas, Reportajes de calle. Cada secci�n es un deleite , ya que tiene el sello y la experiencia de MAURICIO CASTILLO .	TALK SHOW	B	SPA	CC	STEREO
6	3	10/24/2022	22:00:00	1:00:00	[B]EL CHIRINGUITO	"El Chiringuito de Jugones" es un programa de televisi�n espa�ol de debate futbol�stico, en el que se trata la actualidad de los principales equipos de la liga Espa�ola.	DEPORTES	B	SPA	CC	STEREO
6	3	10/24/2022	23:00:00	1:00:00	[B]C4 EN ALERTA CDMX	Espacio dedicado a atender los reportes que aquejan a la ciudadan�a, dando voz en vivo para que expongan las problem�ticas que enfrentan en sus colonias. Somos el puente entre las personas y las autoridades.	NOTICIAS	B	SPA	CC	STEREO
6	3	10/25/2022	00:00:00	1:30:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/25/2022	01:30:00	2:30:00	[B]ES SHOW LUNES	Las noches son m�s divertidas si acompa�as a Ernesto Chavana y sus bellas chicas, un programa donde todo puede pasar... "ES SHOW"	ENTRETENIMIENTO	B	SPA	CC	STEREO
6	3	10/25/2022	04:00:00	1:30:00	[A]REP.  VIVALAVI MTY	Tips para en buen vivir, tecnolog�a, chismes, belleza... en Vivalavi.	REVISTA	A	SPA	CC	STEREO
6	3	10/25/2022	05:30:00	0:30:00	[A]WORK OUT	Programa con rutinas de ejercicio y tips de alimentaci�n	DEPORTES	A	SPA	CC	STEREO
6	3	10/25/2022	06:00:00	1:00:00	[B]REP. LAS RAPIDITAS	Noticias, Redes sociales, Farandulero, Deportes, Entrevistas, Reportajes de calle. Cada secci�n es un deleite , ya que tiene el sello y la experiencia de MAURICIO CASTILLO .	TALK SHOW	B	SPA	CC	STEREO
6	3	10/25/2022	07:00:00	0:30:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/25/2022	07:30:00	0:30:00	[A]IGLESIA UNIVERSAL	Publicidad Pagada Productos y recomendaciones para la salud	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/25/2022	08:00:00	1:00:00	[A-B]LA HORA DE WILLIE	Programa deportivo de pol�mica, que abarca temas del f�tbol y todos los deportes.	DEPORTES	A-B	SPA	CC	STEREO
6	3	10/25/2022	09:00:00	0:30:00	[A]BATICURE ALMA	Publicidad pagada. Salud y cuidado personal.	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/25/2022	09:30:00	0:30:00	[A]PROSTALIVE   FABI	Publicidad pagada. Productos para la salud y bienestar	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/25/2022	10:00:00	2:00:00	[B]VIVALAVI CDMX	PORQUE LA VIDA ES AHORA, VIVALAVI...ES EL MATUTINO QUE NOS ENTRETIENE, INTERACTUAMOS Y NOS COMPARTE.. INFORMACION PRACTICA PARA LA VIDA COTIDIANA.	ENTRETENIMIENTO	B	SPA	CC	STEREO
6	3	10/25/2022	12:00:00	1:00:00	[A]FUERA DEL CONTROL	El mejor Critico en videojuegos MEMO HIERVAS te trae mas nuevo del mundo del GAMING, criticas, rese�as , avances tecnol�gicos y mucho mas .	ENTRETENIMIENTO	A	SPA	CC	STEREO
6	3	10/25/2022	13:00:00	1:00:00	[A]TRAICIONADOS		SERIE	A	SPA	CC	STEREO
6	3	10/25/2022	14:00:00	0:30:00	[A]IGLESIA UNIVERSAL	Publicidad Pagada Productos y recomendaciones para la salud	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/25/2022	14:30:00	0:30:00	[A]PROSTALIVE   FABI	Publicidad pagada. Productos para la salud y bienestar	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/25/2022	15:00:00	1:00:00	[A]HOMENAJE A	Conocer un poco de la vida de tu artista favorito, asi como sus pequenos y grandes logros artisticos, puedes verlo a traves de HOMENAJE A?.	ENTRETENIMIENTO	A	SPA	CC	STEREO
6	3	10/25/2022	16:00:00	1:00:00	[A]SABOTAJE EN  LA COCINA	�Qu� tan lejos est� dispuesto a ir un chef para ganar una competencia de cocina? En este programa se les dar� a cuatro chefs $25,000 y la oportunidad de gastar ese dinero para ayudarse a s� mismos o sabotear a sus competidores.	ENTRETENIMIENTO	A	SPA	CC	STEREO
6	3	10/25/2022	17:00:00	1:00:00	[A-B]CHISMORREO	Este programa te presenta una miscel�nea informativa sobre noticias, espect�culos, far�ndula nacional e internacional, redes sociales, invitados especialistas que nos ayudar�n a entender y debatir lo que esta en boca de todos.	ENTRETENIMIENTO	A-B	SPA	CC	STEREO
6	3	10/25/2022	18:00:00	0:30:00	[A]PROSTALIVE   FABI	Publicidad pagada. Productos para la salud y bienestar	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/25/2022	18:30:00	2:30:00	[B]ES SHOW LUNES	Las noches son m�s divertidas si acompa�as a Ernesto Chavana y sus bellas chicas, un programa donde todo puede pasar... "ES SHOW"	ENTRETENIMIENTO	B	SPA	CC	STEREO
6	3	10/25/2022	21:00:00	1:00:00	[B]LAS RAPIDITAS	Noticias, Redes sociales, Farandulero, Deportes, Entrevistas, Reportajes de calle. Cada secci�n es un deleite , ya que tiene el sello y la experiencia de MAURICIO CASTILLO .	TALK SHOW	B	SPA	CC	STEREO
6	3	10/25/2022	22:00:00	1:00:00	[B]EL CHIRINGUITO	"El Chiringuito de Jugones" es un programa de televisi�n espa�ol de debate futbol�stico, en el que se trata la actualidad de los principales equipos de la liga Espa�ola.	DEPORTES	B	SPA	CC	STEREO
6	3	10/25/2022	23:00:00	1:00:00	[B]C4 EN ALERTA CDMX	Espacio dedicado a atender los reportes que aquejan a la ciudadan�a, dando voz en vivo para que expongan las problem�ticas que enfrentan en sus colonias. Somos el puente entre las personas y las autoridades.	NOTICIAS	B	SPA	CC	STEREO
6	3	10/26/2022	00:00:00	1:30:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/26/2022	01:30:00	2:30:00	[B]ES SHOW MARTES	Las noches son m�s divertidas si acompa�as a Ernesto Chavana y sus bellas chicas, un programa donde todo puede pasar... "ES SHOW"	TALK SHOW	B	SPA	CC	STEREO
6	3	10/26/2022	04:00:00	1:30:00	[A]REP.  VIVALAVI MTY	Tips para en buen vivir, tecnolog�a, chismes, belleza... en Vivalavi.	REVISTA	A	SPA	CC	STEREO
6	3	10/26/2022	05:30:00	0:30:00	[A]WORK OUT	Programa con rutinas de ejercicio y tips de alimentaci�n	DEPORTES	A	SPA	CC	STEREO
6	3	10/26/2022	06:00:00	1:00:00	[B]REP. LAS RAPIDITAS	Noticias, Redes sociales, Farandulero, Deportes, Entrevistas, Reportajes de calle. Cada secci�n es un deleite , ya que tiene el sello y la experiencia de MAURICIO CASTILLO .	TALK SHOW	B	SPA	CC	STEREO
6	3	10/26/2022	07:00:00	0:30:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/26/2022	07:30:00	0:30:00	[A]IGLESIA UNIVERSAL	Publicidad Pagada Productos y recomendaciones para la salud	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/26/2022	08:00:00	1:00:00	[A-B]LA HORA DE WILLIE	Programa deportivo de pol�mica, que abarca temas del f�tbol y todos los deportes.	DEPORTES	A-B	SPA	CC	STEREO
6	3	10/26/2022	09:00:00	0:30:00	[A]PROD.NAT. ALMA	Publicidad pagada. Productos de salud y belleza.	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/26/2022	09:30:00	0:30:00	[A]PROSTALIVE   FABI	Publicidad pagada. Productos para la salud y bienestar	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/26/2022	10:00:00	2:00:00	[B]VIVALAVI CDMX	PORQUE LA VIDA ES AHORA, VIVALAVI...ES EL MATUTINO QUE NOS ENTRETIENE, INTERACTUAMOS Y NOS COMPARTE.. INFORMACION PRACTICA PARA LA VIDA COTIDIANA.	ENTRETENIMIENTO	B	SPA	CC	STEREO
6	3	10/26/2022	12:00:00	1:00:00	[A]AMAZON MUSIC NEWS	Playlists de MUSICA y novedades �sta en AMAZON MUSIC NEWS	MUSICAL	A	SPA	CC	STEREO
6	3	10/26/2022	13:00:00	1:00:00	[A]TRAICIONADOS		SERIE	A	SPA	CC	STEREO
6	3	10/26/2022	14:00:00	0:30:00	[A]IGLESIA UNIVERSAL	Publicidad Pagada Productos y recomendaciones para la salud	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/26/2022	14:30:00	0:30:00	[A]PROSTALIVE   FABI	Publicidad pagada. Productos para la salud y bienestar	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/26/2022	15:00:00	1:00:00	[A]HOMENAJE A	Conocer un poco de la vida de tu artista favorito, asi como sus pequenos y grandes logros artisticos, puedes verlo a traves de HOMENAJE A?.	ENTRETENIMIENTO	A	SPA	CC	STEREO
6	3	10/26/2022	16:00:00	1:00:00	[A]SABOTAJE EN  LA COCINA	�Qu� tan lejos est� dispuesto a ir un chef para ganar una competencia de cocina? En este programa se les dar� a cuatro chefs $25,000 y la oportunidad de gastar ese dinero para ayudarse a s� mismos o sabotear a sus competidores.	ENTRETENIMIENTO	A	SPA	CC	STEREO
6	3	10/26/2022	17:00:00	1:00:00	[A-B]CHISMORREO	Este programa te presenta una miscel�nea informativa sobre noticias, espect�culos, far�ndula nacional e internacional, redes sociales, invitados especialistas que nos ayudar�n a entender y debatir lo que esta en boca de todos.	ENTRETENIMIENTO	A-B	SPA	CC	STEREO
6	3	10/26/2022	18:00:00	0:30:00	[A]PROSTALIVE   FABI	Publicidad pagada. Productos para la salud y bienestar	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/26/2022	18:30:00	2:30:00	[B]ES SHOW MARTES	Las noches son m�s divertidas si acompa�as a Ernesto Chavana y sus bellas chicas, un programa donde todo puede pasar... "ES SHOW"	TALK SHOW	B	SPA	CC	STEREO
6	3	10/26/2022	21:00:00	1:00:00	[B]LAS RAPIDITAS	Noticias, Redes sociales, Farandulero, Deportes, Entrevistas, Reportajes de calle. Cada secci�n es un deleite , ya que tiene el sello y la experiencia de MAURICIO CASTILLO .	TALK SHOW	B	SPA	CC	STEREO
6	3	10/26/2022	22:00:00	1:00:00	[B]EL CHIRINGUITO	"El Chiringuito de Jugones" es un programa de televisi�n espa�ol de debate futbol�stico, en el que se trata la actualidad de los principales equipos de la liga Espa�ola.	DEPORTES	B	SPA	CC	STEREO
6	3	10/26/2022	23:00:00	1:00:00	[B]C4 EN ALERTA CDMX	Espacio dedicado a atender los reportes que aquejan a la ciudadan�a, dando voz en vivo para que expongan las problem�ticas que enfrentan en sus colonias. Somos el puente entre las personas y las autoridades.	NOTICIAS	B	SPA	CC	STEREO
6	3	10/27/2022	00:00:00	1:30:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/27/2022	01:30:00	2:30:00	[B]ADRIAN MARCELO PRESENTA	La irreverencia de Adrian Marcelo tiene su propio espacio en el que se pueden presenciar entrevistas descaradas, platicas prohibidas, juegos peligrosos y muchas m�s locuras. Grandes sorpresas e invitados nos esperan a mitad de la semana con este divertido y polemico conductor.	TALK SHOW	B	SPA	CC	STEREO
6	3	10/27/2022	04:00:00	1:30:00	[A]REP.  VIVALAVI MTY	Tips para en buen vivir, tecnolog�a, chismes, belleza... en Vivalavi.	REVISTA	A	SPA	CC	STEREO
6	3	10/27/2022	05:30:00	0:30:00	[A]WORK OUT	Programa con rutinas de ejercicio y tips de alimentaci�n	DEPORTES	A	SPA	CC	STEREO
6	3	10/27/2022	06:00:00	1:00:00	[B]REP. LAS RAPIDITAS	Noticias, Redes sociales, Farandulero, Deportes, Entrevistas, Reportajes de calle. Cada secci�n es un deleite , ya que tiene el sello y la experiencia de MAURICIO CASTILLO .	TALK SHOW	B	SPA	CC	STEREO
6	3	10/27/2022	07:00:00	0:30:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/27/2022	07:30:00	0:30:00	[A]IGLESIA UNIVERSAL	Publicidad Pagada Productos y recomendaciones para la salud	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/27/2022	08:00:00	1:00:00	[A-B]LA HORA DE WILLIE	Programa deportivo de pol�mica, que abarca temas del f�tbol y todos los deportes.	DEPORTES	A-B	SPA	CC	STEREO
6	3	10/27/2022	09:00:00	0:30:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/27/2022	09:30:00	0:30:00	[A]PROSTALIVE   FABI	Publicidad pagada. Productos para la salud y bienestar	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/27/2022	10:00:00	2:00:00	[B]VIVALAVI CDMX	PORQUE LA VIDA ES AHORA, VIVALAVI...ES EL MATUTINO QUE NOS ENTRETIENE, INTERACTUAMOS Y NOS COMPARTE.. INFORMACION PRACTICA PARA LA VIDA COTIDIANA.	ENTRETENIMIENTO	B	SPA	CC	STEREO
6	3	10/27/2022	12:00:00	1:00:00	[B]PANTALLAZO	Los momentos chuscos y divertidos de la programaci�n de Canal 6, los revives en PANTALLAZO.	TALK SHOW	B	SPA	CC	STEREO
6	3	10/27/2022	13:00:00	1:00:00	[A]TRAICIONADOS		SERIE	A	SPA	CC	STEREO
6	3	10/27/2022	14:00:00	0:30:00	[A]IGLESIA UNIVERSAL	Publicidad Pagada Productos y recomendaciones para la salud	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/27/2022	14:30:00	0:30:00	[A]PROSTALIVE   FABI	Publicidad pagada. Productos para la salud y bienestar	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/27/2022	15:00:00	1:00:00	[A]HOMENAJE A	Conocer un poco de la vida de tu artista favorito, asi como sus pequenos y grandes logros artisticos, puedes verlo a traves de HOMENAJE A?.	ENTRETENIMIENTO	A	SPA	CC	STEREO
6	3	10/27/2022	16:00:00	1:00:00	[A]SABOTAJE EN  LA COCINA	�Qu� tan lejos est� dispuesto a ir un chef para ganar una competencia de cocina? En este programa se les dar� a cuatro chefs $25,000 y la oportunidad de gastar ese dinero para ayudarse a s� mismos o sabotear a sus competidores.	ENTRETENIMIENTO	A	SPA	CC	STEREO
6	3	10/27/2022	17:00:00	1:00:00	[A-B]CHISMORREO	Este programa te presenta una miscel�nea informativa sobre noticias, espect�culos, far�ndula nacional e internacional, redes sociales, invitados especialistas que nos ayudar�n a entender y debatir lo que esta en boca de todos.	ENTRETENIMIENTO	A-B	SPA	CC	STEREO
6	3	10/27/2022	18:00:00	0:30:00	[A]PROSTALIVE  3 FABI	Publicidad pagada Productos para la salud y bienestar	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/27/2022	18:30:00	2:30:00	[B]ADRIAN MARCELO PRESENTA	La irreverencia de Adrian Marcelo tiene su propio espacio en el que se pueden presenciar entrevistas descaradas, platicas prohibidas, juegos peligrosos y muchas m�s locuras. Grandes sorpresas e invitados nos esperan a mitad de la semana con este divertido y polemico conductor.	TALK SHOW	B	SPA	CC	STEREO
6	3	10/27/2022	21:00:00	1:00:00	[B]LAS RAPIDITAS	Noticias, Redes sociales, Farandulero, Deportes, Entrevistas, Reportajes de calle. Cada secci�n es un deleite , ya que tiene el sello y la experiencia de MAURICIO CASTILLO .	TALK SHOW	B	SPA	CC	STEREO
6	3	10/27/2022	22:00:00	1:00:00	[B]EL CHIRINGUITO	"El Chiringuito de Jugones" es un programa de televisi�n espa�ol de debate futbol�stico, en el que se trata la actualidad de los principales equipos de la liga Espa�ola.	DEPORTES	B	SPA	CC	STEREO
6	3	10/27/2022	23:00:00	1:00:00	[B]C4 EN ALERTA CDMX	Espacio dedicado a atender los reportes que aquejan a la ciudadan�a, dando voz en vivo para que expongan las problem�ticas que enfrentan en sus colonias. Somos el puente entre las personas y las autoridades.	NOTICIAS	B	SPA	CC	STEREO
6	3	10/28/2022	00:00:00	1:30:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/28/2022	01:30:00	2:30:00	[B]SNSERIO	Cada semana invitados especiales nos cautivan en SNSERIO con sus amenas e interesantes entrevistas en donde se conoce a profundidad a los invitados ; tambi�n se habla de noticias actuales y se discuten los temas que se mueven en redes sociales, en el pa�s y en el mundo. Acompa�a a nuestros entrevistadores de lujo Enrique Mayagoitia y Adri�n Marcelo? SNSERIO es para ti.	TALK SHOW	B	SPA	CC	STEREO
6	3	10/28/2022	04:00:00	1:30:00	[A]REP.  VIVALAVI MTY	Tips para en buen vivir, tecnolog�a, chismes, belleza... en Vivalavi.	REVISTA	A	SPA	CC	STEREO
6	3	10/28/2022	05:30:00	0:30:00	[A]WORK OUT	Programa con rutinas de ejercicio y tips de alimentaci�n	DEPORTES	A	SPA	CC	STEREO
6	3	10/28/2022	06:00:00	1:00:00	[B]REP. LAS RAPIDITAS	Noticias, Redes sociales, Farandulero, Deportes, Entrevistas, Reportajes de calle. Cada secci�n es un deleite , ya que tiene el sello y la experiencia de MAURICIO CASTILLO .	TALK SHOW	B	SPA	CC	STEREO
6	3	10/28/2022	07:00:00	0:30:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/28/2022	07:30:00	0:30:00	[A]IGLESIA UNIVERSAL	Publicidad Pagada Productos y recomendaciones para la salud	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/28/2022	08:00:00	1:00:00	[A-B]LA HORA DE WILLIE	Programa deportivo de pol�mica, que abarca temas del f�tbol y todos los deportes.	DEPORTES	A-B	SPA	CC	STEREO
6	3	10/28/2022	09:00:00	1:00:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/28/2022	10:00:00	2:00:00	[B]VIVALAVI CDMX	PORQUE LA VIDA ES AHORA, VIVALAVI...ES EL MATUTINO QUE NOS ENTRETIENE, INTERACTUAMOS Y NOS COMPARTE.. INFORMACION PRACTICA PARA LA VIDA COTIDIANA.	ENTRETENIMIENTO	B	SPA	CC	STEREO
6	3	10/28/2022	12:00:00	1:00:00	[A]ORBITAL	Programa de varidades, entretenimiento, deporte extremo,moda, cine, redes sociales, viedeo juegos, musica y mucho mas diversi�n.	ENTRETENIMIENTO	A	SPA	CC	STEREO
6	3	10/28/2022	13:00:00	1:00:00	[A]TRAICIONADOS		SERIE	A	SPA	CC	STEREO
6	3	10/28/2022	14:00:00	0:30:00	[A]IGLESIA UNIVERSAL	Publicidad Pagada Productos y recomendaciones para la salud	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/28/2022	14:30:00	0:30:00	[B]INFOMERCIALES	Ventas por televisi�n Productos de Belleza, Salud, Bienestar, Hogar y Deporte, Esot�ricos,	VENTAS/MERCADEO	B	SPA	CC	STEREO
6	3	10/28/2022	15:00:00	1:00:00	[A]HOMENAJE A	Conocer un poco de la vida de tu artista favorito, asi como sus pequenos y grandes logros artisticos, puedes verlo a traves de HOMENAJE A?.	ENTRETENIMIENTO	A	SPA	CC	STEREO
6	3	10/28/2022	16:00:00	1:00:00	[A]SABOTAJE EN  LA COCINA	�Qu� tan lejos est� dispuesto a ir un chef para ganar una competencia de cocina? En este programa se les dar� a cuatro chefs $25,000 y la oportunidad de gastar ese dinero para ayudarse a s� mismos o sabotear a sus competidores.	ENTRETENIMIENTO	A	SPA	CC	STEREO
6	3	10/28/2022	17:00:00	1:00:00	[B]ENTRE SOMBRAS	Historias de Fantasmas, Terror y mas nos presenta Carlos Trejo en este programa ENTRE SOMBRAS	ENTRETENIMIENTO	B	SPA	CC	STEREO
6	3	10/28/2022	18:00:00	0:30:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/28/2022	18:30:00	2:30:00	[B]SNSERIO	Cada semana invitados especiales nos cautivan en SNSERIO con sus amenas e interesantes entrevistas en donde se conoce a profundidad a los invitados ; tambi�n se habla de noticias actuales y se discuten los temas que se mueven en redes sociales, en el pa�s y en el mundo. Acompa�a a nuestros entrevistadores de lujo Enrique Mayagoitia y Adri�n Marcelo? SNSERIO es para ti.	TALK SHOW	B	SPA	CC	STEREO
6	3	10/28/2022	21:00:00	1:00:00	[B]LAS RAPIDITAS	Noticias, Redes sociales, Farandulero, Deportes, Entrevistas, Reportajes de calle. Cada secci�n es un deleite , ya que tiene el sello y la experiencia de MAURICIO CASTILLO .	TALK SHOW	B	SPA	CC	STEREO
6	3	10/28/2022	22:00:00	1:00:00	[A-B]LUCHA LIBRE AAA	En el Mundo de la Lucha Libre siempre ha existido la rivalidad entre RUDOS Y T�CNICOS y cada semana nos lo hacen saber en sus espectaculares encuentros? LUCHA LIBRE AAA que gane el mejor?	DEPORTES	A-B	SPA	CC	STEREO
6	3	10/28/2022	23:00:00	1:00:00	[A]FUERA DEL CONTROL	El mejor Critico en videojuegos MEMO HIERVAS te trae mas nuevo del mundo del GAMING, criticas, rese�as , avances tecnol�gicos y mucho mas .	ENTRETENIMIENTO	A	SPA	CC	STEREO
6	3	10/29/2022	00:00:00	2:00:00	[B]ES SHOW EL MUSICAL	Programa de entretenimiento variado en donde Ernesto Chavana junto a su divertido elenco har�n re�r con sus ocurrencias y canto por las noches.	TALK SHOW	B	SPA	CC	STEREO
6	3	10/29/2022	02:00:00	1:30:00	[B]BUENAS NOCHES DON FEMATT	Programa con entrevistas informales, m�sica en vivo, y una mezcla de sketchs y stand up.	TALK SHOW	B	SPA	CC	STEREO
6	3	10/29/2022	03:30:00	0:30:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/29/2022	04:00:00	1:30:00	[A]REP.  VIVALAVI MTY	Tips para en buen vivir, tecnolog�a, chismes, belleza... en Vivalavi.	REVISTA	A	SPA	CC	STEREO
6	3	10/29/2022	05:30:00	0:30:00	[A]WORK OUT	Programa con rutinas de ejercicio y tips de alimentaci�n	DEPORTES	A	SPA	CC	STEREO
6	3	10/29/2022	06:00:00	1:30:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/29/2022	07:30:00	0:30:00	[A]IGLESIA UNIVERSAL	Publicidad Pagada Productos y recomendaciones para la salud	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/29/2022	08:00:00	0:30:00	[A-B]REGENEX	Publicidad pagada. Productos para cuidado y conservaci�n de la salud	VENTAS/MERCADEO	A-B	SPA	CC	STEREO
6	3	10/29/2022	08:30:00	0:30:00	[A]WORK OUT	Programa con rutinas de ejercicio y tips de alimentaci�n	DEPORTES	A	SPA	CC	STEREO
6	3	10/29/2022	09:00:00	1:00:00	[A]CELEXTRA ALMA	Publicidad pagada. Productos para la conservaci�n conservaci�n y mejora de la salud.	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/29/2022	10:00:00	1:00:00	[A]AMAZON MUSIC NEWS	Playlists de MUSICA y novedades �sta en AMAZON MUSIC NEWS	MUSICAL	A	SPA	CC	STEREO
6	3	10/29/2022	11:00:00	2:00:00	[A]CELEXTRA ALMA	Publicidad pagada. Productos para la conservaci�n conservaci�n y mejora de la salud.	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/29/2022	13:00:00	1:00:00	[A-B]BACTERIUM ALMA	Productos para la conservacion y bienestar de la salud	VENTAS/MERCADEO	A-B	SPA	CC	STEREO
6	3	10/29/2022	14:00:00	0:30:00	[A]IGLESIA UNIVERSAL	Publicidad Pagada Productos y recomendaciones para la salud	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/29/2022	14:30:00	0:30:00	[A]PROSTALIVE ANA	Publicidad Pagada. Productos para conservaci�n de la salud.	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/29/2022	15:00:00	1:00:00	[A]CELEXTRA ALMA	Publicidad pagada. Productos para la conservaci�n conservaci�n y mejora de la salud.	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/29/2022	16:00:00	0:30:00	[A]HELIX ANA	Publicidad pagada. Productos para la salud y bienestar.	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/29/2022	16:30:00	1:00:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/29/2022	17:30:00	1:00:00	[A]BEETHERAPY	Publicidad Pagada Productos y Terapias para la Conservaci�n de la Salud	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/29/2022	18:30:00	1:00:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/29/2022	19:30:00	1:00:00	[B]RESUMEN LAS RAPIDITAS	Programa con Noticias de las Redes sociales, Farandulero, Deportes, Entrevistas, Reportajes de calle. Cada secci�n es un deleite , ya que tiene el sello y la experiencia de MAURICIO CASTILLO .	TALK SHOW	B	SPA	CC	STEREO
6	3	10/29/2022	20:30:00	1:30:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/29/2022	22:00:00	0:30:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/29/2022	22:30:00	1:30:00	[B]REP. SNSERIO	Cada semana invitados especiales nos cautivan en SNSERIO con sus amenas e interesantes entrevistas en donde se conoce a profundidad a los invitados de esa noche; tambi�n se habla de noticias actuales y se discuten los temas que se mueven en redes sociales, en el pa�s y en el mundo. Acompa�a a nuestros entrevistadores de lujo Enrique Mayagoitia y Adrian Marcelo todos los Jueves en nuestra barra nocturna? SNSERIO es para ti.	TALK SHOW	B	SPA	CC	STEREO
6	3	10/30/2022	00:00:00	1:00:00	[B]REP. SNSERIO	Cada semana invitados especiales nos cautivan en SNSERIO con sus amenas e interesantes entrevistas en donde se conoce a profundidad a los invitados de esa noche; tambi�n se habla de noticias actuales y se discuten los temas que se mueven en redes sociales, en el pa�s y en el mundo. Acompa�a a nuestros entrevistadores de lujo Enrique Mayagoitia y Adrian Marcelo todos los Jueves en nuestra barra nocturna? SNSERIO es para ti.	TALK SHOW	B	SPA	CC	STEREO
6	3	10/30/2022	01:00:00	1:00:00	[A]ORBITAL	Programa de varidades, entretenimiento, deporte extremo,moda, cine, redes sociales, viedeo juegos, musica y mucho mas diversi�n.	ENTRETENIMIENTO	A	SPA	CC	STEREO
6	3	10/30/2022	02:00:00	1:00:00	[A]AMAZON MUSIC NEWS	Playlists de MUSICA y novedades �sta en AMAZON MUSIC NEWS	MUSICAL	A	SPA	CC	STEREO
6	3	10/30/2022	03:00:00	1:00:00	[A]FUERA DEL CONTROL	El mejor Critico en videojuegos MEMO HIERVAS te trae mas nuevo del mundo del GAMING, criticas, rese�as , avances tecnol�gicos y mucho mas .	ENTRETENIMIENTO	A	SPA	CC	STEREO
6	3	10/30/2022	04:00:00	1:00:00	[A-B]ESPECIAL DE HOMENAJES	ESTAMOS LISTOS PARA VER Y ESCUCHAR LAS VIVENCIAS, TRIUNFOS, Y TODO LO QUE NOS QUIERAN COMPARTIR NUESTROS ARTISTAS CADA SEMANA EN ESPECIAL DE HOMENAJES	ENTRETENIMIENTO	A-B	SPA	CC	STEREO
6	3	10/30/2022	05:00:00	0:30:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/30/2022	05:30:00	0:30:00	[A]WORK OUT	Programa con rutinas de ejercicio y tips de alimentaci�n	DEPORTES	A	SPA	CC	STEREO
6	3	10/30/2022	06:00:00	0:30:00	[A]BEETHERAPY	Publicidad Pagada Productos y Terapias para la Conservaci�n de la Salud	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/30/2022	06:30:00	2:00:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/30/2022	08:30:00	0:30:00	[A]ABEE MEED	Publicidad Pagada Productos para la Conservaci�n de la Salud	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/30/2022	09:00:00	1:00:00	[A]CELEXTRA ALMA	Publicidad pagada. Productos para la conservaci�n conservaci�n y mejora de la salud.	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/30/2022	10:00:00	1:00:00	[A]PROD.NAT. ALMA	Publicidad pagada. Productos de salud y belleza.	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/30/2022	11:00:00	1:00:00	[A]CELEXTRA ALMA	Publicidad pagada. Productos para la conservaci�n conservaci�n y mejora de la salud.	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/30/2022	12:00:00	0:30:00	[A-B]BACTERIUM ALMA	Productos para la conservacion y bienestar de la salud	VENTAS/MERCADEO	A-B	SPA	CC	STEREO
6	3	10/30/2022	12:30:00	0:30:00	[A]SOBRE LA RAYA	Equipos de casa que participan en la liga Expansi�n y Rayadas Femenil, comentarios, entrevistas, entrenamientos,etc.	ENTRETENIMIENTO	A	SPA	CC	STEREO
6	3	10/30/2022	13:00:00	1:00:00	[A]MISA DOMINICAL	Misa Dominical transmitida desde la Catedral de Monterrey.	RELIGIOSO	A	SPA	CC	STEREO
6	3	10/30/2022	14:00:00	0:30:00	[A]BEETHERAPY	Publicidad Pagada Productos y Terapias para la Conservaci�n de la Salud	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/30/2022	15:00:00	0:30:00	[A-B]UNA MAJA EN MEXICO	TURISTEANDO POR TODO MEXICO Y VISTO POR LOS OJOS DE UN TURISTA ESPANOL, QUE NOS PRESENTA LUGARES, CIUDADES, PAISAJES, CAMINOS Y PUEBLOS INCREIBLEMENTE BELLOS.	CULTURAL	A-B	SPA	CC	STEREO
6	3	10/30/2022	15:30:00	0:30:00	[A-B]CHISMORREO	Este programa te presenta una miscel�nea informativa sobre noticias, espect�culos, far�ndula nacional e internacional, redes sociales, invitados especialistas que nos ayudar�n a entender y debatir lo que esta en boca de todos.	ENTRETENIMIENTO	A-B	SPA	CC	STEREO
6	3	10/30/2022	16:00:00	0:30:00	[A]CELEXTRA ALMA	Publicidad pagada. Productos para la conservaci�n conservaci�n y mejora de la salud.	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/30/2022	16:30:00	0:30:00	[A]CELEXTRA ALMA	Publicidad pagada. Productos para la conservaci�n conservaci�n y mejora de la salud.	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/30/2022	17:00:00	1:00:00	[A]CELEXTRA ALMA	Publicidad pagada. Productos para la conservaci�n conservaci�n y mejora de la salud.	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/30/2022	18:00:00	0:30:00	[A-B]INFO. ANITA	Productos para la belleza y conservaci�n de la salud	VENTAS/MERCADEO	A-B	SPA	CC	STEREO
6	3	10/30/2022	18:30:00	0:30:00	[A]PROSTALIVE ANA	Publicidad Pagada. Productos para conservaci�n de la salud.	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/30/2022	19:00:00	0:30:00	[A]HELIX ANA	Publicidad pagada. Productos para la salud y bienestar.	VENTAS/MERCADEO	A	SPA	CC	STEREO
6	3	10/30/2022	19:30:00	1:00:00	[B]INFOMERCIALES - JUAN	Ventas por televisi�n Productos de Belleza, Salud, Bienestar, Hogar y Deporte, Esot�ricos,	VENTAS/MERCADEO	B	SPA	CC	STEREO
6	3	10/30/2022	20:30:00	0:30:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/30/2022	21:00:00	1:00:00	[A-B]BACTERIUM ALMA	Productos para la conservacion y bienestar de la salud	VENTAS/MERCADEO	A-B	SPA	CC	STEREO
6	3	10/30/2022	22:00:00	0:30:00	[A-B]INFO. BIOMAUSSAN	PRODUCTOS PARA MEJORAMIENTO Y CUIDADO DE LA SALUD	VENTAS/MERCADEO	A-B	SPA	CC	STEREO
6	3	10/30/2022	22:30:00	0:30:00	[A]VIDEOS TELERITMO	Videos musicales de todos los g�neros.	MUSICAL	A	SPA	CC	STEREO
6	3	10/30/2022	23:00:00	1:00:00	[B]REP.  ADRIAN MARCELO	La irreverencia de Adri�n Marcelo tiene su propio espacio en el que se podr�n presenciar entrevistas descaradas, pl�ticas prohibidas, juegos peligrosos y muchas locura m�s. Grandes sorpresas e invitados nos esperan con este divertido y pol�mico conductor.	TALK SHOW	B	SPA	CC	STEREO
